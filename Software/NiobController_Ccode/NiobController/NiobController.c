/*
 * NiobController.c
 *
 * Created: 23.01.2023 25:25:25
 *  Author: NiobEngine
 */ 

#define F_CPU 12000000UL
#include "NiobController.h"
#include <util/delay.h>

// UART interface initialisieren
// 9600Baud 8N1
void Init_Controller(){
	UCSRA&=~(1<<U2X);				// keine doppelte Geschwindigkeit
	UCSRB|=(1<<TXEN);				// transmit
	UCSRC|=(1<<UCSZ1)|(1<<UCSZ0);	// 8 Bit Datenübertragung
	
	UBRRH = 0X00;					// Data Rate = 9600 Baud
	UBRRL = 0X4D;					// -""-
	
	DDRD|=0x02;		PORTD|=0x02;	// TxD => Ausgang
}


void Send_Input(char channel, long data){
	UART_Transmit(channel&0x3F);
	volatile unsigned char i = 0x40;
	for (;((unsigned char)i)>0x0F;i+=0x40)
	{
		UART_Transmit((((char)data)&0x3F)|i);
		data = ((data<<2)>>8);
	}
}

void UART_Transmit(char Data)
{	
	_delay_ms(1);
	UDR= Data;
	while(!(UCSRA&&(1<<TXC)));
}