/*
 * NiobController.h
 *
 * Created: 23.01.2023 25:25:25
 *  Author: NiobEngine
 */ 


#ifndef NIOBCONTROLLER_H_
#define NIOBCONTROLLER_H_
#include <avr/io.h>

void Init_Controller(void);
void Send_Input(char channel, long data);


#endif /* NIOBCONTROLLER_H_ */