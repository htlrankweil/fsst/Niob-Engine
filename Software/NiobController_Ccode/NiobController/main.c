#include <avr/io.h>
#include <util/delay.h>
#include "NiobController.h"

#define F_CPU 12000000ul

void Init_Ports(){
	//S0-S3 Input
	DDRA &= 0xD0;
	PORTA|= 0x0F;
}

void Init_ADC(){
	// ADC - einschalten
	ADCSRA|= (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); 				//Relative Messung, Rechtsbündig
	ADMUX|= (1<<REFS0);
}

int Read_ADC(char channel){
	ADMUX&=0xE0;					//Alter Channel Löschen
	ADMUX |= channel&0x1F;			//Channel Einstellen

	ADCSRA |= (1<<ADSC);			//Einzellmessung wird gestartet
	while (ADCSRA & (1<<ADSC)){};	//Auf Ende der Messung Warten

	return ADC;						//10Bit ausgeben
}


int main(void)
{
	Init_Ports();
	Init_ADC();
	Init_Controller();
	
	char buttons_Last = 0;
	int adc_Last = 0;
	while (1)
	{
		char buttons = (~PINA)&0x0F;
		if (buttons_Last^buttons) Send_Input(0,buttons);
		
		int adc_val = Read_ADC(5);
		if (adc_Last^adc_val) Send_Input(1,adc_val);
		
		buttons_Last = buttons;
		adc_Last = adc_val;
		_delay_ms(10);
	}
}

