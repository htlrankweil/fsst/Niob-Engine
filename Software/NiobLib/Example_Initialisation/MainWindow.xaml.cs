﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NiobLib;
using NiobLib.Entities;
using SharpDX;

namespace Example_Initialisation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NiobViewPort mainPort = new NiobViewPort();
        NiobScene exampleScene = new NiobScene();
        public MainWindow()
        {
            InitializeComponent();
            grdMain.Children.Add(new NiobViewPort());
            mainPort.Scene = exampleScene;
            NiobDirectionalLight dirLight = 
                new NiobDirectionalLight(Color4.White, new Vector3(0, 0, 0));
            exampleScene.Entities.Add(dirLight);
        }
    }
}
