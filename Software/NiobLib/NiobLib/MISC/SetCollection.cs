﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NiobLib.Entities;
using NiobLib;


// Danke Bra: https://stackoverflow.com/questions/526986/how-can-i-make-an-observable-hashset-in-c
namespace NiobLib
{
    public class SetCollection<T> : ObservableCollection<T>
    {
        protected override void InsertItem(int index, T item)
        {
            if (Contains(item)) throw new NiobException(item, Stressgrund.ItemExists);

            base.InsertItem(index, item);
        }

        protected override void SetItem(int index, T item)
        {
            int i = IndexOf(item);
            if (i >= 0 && i != index) throw new NiobException(item, Stressgrund.ItemExists);

            base.SetItem(index, item);
        }
    }
}
