﻿using Assimp;
using HelixToolkit.SharpDX.Core.Assimp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using HelixToolkit.Wpf.SharpDX;

namespace NiobLib
{
    public class SceneImporter
    {
        public static PostProcessSteps DefaultPostProcessing = PostProcessSteps.Triangulate | PostProcessSteps.JoinIdenticalVertices
            | PostProcessSteps.FindDegenerates | PostProcessSteps.SortByPrimitiveType | PostProcessSteps.RemoveRedundantMaterials | PostProcessSteps.FlipUVs;

        public static HelixToolkitScene ImportEmbeded(string filename, string textureDir, float GlobalScale = 0.1f, bool CreateSceleton = false, float BoneScale = 0.04f)
        {
            Assembly assembly = Assembly.GetEntryAssembly();
           
            string FilePath = assembly.GetManifestResourceNames().Where(x => x.EndsWith(filename)).FirstOrDefault();
            if (FilePath == null) throw new Exception("FileNotFound");

            AssimpContext assimpContext = new AssimpContext() { Scale = GlobalScale };
            Scene assimpScene = assimpContext.ImportFileFromStream(assembly.GetManifestResourceStream(FilePath), DefaultPostProcessing);


            Dictionary<string, int> textureDict = new Dictionary<string, int>();
            foreach (Assimp.Material material in assimpScene.Materials)
            {
                EmbedMaterial(material, assimpScene, textureDir, textureDict);
            }

            Importer importer = new Importer();
            importer.Configuration.GlobalScale = GlobalScale;
            importer.Configuration.CreateSkeletonForBoneSkinningMesh = CreateSceleton;
            importer.Configuration.SkeletonSizeScale = BoneScale;


            if (importer.Load(assimpScene, "", out var root).HasFlag(ErrorCode.Succeed))
                return root;
            return null;
        }



        private static void EmbedMaterial(Assimp.Material material, Scene scene, string textureDir, Dictionary<string, int> textureDict)
        {
            if (material.HasTextureAmbient)
            {
                EmbedTexture(material.TextureAmbient, scene, textureDir, textureDict);
            }
            if (material.HasTextureAmbientOcclusion)
            {
                EmbedTexture(material.TextureAmbientOcclusion, scene, textureDir, textureDict);
            }
            if (material.HasTextureDiffuse)
            {
                EmbedTexture(material.TextureDiffuse, scene, textureDir, textureDict);
            }
            if (material.HasTextureDisplacement)
            {
                EmbedTexture(material.TextureDisplacement, scene, textureDir, textureDict);
            }
            if (material.HasTextureEmissive)
            {
                EmbedTexture(material.TextureEmissive, scene, textureDir, textureDict);
            }
            if (material.HasTextureHeight)
            {
                EmbedTexture(material.TextureHeight, scene, textureDir, textureDict);
            }
            if (material.HasTextureLightMap)
            {
                EmbedTexture(material.TextureLightMap, scene, textureDir, textureDict);
            }
            if (material.HasTextureNormal)
            {
                EmbedTexture(material.TextureNormal, scene, textureDir, textureDict);
            }
            if (material.HasTextureOpacity)
            {
                EmbedTexture(material.TextureOpacity, scene, textureDir, textureDict);
            }
            if (material.HasTextureReflection)
            {
                EmbedTexture(material.TextureReflection, scene, textureDir, textureDict);
            }
            if (material.HasTextureSpecular)
            {
                EmbedTexture(material.TextureSpecular, scene, textureDir, textureDict);
            }
        }


        private static void EmbedTexture(TextureSlot textureSlot, Scene scene, string textureDir, Dictionary<string, int> textureDict)
        {
            if (textureSlot.FilePath != null)
            {
                string filepath = textureSlot.FilePath;
                if (filepath.Contains("\0")) return;

                if (textureDict.ContainsKey(filepath))
                {
                    textureSlot.FilePath = $"{textureDict[filepath]}\0" + filepath;
                }
                else
                {
                    Assembly assembly = Assembly.GetEntryAssembly();
                    string EmbedPath = assembly.GetManifestResourceNames().Where(x => x.EndsWith(textureDir.TrimEnd('.') + "." + filepath.Split('\\').Last().TrimStart('.'))).FirstOrDefault();
                    if (EmbedPath == null) return;

                    using (Stream stream = assembly.GetManifestResourceStream(EmbedPath))
                    {
                        int index = scene.TextureCount;
                        textureDict.Add(filepath, index);
                        byte[] bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, (int)stream.Length);
                        scene.Textures.Add(new EmbeddedTexture(Path.GetExtension(filepath).Trim('.'), bytes, filepath));
                        textureSlot.FilePath = $"{index}\0" + filepath;
                    }
                }

            }
        }

    }
}
