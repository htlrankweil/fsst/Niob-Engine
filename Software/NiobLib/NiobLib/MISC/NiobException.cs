﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib
{
    public class NiobException : Exception
    {
        private object causingObject;
        private Stressgrund problem;

        public object CausingObject
        {
            get { return causingObject; }
        }
        public Stressgrund Problem
        {
            get { return problem; }
        }


        public NiobException(object srcObject,Stressgrund stressgrund):base($"OHJE! {srcObject} macht Stress") //stress wegen fortnite
        { 
            this.causingObject = srcObject;
            problem = stressgrund;
        }
    }

    //Ausbaubar
    public enum Stressgrund
    {
        ItemExists,
        Stress,
        NoLootAtLootLake,
        OutOfRange,
        PortAlreadyOpen,
        PortNotFound,
        PortIsNotOpen,
        NoPortAvailable
    }

}
