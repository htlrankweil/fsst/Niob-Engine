﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NiobLib.MISC
{
    /// <summary>
    /// Interaction logic for ErrorWarning.xaml
    /// </summary>
    public partial class ErrorWarning : Window
    {
        public ErrorWarning(string errorText)
        {
            InitializeComponent();
            ErrorText = errorText;
        }
        public ErrorWarning()
        {
            InitializeComponent();
        }

        public string ErrorText { get { return tblock.Text; } set { tblock.Text = value; } }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
