﻿using HelixToolkit.SharpDX.Core;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NiobLib.Math
{
    static public class NiobMath
    {
        public static Vector3 Multiply(this Matrix3x3 left, Vector3 right)
        {
            return new Vector3(left.M11 * right.X + left.M21 * right.Y + left.M31 * right.Z,
                               left.M12 * right.X + left.M22 * right.Y + left.M32 * right.Z,
                               left.M13 * right.X + left.M23 * right.Y + left.M33 * right.Z);
        }

        public static Vector3 Rotate(this Matrix3x3 left, Vector3 right)
        {
            return new Vector3(left.M11 * right.X + left.M21 * right.Y + left.M31 * right.Z,
                               left.M12 * right.X + left.M22 * right.Y + left.M32 * right.Z,
                               left.M13 * right.X + left.M23 * right.Y + left.M33 * right.Z);
        }

        //ony works if matrix is a pure rotation matrix
        public static Vector3 InvRot(this Matrix3x3 left, Vector3 right)
        {
            return new Vector3(left.M11 * right.X + left.M12 * right.Y + left.M13 * right.Z,
                               left.M21 * right.X + left.M22 * right.Y + left.M23 * right.Z,
                               left.M31 * right.X + left.M32 * right.Y + left.M33 * right.Z);
        }


        public static Vector3 Rotate(this Matrix left, Vector3 right)
        {
            return new Vector3(left.M11 * right.X + left.M21 * right.Y + left.M31 * right.Z,
                               left.M12 * right.X + left.M22 * right.Y + left.M32 * right.Z,
                               left.M13 * right.X + left.M23 * right.Y + left.M33 * right.Z);
          
        }

        public static Vector3 Transform(this Matrix left, Vector3 right)
        {
            return new Vector3(left.M11 * right.X + left.M21 * right.Y + left.M31 * right.Z + left.M41,
                               left.M12 * right.X + left.M22 * right.Y + left.M32 * right.Z + left.M42,
                               left.M13 * right.X + left.M23 * right.Y + left.M33 * right.Z + left.M43);
        }


        public static Vector3 GetScale(this Matrix matrix)
        {
            return new Vector3(MathF.Sqrt(matrix.M11 * matrix.M11 + matrix.M12 * matrix.M12 + matrix.M13 * matrix.M13),
                               MathF.Sqrt(matrix.M21 * matrix.M21 + matrix.M22 * matrix.M22 + matrix.M23 * matrix.M23),
                               MathF.Sqrt(matrix.M31 * matrix.M31 + matrix.M32 * matrix.M32 + matrix.M33 * matrix.M33));
           
        }


        public static void Scale(this ref Matrix matrix, Vector3 scale)
        {
            matrix.M11 *= scale.X; matrix.M12 *= scale.X; matrix.M13 *= scale.X;
            matrix.M21 *= scale.Y; matrix.M22 *= scale.Y; matrix.M23 *= scale.Y;
            matrix.M31 *= scale.Z; matrix.M32 *= scale.Z; matrix.M33 *= scale.Z;
        }


        public static Quaternion Multiply(this Vector3 right, Quaternion left)
        {
            return new Quaternion(right, 0) * left;
        }


        public static Vector3 Rotate(this Quaternion q, Vector3 v)
        {
            float num1 = q.X * q.X;
            float num2 = q.Y * q.Y;
            float num3 = q.Z * q.Z;
            float num4 = q.X * q.Y;
            float num5 = q.Z * q.W;
            float num6 = q.Z * q.X;
            float num7 = q.Y * q.W;
            float num8 = q.Y * q.Z;
            float num9 = q.X * q.W;

            return new Vector3(v.X * (1f - 2f * (num2 + num3)) + v.Y * (2f * (num4 - num5)) + v.Z * (2f * (num6 + num7)),
                               v.X * (2f * (num4 + num5)) + v.Y * (1f - 2f * (num3 + num1)) + v.Z * (2f * (num8 - num9)),
                               v.X * (2f * (num6 - num7)) + v.Y * (2f * (num8 + num9)) + v.Z * (1f - 2f * (num2 + num1)));
        }

        public static Vector3 InvRotate(this Quaternion q, Vector3 v)
        {
            q = Quaternion.Invert(q);
            float num1 = q.X * q.X;
            float num2 = q.Y * q.Y;
            float num3 = q.Z * q.Z;
            float num4 = q.X * q.Y;
            float num5 = q.Z * q.W;
            float num6 = q.Z * q.X;
            float num7 = q.Y * q.W;
            float num8 = q.Y * q.Z;
            float num9 = q.X * q.W;

            return new Vector3(v.X * (1f - 2f * (num2 + num3)) + v.Y * (2f * (num4 - num5)) + v.Z * (2f * (num6 + num7)),
                               v.X * (2f * (num4 + num5)) + v.Y * (1f - 2f * (num3 + num1)) + v.Z * (2f * (num8 - num9)),
                               v.X * (2f * (num6 - num7)) + v.Y * (2f * (num8 + num9)) + v.Z * (1f - 2f * (num2 + num1)));
        }


        public static Vector3 GetNormal(this Vector3 v, Vector3 dir)
        {
            float numX = v.Y * dir.Z - v.Z * dir.Y;
            float numY = v.Z * dir.X - v.X * dir.Z;
            float numZ = v.X * dir.Y - v.Y * dir.X;

            return new Vector3(numY * v.Z - numZ * v.Y,
                               numZ * v.X - numX * v.Z,
                               numX * v.Y - numY * v.X);
        }


        public static bool NearEqual(Vector3 a, Vector3 b)
        {
            Vector3 d = a - b;
            return (d.X < 1e-4f) && (d.X > -1e-4f) &&
                   (d.Y < 1e-4f) && (d.Y > -1e-4f) &&
                   (d.Z < 1e-4f) && (d.Z > -1e-4f);
        }

        public static void Barycentric(Vector3 p, Vector3 a, Vector3 b, Vector3 c, out float v, out float w, out float u)
        {
            Vector3 v0 = b - a, v1 = c - a, v2 = p - a;
            float d00 = Vector3.Dot(v0, v0);
            float d01 = Vector3.Dot(v0, v1);
            float d11 = Vector3.Dot(v1, v1);
            float d20 = Vector3.Dot(v2, v0);
            float d21 = Vector3.Dot(v2, v1);
            float denom = d00 * d11 - d01 * d01;
            v = (d11 * d20 - d01 * d21) / denom;
            w = (d00 * d21 - d01 * d20) / denom;
            u = 1.0f - v - w;
        }

        public static Quaternion RotateBy(this Quaternion q, Vector3 w)
        {
            float phi = w.Length();
            if (phi <= 000.5f)
                return Quaternion.Normalize(q + 0.5f * w.Multiply(q));
            else
                return Quaternion.Normalize(q + 1 / phi * w.Multiply(q));

        }


        public static Quaternion RotateBy(this Quaternion q, Vector3 w,float time)
        {
            float phi = w.Length();
            if (phi * time <= 000.5f)
                return Quaternion.Normalize(q + time * 0.5f * w.Multiply(q));
            else
                return Quaternion.Normalize(q + 1 / phi * w.Multiply(q));
        }

        public static void Decompose2Sharp(this Assimp.Matrix4x4 matrix, out Matrix3x3 ScaleScew, out Quaternion rotation, out Vector3 translation)
        {
            translation.X = matrix.A4;
            translation.Y = matrix.B4;
            translation.Z = matrix.C4;

            Vector3 X = new Vector3(matrix.A1, matrix.B1, matrix.C1);
            float sizex = X.Length();
            X /= sizex;

            ScaleScew.M11 = sizex;
            ScaleScew.M12 = 0;
            ScaleScew.M13 = 0;


            Vector3 Y1 = new Vector3(matrix.A2, matrix.B2, matrix.C2);
            float yx = Vector3.Dot(X, Y1);
            Vector3 Y = X.GetNormal(Y1).Normalized();
            float yy = Vector3.Dot(Y, Y1);

            ScaleScew.M21 = yx;
            ScaleScew.M22 = yy;
            ScaleScew.M23 = 0;


            Vector3 Z1 = new Vector3(matrix.A3, matrix.B3, matrix.C3);
            Vector3 Z = Vector3.Cross(X, Y);
            float zx = Vector3.Dot(X, Z1);
            float zy = Vector3.Dot(Y, Z1);
            float zz = Vector3.Dot(Z, Z1);

            ScaleScew.M31 = zx;
            ScaleScew.M32 = zy;
            ScaleScew.M33 = zz;


            Matrix3x3 rotmatrix = new Matrix3x3(X.X, X.Y, X.Z,
                                                Y.X, Y.Y, Y.Z,
                                                Z.X, Z.Y, Z.Z);
            Quaternion.RotationMatrix(ref rotmatrix, out rotation);

        }


    }


    public struct Simplex
    {
        public Vector3 a, oa;
        public Vector3 b, ob;
        public Vector3 c, oc;
        public Vector3 d, od;
        public int l;

        public Simplex()
        {
            a = Vector3.Zero;
            b = Vector3.Zero;
            c = Vector3.Zero;
            d = Vector3.Zero;
            oa = Vector3.Zero;
            ob = Vector3.Zero;
            oc = Vector3.Zero;
            od = Vector3.Zero;
            l = 0;
        }

        public bool Add(Vector3 v, Vector3 ov, ref Vector3 dir)
        {
            switch (l)
            {
                case 0:
                    return Add0Dimension(v, ov, ref dir);
                case 1:
                    return Add1Dimension(v, ov, ref dir);
                case 2:
                    return Add2Dimension(v, ov, ref dir);
                case 3:
                    return Add3Dimension(v, ov, ref dir);
                default:
                    {

                        //Vector3 ac = a - c;
                        //Vector3 bc = b - c;
                        //Vector3 da = d - a;
                        //Vector3 db = d - b;

                        //Vector3 dab = Vector3.Cross(da, db);
                        //Vector3 dac = Vector3.Cross(da, ac);
                        //Vector3 dbc = Vector3.Cross(db, bc);
                        //Vector3 abc = Vector3.Cross(ac, bc);


                        //bool IDAB = Vector3.Dot(dab, c) > 0;
                        //bool IDAC = Vector3.Dot(dac, b) > 0;
                        //bool IDBC = Vector3.Dot(dbc, a) > 0;
                        //bool IABC = Vector3.Dot(abc, d) > 0;

                        //bool IsDAB = Vector3.Dot(dab, d) < 0 ^ IDAB;
                        //bool IsDAC = Vector3.Dot(dac, d) < 0 ^ IDAC;
                        //bool IsDBC = Vector3.Dot(dbc, d) < 0 ^ IDBC;
                        //bool IsABC = Vector3.Dot(abc, a) < 0 ^ IABC;

                        //if (!(IsDAB || IsDAC || IsDBC || IsABC))
                        //{
                        //    return true;
                        //}
                        break;
                    }
            }

            return true;
        }

        private bool Add0Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
        {
            a = v;
            oa = ov;
            dir = -a;
            l = 1;
            return false;
        }

        private bool Add1Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
        {
            b = v;
            ob = ov;
            dir = -(a - b).GetNormal(b);
            l = 2;
            return false;
        }

        private bool Add2Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
        {
            c = v;
            oc = ov;
            Vector3 ac = a - c;
            Vector3 bc = b - c;
            Vector3 abc = Vector3.Cross(ac, bc);
            
            //Origin lies in C
            //if (Vector3.Dot(Vector3.Cross(bc, abc), c) > 0)
            if (Vector3.Dot(ac.GetNormal(bc), c) > 0)
            {
                b = c;
                ob = oc;
                dir = -(a - b).GetNormal(b);
                return false;
            }

            //Origin lies in B
            //if (Vector3.Dot(Vector3.Cross(abc, ac), c) > 0)
            if (Vector3.Dot(bc.GetNormal(ac), c) > 0)
            {
                a = c;
                oa = oc;
                dir = -(a - b).GetNormal(b);
                return false;
            }

            if (Vector3.Dot(abc, a) < 0)
                dir = abc;
            else
                dir = -abc;

            l = 3;
            return false;
        }

        private bool Add3Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
        {
            d = v;
            od = ov;
            Vector3 ad = d - a;
            Vector3 bd = d - b;
            Vector3 cd = d - c;

            Vector3 dab = Vector3.Cross(ad, bd);
            Vector3 dac = Vector3.Cross(ad, cd);
            Vector3 dbc = Vector3.Cross(bd, cd);

            bool IDAB = Vector3.Dot(dab, cd) < 0;
            bool IDAC = Vector3.Dot(dac, bd) < 0;
            bool IDBC = Vector3.Dot(dbc, ad) < 0;

            bool IsDAB = Vector3.Dot(dab, d) < 0 ^ IDAB;
            bool IsDAC = Vector3.Dot(dac, d) < 0 ^ IDAC;
            bool IsDBC = Vector3.Dot(dbc, d) < 0 ^ IDBC;

            //Is over Triangle DAB
            if (IsDAB)
            {
                //if (IsDAC)
                //{
                //    b = d;
                //    dir = -(a - b).GetNormal(b);
                //    l = 2;
                //    return false;
                //}

                //if (IsDBC)
                //{
                //    a = d;
                //    l = 2;
                //    dir = -(a - b).GetNormal(b);
                //    return false;
                //}

                c = d;
                oc = od;

                if (IDAB)
                    dir = -dab;
                else
                    dir = dab;

                return false;
            }

            if (IsDAC)
            {
                //if (IsDAC && IsDBC)
                //{
                //    a = d;
                //    b = c;
                //    l = 2;
                //    dir = -(a - b).GetNormal(b);
                //    return false;
                //}

                b = d;
                ob = od;
                if (IDAC)
                    dir = -dac;
                else
                    dir = dac;

                return false;
            }

            if (IsDBC)
            {
                a = d;
                oa = od;
                if (IDBC)
                    dir = -dbc;
                else
                    dir = dbc;

                return false;
            }

            l = 4;
            return true;
        }
    }


}
