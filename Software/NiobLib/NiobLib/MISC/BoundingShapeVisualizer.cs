﻿using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using NiobLib.Entities;
using NiobLib.Physics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using static NiobLib.NiobScene;

namespace NiobLib.MISC
{
    public class BoundingShapeVisualizer:IDisposable
    {
        private List<Tuple<INiobEntity, IPhysicsObject>> boundingShapes = new List<Tuple<INiobEntity, IPhysicsObject>>();

        private bool visible;

        public bool Visible
        {
            get { return visible; }
            set { 
                visible = value;
                if (visible)
                {
                    foreach (var item in boundingShapes)
                    {
                        scene.Entities.Add(item.Item1);
                    }
                }
                else
                {
                    foreach (var item in boundingShapes)
                    {
                        scene.Entities.Remove(item.Item1);
                    }
                }
            }
        }

        private NiobScene scene;


        public BoundingShapeVisualizer(NiobScene scene)
        {
            this.scene = scene;
            visible = true;
            scene.TickEvent += UpdatePositions;
            UpdateShapes();
        }


        public void UpdateShapes()
        {
            foreach (var item in boundingShapes)
            {
                scene.Entities.Remove(item.Item1);
            }

            boundingShapes.Clear();

            HashSet<IPhysicsObject> pObjects = new HashSet<IPhysicsObject>();
            foreach (var group in scene.Physics.PhysicsGroups)
            {
                foreach (var entity in group)
                {
                    pObjects.Add(entity);
                }
            }

            foreach (var pObject in pObjects)
            {
                if (GetNode(pObject.BoundingShape) is SceneNode node)
                {
                    INiobEntity entity = new NiobEntity3d(node);
                    boundingShapes.Add(Tuple.Create(entity, pObject));
                    if (Visible)
                    {
                        scene.Entities.Add(entity);
                    }
                }
            }
        }

        private static SceneNode GetNode(IBoundingShape shape)
        {
            switch (shape)
            {
                case ConvexMeshBounding convexMeshBounding:
                    return GetMeshBoundingNode(convexMeshBounding);
                case MultiBounding multiBounding:
                    GroupNode groupNode = new GroupNode();
                    foreach (var positionedShape in multiBounding.Shapes)
                    {
                        if (GetNode(positionedShape.BoundingShape) is SceneNode node)
                        {
                            SharpDX.Matrix matrix = SharpDX.Matrix.RotationQuaternion(positionedShape.Rotation);
                            matrix.TranslationVector = positionedShape.Position;
                            node.ModelMatrix = matrix;
                            groupNode.AddChildNode(node);
                        }
                    }
                    return groupNode;
                case EllipsoidBounding ellipsoidBounding:
                    {
                        LineNode lineNode = new LineNode();
                        lineNode.Visible = true;
                        LineBuilder lineBuilder = new LineBuilder();
                        lineBuilder.AddCircle(Vector3.Zero, new Vector3(1, 0, 0), 0.5f, 20);
                        lineBuilder.AddCircle(Vector3.Zero, new Vector3(0, 1, 0), 0.5f, 20);
                        lineBuilder.AddCircle(Vector3.Zero, new Vector3(0, 0, 1), 0.5f, 20);
                        lineNode.Geometry = lineBuilder.ToLineGeometry3D();
                        for (int i = 0; i < lineNode.Geometry.Positions.Count; i++)
                        {
                            lineNode.Geometry.Positions[i] *= ellipsoidBounding.Size;
                        }
                        lineNode.Material = new LineMaterial() { Color = Colors.Orange, Thickness = 1, FixedSize = true, EnableDistanceFading = false };
                        return lineNode;
                    }
                case CuboidBounding cuboidBounding:
                    {
                        LineNode lineNode = new LineNode();
                        lineNode.Visible = true;
                        LineBuilder lineBuilder = new LineBuilder();
                        lineBuilder.AddBox(Vector3.Zero,cuboidBounding.Size.X,cuboidBounding.Size.Y,cuboidBounding.Size.Z);
                        lineNode.Geometry = lineBuilder.ToLineGeometry3D();
                        lineNode.Material = new LineMaterial() { Color = Colors.Green, Thickness = 1, FixedSize = true, EnableDistanceFading = false };
                        return lineNode;
                    }
                case CylinderBounding cylinderBounding:
                    {
                        LineNode lineNode = new LineNode();
                        lineNode.Visible = true;
                        LineBuilder lineBuilder = new LineBuilder();
                        switch (cylinderBounding.Axis)
                        {
                            case Axis.X:
                                lineBuilder.AddCircle(new Vector3(0.5f, 0, 0), new Vector3(1, 0, 0), 0.5f, 20);
                                lineBuilder.AddCircle(new Vector3(-0.5f, 0, 0), new Vector3(1, 0, 0), 0.5f, 20);

                                lineBuilder.AddLine(new Vector3(0.5f, 0.5f, 0), new Vector3(-0.5f, 0.5f, 0));
                                lineBuilder.AddLine(new Vector3(0.5f, -0.5f, 0), new Vector3(-0.5f, -0.5f, 0));

                                lineBuilder.AddLine(new Vector3(0.5f, 0, 0.5f), new Vector3(-0.5f, 0, 0.5f));
                                lineBuilder.AddLine(new Vector3(0.5f, 0, -0.5f), new Vector3(-0.5f, 0, -0.5f));
                                break;
                            case Axis.Y:
                                lineBuilder.AddCircle(new Vector3(0, 0.5f, 0), new Vector3(0, 1, 0), 0.5f, 20);
                                lineBuilder.AddCircle(new Vector3(0, -0.5f, 0), new Vector3(0, 1, 0), 0.5f, 20);

                                lineBuilder.AddLine(new Vector3(0.5f, 0.5f, 0), new Vector3(0.5f, -0.5f, 0));
                                lineBuilder.AddLine(new Vector3(-0.5f, 0.5f, 0), new Vector3(-0.5f, -0.5f, 0));

                                lineBuilder.AddLine(new Vector3(0, 0.5f, 0.5f), new Vector3(0, -0.5f, 0.5f));
                                lineBuilder.AddLine(new Vector3(0, 0.5f, -0.5f), new Vector3(0, -0.5f, -0.5f));
                                break;
                            case Axis.Z:
                                lineBuilder.AddCircle(new Vector3(0, 0, 0.5f), new Vector3(0, 0, 1), 0.5f, 20);
                                lineBuilder.AddCircle(new Vector3(0, 0, -0.5f), new Vector3(0, 0, 1), 0.5f, 20);

                                lineBuilder.AddLine(new Vector3(0, 0.5f, 0.5f), new Vector3(0, 0.5f, -0.5f));
                                lineBuilder.AddLine(new Vector3(0, -0.5f, 0.5f), new Vector3(0, -0.5f, -0.5f));

                                lineBuilder.AddLine(new Vector3(0.5f, 0, 0.5f), new Vector3(0.5f, 0, -0.5f));
                                lineBuilder.AddLine(new Vector3(-0.5f, 0, 0.5f), new Vector3(-0.5f, 0, -0.5f));
                                break;
                            default:
                                break;
                        }
                        lineNode.Geometry = lineBuilder.ToLineGeometry3D();
                        for (int i = 0; i < lineNode.Geometry.Positions.Count; i++)
                        {
                            lineNode.Geometry.Positions[i] *= cylinderBounding.Size;
                        }
                        lineNode.Material = new LineMaterial() { Color = Colors.Yellow, Thickness = 1, FixedSize = true ,EnableDistanceFading= false};
                        return lineNode;
                    }
                default:
                    break;
            }
            throw null;
        }

        public static LineNode GetMeshBoundingNode(ConvexMeshBounding convexMeshBounding)
        {
            LineNode lineNode = new LineNode();
            lineNode.Visible = true;
            List <Vector3> unusedVerts = convexMeshBounding.MeshVertexes.Distinct().ToList();
            if (unusedVerts.Count >= 4)
            {

                List<Vector3> usedVerts = new List<Vector3>();
                usedVerts.Add(convexMeshBounding.GetSupport(new Vector3(1, 0, 0)));
                usedVerts.Add(convexMeshBounding.GetSupport(new Vector3(-1, 1, 0)));
                usedVerts.Add(convexMeshBounding.GetSupport(new Vector3(-1, -1, -1)));
                usedVerts.Add(convexMeshBounding.GetSupport(new Vector3(-1, -1, 1)));
                unusedVerts.Remove(usedVerts[0]);
                unusedVerts.Remove(usedVerts[1]);
                unusedVerts.Remove(usedVerts[2]);
                unusedVerts.Remove(usedVerts[3]);

                Vector3 mid = (usedVerts[0] + usedVerts[1] + usedVerts[2] + usedVerts[3]) / 4;
                List<Tuple<int, int, int, Vector3>> tris = new List<Tuple<int, int, int, Vector3>>();

                Vector3 u1 = usedVerts[1] - usedVerts[0];
                Vector3 v1 = usedVerts[2] - usedVerts[0];
                Vector3 c1 = Vector3.Cross(u1, v1);
                float d1 = Vector3.Dot(usedVerts[0] - mid, c1);
                if (d1 > 0)//counter clock wise
                {
                    //0-1-2
                    tris.Add(Tuple.Create(0, 1, 2, c1));

                    //1-0-3
                    Vector3 u2 = usedVerts[0] - usedVerts[1];
                    Vector3 v2 = usedVerts[3] - usedVerts[1];
                    Vector3 c2 = Vector3.Cross(u2, v2);

                    tris.Add(Tuple.Create(1, 0, 3, c2));

                    //3-0-2
                    Vector3 u3 = usedVerts[0] - usedVerts[3];
                    Vector3 v3 = usedVerts[2] - usedVerts[3];
                    Vector3 c3 = Vector3.Cross(u3, v3);

                    tris.Add(Tuple.Create(3, 0, 2, c3));

                    //1-3-2
                    Vector3 u4 = usedVerts[3] - usedVerts[1];
                    Vector3 v4 = usedVerts[2] - usedVerts[1];
                    Vector3 c4 = Vector3.Cross(u4, v4);

                    tris.Add(Tuple.Create(1, 3, 2, c4));
                }
                else
                {
                    //2-1-0
                    tris.Add(Tuple.Create(2, 1, 0, -c1));

                    //3-0-1
                    Vector3 u2 = usedVerts[0] - usedVerts[3];
                    Vector3 v2 = usedVerts[1] - usedVerts[3];
                    Vector3 c2 = Vector3.Cross(u2, v2);

                    tris.Add(Tuple.Create(3, 0, 1, c2));

                    //2-0-3
                    Vector3 u3 = usedVerts[0] - usedVerts[2];
                    Vector3 v3 = usedVerts[3] - usedVerts[2];
                    Vector3 c3 = Vector3.Cross(u3, v3);

                    tris.Add(Tuple.Create(2, 0, 3, c3));

                    //2-3-1
                    Vector3 u4 = usedVerts[3] - usedVerts[2];
                    Vector3 v4 = usedVerts[1] - usedVerts[2];
                    Vector3 c4 = Vector3.Cross(u4, v4);

                    tris.Add(Tuple.Create(2, 3, 1, c4));
                }

                

                List<Tuple<int, int>> uniqueEdges = new List<Tuple<int, int>>();
                uniqueEdges.EnsureCapacity(20);
                foreach (Vector3 p in unusedVerts)
                {
                    for (int i = 0; i < tris.Count; i++)
                    {
                        var tri = tris[i];
                        if (Vector3.Dot(tri.Item4, p - usedVerts[tri.Item1]) > 0)
                        {
                            tris.RemoveAt(i);
                            i--;

                            addIfUnique(uniqueEdges,Tuple.Create(tri.Item1, tri.Item2));
                            addIfUnique(uniqueEdges,Tuple.Create(tri.Item2, tri.Item3));
                            addIfUnique(uniqueEdges,Tuple.Create(tri.Item3, tri.Item1));
                        }
                    }

                    if (uniqueEdges.Count > 0)
                    {
                        foreach (var edge in uniqueEdges)
                        {
                            Vector3 u = usedVerts[edge.Item1] - usedVerts[edge.Item2];
                            Vector3 v = p - usedVerts[edge.Item2];
                            Vector3 c = Vector3.Cross(u, v);
                            tris.Add(Tuple.Create(edge.Item2, edge.Item1,usedVerts.Count,c));
                        }
                        usedVerts.Add(p);
                        uniqueEdges.Clear();
                    }
                }

                List<Tuple<int, int>> lines = new List<Tuple<int, int>>();
                foreach (var tri in tris)
                {
                    addUnique(lines, Tuple.Create(tri.Item1, tri.Item2));
                    addUnique(lines, Tuple.Create(tri.Item2, tri.Item3));
                    addUnique(lines, Tuple.Create(tri.Item3, tri.Item1));
                }

                LineBuilder lineBuilder = new LineBuilder();
                foreach (var line in lines)
                {
                    lineBuilder.AddLine(usedVerts[line.Item1], usedVerts[line.Item2]);
                }
                lineNode.Geometry = lineBuilder.ToLineGeometry3D();

            }
            else if (unusedVerts.Count >= 2)
            {
                LineBuilder lineBuilder = new LineBuilder();
                for (int i = 0; i < unusedVerts.Count; i++)
                {
                    lineBuilder.AddLine(unusedVerts[i], unusedVerts[(i+1)%unusedVerts.Count]);
                }
                lineNode.Geometry = lineBuilder.ToLineGeometry3D();
            }

            lineNode.Material = new LineMaterial() { Color = Colors.Red, Thickness = 1, FixedSize = true, EnableDistanceFading = false };
            return lineNode;
        }

        private static void addIfUnique(List<Tuple<int, int>> edges, Tuple<int,int> edge)
        {
            if (edges.Contains(edge))
            {
                edges.Remove(edge);
            }
            else
            {
                edges.Add(Tuple.Create(edge.Item2,edge.Item1));
            }
        }
        private static void addUnique(List<Tuple<int, int>> edges, Tuple<int, int> edge)
        {
            if (!edges.Contains(edge))
            {
                edges.Add(Tuple.Create(edge.Item2, edge.Item1));
            }
        }


        public void UpdatePositions(object source, TickEventArgs e)
        {
            if (visible)
            {
                foreach (var item in boundingShapes)
                {
                    item.Item1.Position = item.Item2.Position;
                    item.Item1.Rotation = item.Item2.Orientation;
                }
            }
        }

        public void Dispose()
        {
            Visible = false;
            scene.TickEvent -= UpdatePositions;
            boundingShapes.Clear();
        }
    }
}
