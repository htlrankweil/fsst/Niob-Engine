﻿using Microsoft.VisualBasic;
using NiobInput;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Markup;

namespace NiobLib.Input
{
    public class WASDKeyboardInput : IInput
    {
        private Vector3 intermediate;
        public object Intermediate
        {
            get { return intermediate; }
        }

        private long timestamp;
        public long Timestamp { get {return timestamp; } }

        private Key keyFW = Key.W;
        private Key keyBW = Key.S;
        private Key keyLeft = Key.A;
        private Key keyRight = Key.D;


        public WASDKeyboardInput(NiobViewPort hoveredPort, Key TasteFW, Key TasteBW, Key TasteLeft, Key TasteRight)
        {
            keyFW = TasteFW;
            keyBW = TasteBW;
            keyLeft = TasteLeft;
            keyRight = TasteRight;
            hoveredPort.KeyboardUpdate += update;
        }

        private void update(object? sender, EventArgs e)
        {
            intermediate = Vector3.Zero;    
            if (Keyboard.IsKeyDown(keyFW))
            {
                intermediate += new Vector3(1, 0, 0);
            }
            if (Keyboard.IsKeyDown(keyBW))
            {
                intermediate += new Vector3(-1, 0, 0);
            }
            if (Keyboard.IsKeyDown(keyLeft))
            {
                intermediate += new Vector3(0, 0, -1);
            }
            if (Keyboard.IsKeyDown(keyRight))
            {
                intermediate += new Vector3(0, 0, 1);
            }

            timestamp = Stopwatch.GetTimestamp();


            //Maybe normalize vector or something like that 
        }

    }
   
}
