﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace NiobInput
{
    public static class MegaInputTranslator
    {
        public static bool ButtonPressed(int data, int btNum)
        {
            return (data & (1 << btNum)) != 0;
        }

        public static bool ButtonPressed(int data, MEGAbuttons button)
        {
            return (data & ((int)button)) != 0;
        }

        public static bool ButtonPressed(this MEGAbuttons data, MEGAbuttons button)
        {
            return (((int)data) & ((int)button)) != 0;
        }

        public static IEnumerable<MEGAbuttons> GetButtons(int data)
        {
            for (int i = 1; i < 1<<18; i<<=1)
            {
                if ((data & i) != 0)
                    yield return (MEGAbuttons)i;
            }
        }

        public static IEnumerable<MEGAbuttons> GetButtons(this MEGAbuttons data)
        {
            for (int i = 1; i < 1<<18; i<<=1)
            {
                if ((((int)data) & i) != 0)
                    yield return (MEGAbuttons)i;
            }
        }

        public static MEGAbuttons getButton(int btNum)
        {
            return (MEGAbuttons)(1 << btNum);
        }

        public static int getBtNum(MEGAbuttons button)
        {
            int btNum = 0;
            for (int i = 1; i < 1 << 18; i <<= 1)
            {
                if ((((int)button) & i) != 0)
                    return btNum;
                btNum++;
            }
            return -1;
        }

    }

    public enum MEGAbuttons : int
    {
        s0 = 1 << 0,
        s1 = 1 << 1,
        s2 = 1 << 2,
        s3 = 1 << 3,
        s4 = 1 << 4,
        s5 = 1 << 5,
        s6 = 1 << 6,
        s7 = 1 << 7,
        s8 = 1 << 8,
        s9 = 1 << 9,
        s10 = 1 << 10,
        s11 = 1 << 11,
        s12 = 1 << 12,
        s13 = 1 << 13,
        s14 = 1 << 14,
        s15 = 1 << 15,
        s16 = 1 << 16,
        s17 = 1 << 17
    }
}
