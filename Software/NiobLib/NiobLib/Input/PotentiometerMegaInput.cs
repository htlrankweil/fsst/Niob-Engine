﻿using NiobInput;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NiobInput.MEGACard_Input;

namespace NiobLib.Input
{
    public class PotentiometerMegaInput:IInput
    {
        private double intermediate;
        public object Intermediate
        {
            get { return intermediate; }
        }

        private long timestamp;
        public long Timestamp { get { return timestamp; } }

        private int res;
        private int channel;

        public PotentiometerMegaInput(MEGACard_Input input, int channel, int maxval = 1023)
        {
            res = maxval;
            input.DataRcv += update;
            this.channel = channel;
        }


        private void update(object? sender, DataRcvEventArgs e)
        {
            if (channel != e.Channel) return;

            intermediate = e.Data/((double)res);
            timestamp = Stopwatch.GetTimestamp();
        }
    }
}
