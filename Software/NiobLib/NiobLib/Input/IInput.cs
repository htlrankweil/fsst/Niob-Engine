﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Input
{
    public interface IInput
    {
        public object Intermediate { get; }
        public long Timestamp { get; }
    }
}
