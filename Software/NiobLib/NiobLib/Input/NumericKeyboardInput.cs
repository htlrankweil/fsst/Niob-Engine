﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace NiobLib.Input
{
    public class NumericKeyboardInput : IInput
    {
        private double intermediate;
        public object Intermediate
        {
            get { return intermediate; }
        }

        private double incSpeed;

        public double IncSpeed
        {
            get { return incSpeed; }
            set { incSpeed = value; }
        }


        private long timestamp;
        public long Timestamp { get { return timestamp; } }

        private Key keyInc = Key.W;
        private Key keyDec = Key.S;


        private int laststate;

        private DispatcherTimer Timer;

        public NumericKeyboardInput(NiobViewPort hoveredPort, Key TasteInc, Key TasteDec, double incspeed, double startVal)
        {
            keyInc = TasteInc;
            keyDec = TasteDec;
            incSpeed = incspeed;
            Timer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 50) };
            Timer.Tick += UpdateCount;
            Timer.Start();
            intermediate = startVal;
            laststate = 0;
            timestamp = Stopwatch.GetTimestamp();
            hoveredPort.KeyboardUpdate += update;
        }

        private void UpdateCount(object? sender, EventArgs e)
        {
            double i = intermediate + laststate * incSpeed / 20;
            if (i > 1) i = 1;
            else if (i < 0) i = 0;
            intermediate = i;
        }


        private void update(object? sender, EventArgs e)
        {
            int i = 0;
            if (Keyboard.IsKeyDown(keyInc))
            {
                i += 1;
            }
            if (Keyboard.IsKeyDown(keyDec))
            {
                i -= 1;
            }
            laststate = i;
            timestamp = Stopwatch.GetTimestamp();
            //Maybe normalize vector or something like that 
        }
    }
}
