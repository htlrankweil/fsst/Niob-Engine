﻿using Microsoft.VisualBasic;
using NiobInput;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using static NiobInput.MEGACard_Input;

namespace NiobLib.Input
{
    public class WASDMegaInput:IInput
    {
        private Vector3 intermediate;
        public object Intermediate
        {
            get { return intermediate; }
        }

        private long timestamp;
        public long Timestamp { get { return timestamp; } }

        private int btFW;
        private int btBW;
        private int btLeft;
        private int btRight;
        private int channel;

        public WASDMegaInput(MEGACard_Input input, int channel, MEGAbuttons TasteFW = MEGAbuttons.s0, MEGAbuttons TasteBW = MEGAbuttons.s1, MEGAbuttons TasteLeft = MEGAbuttons.s2, MEGAbuttons TasteRight = MEGAbuttons.s3)
        {
            btFW = ((int)TasteFW);
            btBW = ((int)TasteBW);
            btLeft = ((int)TasteLeft);
            btRight = ((int)TasteRight);
            input.DataRcv += update;
            this.channel = channel;
        }


        private void update(object? sender, DataRcvEventArgs e)
        {
            if (channel != e.Channel) return;

            intermediate = Vector3.Zero;
            if ((e.Data & btFW)>0)
            {
                intermediate += new Vector3(1, 0, 0);
            }
            if ((e.Data & btBW) > 0)
            {
                intermediate += new Vector3(-1, 0, 0);
            }
            if ((e.Data & btLeft) > 0)
            {
                intermediate += new Vector3(0, 0, -1);
            }
            if ((e.Data & btRight) > 0)
            {
                intermediate += new Vector3(0, 0, 1);
            }
            timestamp = Stopwatch.GetTimestamp();
        }
    }
   
}
