﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using HelixToolkit.SharpDX;

namespace NiobLib.Input
{
    public class MouseInput
    {
        /// <summary>
        /// When setting Funcs manually, make sure to set it to the position of the enum System.Windows.Input.Key
        /// </summary>
        public Func<bool>[] funcs;

        public MouseInput(ref NiobViewPort viewPort, ref GameLoop gameLoopForKeyRepeat)
        {
            viewPort.MouseDown += NiobMouseDownDetected;
            viewPort.MouseUp += NiobMouseUpDetected;
            viewPort.MouseWheel += NiobMouseWheelDetected;
            viewPort.MouseMove += NiobMouseMoveDetected;
            funcs = new Func<bool>[173];
            gameLoopForKeyRepeat.AddEventHandler(InputLoop);
        }

        public bool SetKey(Key key, Func<bool> functionToApply)
        {
            try
            {
                funcs[(int)key] = functionToApply;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void NiobMouseDownDetected(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void NiobMouseUpDetected(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void NiobMouseWheelDetected(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }

        private void NiobMouseMoveDetected(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void InputLoop(object sender, EventArgs e)
        {

        }
    }
}
