﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using HelixToolkit.SharpDX;

namespace NiobLib.Input
{
    public class KeyboardInput
    {
        /// <summary>
        /// When setting Funcs manually, make sure to set it to the position of the enum System.Windows.Input.Key
        /// </summary>
        public Func<bool>[] funcs;
        public bool[] funcActive;
        public DateTime[] funcsCallTime;

        public KeyboardInput(ref NiobViewPort viewPort, ref GameLoop gameLoopForKeyRepeat)
        {
            viewPort.KeyDown += NiobInputDetected;
            funcs = new Func<bool>[173];
            funcActive = new bool[173];
            funcsCallTime = new DateTime[173];
            gameLoopForKeyRepeat.AddEventHandler(InputLoop);
        }

        public bool SetKey(Key key, Func<bool> functionToApply)
        {
            try
            {
                funcs[(int)key] = functionToApply;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void NiobInputDetected(object sender, KeyEventArgs e)
        {
            Key key = e.Key;
            funcs[((int)key)]();

            e.Handled = true;
        }

        private void InputLoop(object sender, EventArgs e)
        {

        }
    }
}
