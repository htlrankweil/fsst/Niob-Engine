﻿using NiobLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using static NiobInput.MEGACard_Input;


namespace NiobInput
{
    public class MEGACard_Input
    {
        public event RcvDataEventHandler DataRcv;
        private string[] ports;
        private SerialPort sPort;
        private string port;


        /// <summary>
        /// Returns the opening/open port
        /// </summary>
        public string Port
        {
            get { return port; }
        }


        /// <summary>
        /// Returns the Ports of the last 'Measurement'
        /// </summary>
        public string[] Ports
        {
            get { return ports; }
        }

        public bool IsConnected
        {
            get { return (sPort is not null) && (sPort.IsOpen); }
        }


        #region Event

        public bool MSBFirst { get; private set; }

        public class DataRcvEventArgs : EventArgs
        {
            public int Data { get; set; }
            public int Channel { get; set; }
        }

        public delegate void RcvDataEventHandler(object sender, DataRcvEventArgs e);
        protected virtual void OnCompleteDatapackageRcv(int channel, int data)
        {
            DataRcv?.Invoke(this, new DataRcvEventArgs() { Channel = channel, Data = data});
        }
        #endregion

        


        /// <summary>
        /// Contains a port-available-check, a GetAvailaplePorts() and a Connect(comOfMEGACard)
        /// <br>Do not forget to define what should happen when a channel recieves data (SetFunctionToExecu...)</br>
        /// </summary>
        /// <param name="comOfMEGACard"></param>
        /// <param name="msbFirst"></param>
        /// <exception cref="NiobPortNotFoundException"></exception>
        public MEGACard_Input(string comOfMEGACard, bool msbFirst = false)
        {
            MSBFirst = msbFirst;
            port = comOfMEGACard;
            sPort = new SerialPort(); //new SerialPort("COMxy", 9600, Parity.None, 8, StopBits.One);
            if (GetAvailablePorts().Contains(Port))
            {

                Connect(Port);
                OpenDataMan();
            }
        }


        private bool datamanrunning = false;
        private void OpenDataMan()
        {
            if (datamanrunning) return;
            datamanrunning = true;
            Thread tread = new Thread(DataMan);
            tread.Start();
        }

        private void DataMan()
        {
            try
            {

                byte[] buffer = new byte[64];
                int count = 0;
                byte[] data = new byte[4];
                while (sPort.IsOpen)
                {
                    datamanrunning = true;
                    //int len = serial.Read(buffer, 0, buffer.Length);
                    int len = sPort.Read(buffer, 0, buffer.Length);
                    for (int i = 0; i < len; i++)
                    {
                        int bitnum = buffer[i] >> 6;
                        if (bitnum == count)
                        {
                            data[count] = (byte)(buffer[i] & 0x3F);

                            if (count == 3)
                            {
                                count = 0;

                                int rcvData = 0;
                                int channel = data[0];
                                if (MSBFirst)
                                {
                                    rcvData = data[1] << 12 | data[2] << 6 | data[3];
                                }
                                else
                                {
                                    rcvData = data[3] << 12 | data[2] << 6 | data[1];
                                }

                                OnCompleteDatapackageRcv(channel, rcvData);
                            }
                            else
                            {
                                count++;
                            }
                        }
                        else
                        {
                            count = 0;
                            if (bitnum == 0)
                            {
                                data[count] = (byte)(buffer[i] & 0x3F);
                                count++;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                datamanrunning = false;
            }
            datamanrunning = false;
        }


        public void Connect(string port)
        {
            try
            {
                sPort.Close();
                sPort.PortName = Port;
                sPort.BaudRate = 9600;
                sPort.Parity = Parity.None;
                sPort.DataBits = 8;
                sPort.StopBits = StopBits.One;
                //sPort.ReadTimeout
                sPort.Open();
                OpenDataMan();
            }
            catch
            {
                throw new NiobException(sPort,Stressgrund.PortNotFound);
            }
        }

        public string[] GetAvailablePorts()
        {
            ports = SerialPort.GetPortNames();
            return ports;
        }


        public void Disconnect()
        {
            if (sPort.IsOpen)
            {
                sPort.Close();
            }
            else
            {
                throw new NiobException(sPort, Stressgrund.PortIsNotOpen);
            }
        }

        /// <summary>
        /// Verbindet automatisch mit dem 
        /// Wichtig: Automatische Verbindung ist nur erfolgreich, wenn Gerät, mit dem man sich verbinden will, bei Funktionsaufrauf schon angeschlossen ist
        /// </summary>
        /// <returns>Port, bei dem die Verbindung erfolgreich war</returns>
        /// <exception cref="InvalidOperationException"></exception>
        public string AutoConnectToFirstAvailableSerialPort(string[] excludedPorts)
        {
            GetAvailablePorts();
            foreach (string port in Ports)
            {
                if (!excludedPorts.Contains(port))
                {
                    try
                    {
                        sPort.PortName = port;
                        sPort.Open();
                        OpenDataMan();
                        return port;
                    }
                    catch
                    {
                        // Do nothing, try next port
                    }
                }
            }
            throw new NiobException(sPort,Stressgrund.NoPortAvailable);
        }
    }
}

//// template
//private static void MEGACardNewInput(string com)
//{
//    MEGACard_Input megaInput = new MEGACard_Input(com, true);
//    megaInput.CompleteDataPackageRcv += OnCompleteDatapackageRcv;
//}

//private static void OnCompleteDatapackageRcv(object sender, EventArgs e)
//{
//    MEGACard_Input.DataAllRecievedEventArgs eventArgs = e as MEGACard_Input.DataAllRecievedEventArgs;
//    // Write what to do on data recieved here
//}

