﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Controls;
using NiobInput;
using NiobLib.Entities;
 

namespace NiobLib
{
    [ContentProperty("Scene")]
    public class NiobViewPort:Viewport3DX
    {
        private SceneNodeGroupModel3D sceneNodeModel;

        private NiobScene? scene;

        private NiobCamera cam;


        public NiobCamera Cam
        {
            get { return cam; }
            set
            {
                cam = value;
                Camera = value.HelixCamera;
            }
        }

        public NiobScene? Scene 
        { 
            get { return scene; } 
            set 
            {
                if (scene is not null)
                    scene.Entities.CollectionChanged -= Entities_CollectionChanged;
                scene = value;
                sceneNodeModel.Clear();
                if (scene is not null)
                {
                    scene.Entities.CollectionChanged += Entities_CollectionChanged;

                    foreach (INiobEntity item in scene.Entities)
                    {
                        sceneNodeModel.AddNode(item.Node);
                    }
                }
            }
        }

        private void Entities_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems is not null)
            {
                foreach (INiobEntity item in e.OldItems)
                {
                    sceneNodeModel.RemoveNode(item.Node);
                }
            }

            if (e.NewItems is not null)
            {
                foreach (INiobEntity item in e.NewItems)
                {
                    sceneNodeModel.AddNode(item.Node);
                }
            }

        }

        public NiobViewPort():this(new NiobCamera())
        {
             
        }

        public NiobViewPort(NiobCamera cam)
        {
            EffectsManager = new DefaultEffectsManager();
            sceneNodeModel = new SceneNodeGroupModel3D();
            Items.Add(sceneNodeModel);
            CompositionTargetEx compTarget = new CompositionTargetEx();
            compTarget.Rendering += OnRender;
            Cam = cam;
            //InputBindings.Clear();           
            PreviewKeyDown += NiobViewPort_KeyUpdate;
            PreviewKeyUp += NiobViewPort_KeyUpdate;
        }

        private void NiobViewPort_KeyUpdate(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (KeyboardUpdate != null)
            {
                KeyboardUpdate(this, e);
            }
            e.Handled = true;
        }

        public event EventHandler KeyboardUpdate;



        //current time, 
        private void OnRender(object? sender, System.Windows.Media.RenderingEventArgs e)
        {
            long tickLength = scene.GameLoop.GameLoopTicksSpeed.Ticks;
            long lastTick = scene.GameLoop.LastTick;
            foreach (INiobEntity item in scene.Entities)
            {
                item.Render(tickLength, lastTick);
            }
        }





        #region Hide stuff from VS

        //[EditorBrowsable (EditorBrowsableState.Never)]


        #endregion
        //public static readonly DependencyProperty butrusProperty = DependencyProperty.Register("butrusProperty", typeof(NiobPlane), typeof(NiobViewPort));
        /*
        // Dependency Property
        public static readonly DependencyProperty CurrentTimeProperty =
             DependencyProperty.Register("CurrentTime", typeof(DateTime),
             typeof(MyClockControl), new FrameworkPropertyMetadata(DateTime.Now));

        // .NET Property wrapper
        public DateTime CurrentTime
        {
            get { return (DateTime)GetValue(CurrentTimeProperty); }
            set { SetValue(CurrentTimeProperty, value); }
        }
        */

    }
    
}
