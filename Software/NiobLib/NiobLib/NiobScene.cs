﻿using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using NiobLib.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NiobLib;
using System.Windows;
using System.Data;
using System.Windows.Threading;
using System.Security.Cryptography.Xml;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Assimp;
using System.Windows.Controls;
using NiobLib.Physics;
using System.Diagnostics;
using System.Windows.Markup;
using NiobLib.Actions;

namespace NiobLib
{
    [ContentProperty("Entities")]
    public class NiobScene 
    {
        #region Properties
        private SetCollection<INiobEntity> entities;

        private SetCollection<IAction> actions;

        private PhysicsSolver physics;

        private GameLoop gameLoop;

        public SetCollection<INiobEntity> Entities 
        { 
            get { return entities ; }
        }

        public SetCollection<IAction> Actions
        {
            get { return actions; }
        }

        public PhysicsSolver Physics
        {
            get { return physics; }
        }

        public GameLoop GameLoop
        {
            get { return gameLoop ; }
        }

        #endregion


        #region Dickhandler
        public delegate void TickEventHandler(object source,TickEventArgs e);
        public event TickEventHandler TickEvent; //Dick Handler

        public class TickEventArgs : EventArgs
        {
            //fortnite
        }

        protected virtual void OnGameLoopTick(TickEventArgs e)
        {
            if (TickEvent != null)
            {
                TickEvent(this, e);
            }
        }
        #endregion


        private void LoopDiLoop(object? sender, EventArgs e)
        {
            OnGameLoopTick(new TickEventArgs());
            
            foreach (IAction item in Actions)
            {
                item.Step(physics.TimeStep);
            }
            physics.SolveStep();
        }

        public NiobScene()
        {
            entities = new SetCollection<INiobEntity>();
            actions = new SetCollection<IAction>();
            physics = new PhysicsSolver(0.02f, 50);
            gameLoop = new GameLoop(LoopDiLoop);
        }

        
        


    }
}