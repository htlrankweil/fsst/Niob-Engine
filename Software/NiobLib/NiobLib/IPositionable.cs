﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib
{
    public interface IPositionable
    {
        public Vector3 Position { get; set; }
    }
}
