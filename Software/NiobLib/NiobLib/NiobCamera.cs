﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelixToolkit;
using HelixToolkit.SharpDX.Core.Cameras;
using HelixToolkit.Wpf.SharpDX;
using SharpDX;


namespace NiobLib
{
    public class NiobCamera : IRotatable, IPositionable
    {

		private PerspectiveCamera helixCamera = new PerspectiveCamera();
		public PerspectiveCamera HelixCamera
		{
			get { return helixCamera; }
		}

		private Quaternion rotation;

		public Quaternion Rotation
		{
			get { return rotation; }
			set { rotation = value; }
		}


		public Vector3 Position
		{
			get { return helixCamera.Position.ToVector3(); }
			set { HelixCamera.Position = value.ToPoint3D(); }
		}
		
		public Vector3 LookDirection
		{
			get { return helixCamera.LookDirection.ToVector3(); }
			set { helixCamera.LookDirection = value.ToVector3D();}
		}
		
		public double FoV
		{
			get { return helixCamera.FieldOfView; }
			set { helixCamera.FieldOfView = value; }
		}

	
		public Vector3 UpDirection
		{
			get { return helixCamera.UpDirection.ToVector3(); }
			set { helixCamera.UpDirection = value.ToVector3D(); }
		}
	
		#region Constructors
		public NiobCamera() 
		{
            Position = new Vector3(0,2,0);
            LookDirection = new Vector3(1, 1 ,1);
			//UpDirection = new Vector3(0, 0, 0);
			FoV = 90;
        }

        public NiobCamera(Vector3 lookdir, Vector3 position, double fov)
		{
			Position = position;
			LookDirection = lookdir;
			//UpDirection = upDirection;
			FoV = fov;
		}
        #endregion
    }
}
