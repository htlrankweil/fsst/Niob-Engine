﻿using HelixToolkit.SharpDX.Core;
using SharpDX;
using SharpDX.Direct2D1.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using NiobLib.Math;
using Vector3 = SharpDX.Vector3;
using SharpDX.Mathematics.Interop;
using System.Windows.Documents;
using System.Security.AccessControl;
using System.Windows.Controls.Ribbon.Primitives;

namespace NiobLib.Physics
{
    public class PhysicsSolver
    {
        private float timeStep;

        public float TimeStep
        {
            get { return timeStep; }
            set { timeStep = value; }
        }

        private int substeps;

        public int Substeps
        {
            get { return substeps; }
            set { substeps = value; }
        }


        private List<PhysicsGroup> physicGroups;
        private HashSet<IPhysicConstaint> constaints = new HashSet<IPhysicConstaint>();
        private HashSet<IPhysicsObject> physicsObjects;
        private HashSet<Tuple<IPhysicsObject, IPhysicsObject>> collisionPairs;

        public List<PhysicsGroup> PhysicsGroups
        {
            get { return physicGroups; }
        }

        public HashSet<IPhysicConstaint> Constaints
        {
            get { return constaints; }
        }

        public void SolveStep()
        {
            physicsObjects.Clear();
            collisionPairs.Clear();

            foreach (PhysicsGroup group in physicGroups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    IPhysicsObject a = group[i];
                    if (physicsObjects.Contains(group[i]))
                    {
                        a.GroupAcceleration += group.GlobalAcceleration;
                    }
                    else
                    {
                        a.CachePhysicValues(timeStep);
                        a.GroupAcceleration = group.GlobalAcceleration;
                        physicsObjects.Add(a);
                    }

                    for (int j = i + 1; j < group.Count; j++)
                    {
                        IPhysicsObject b = group[j];
                        if (PossibleCollision(a, b))
                        {
                            if (a.GetHashCode() > b.GetHashCode())
                                collisionPairs.Add(Tuple.Create(a, b));
                            else
                                collisionPairs.Add(Tuple.Create(a, b));
                        }
                    }
                }
            }

            float substepTime = timeStep / substeps;
            for (int i = 0; i < substeps; i++)
            {
                foreach (IPhysicsObject obj in physicsObjects)
                {
                    obj.PrevPosition = obj.PhysicsPosition;
                    obj.PrevOrientation = obj.PhysicsOrientation;
                    if (obj.IsStationary) continue;

                    //euler position
                    obj.PhysicsVelocity += obj.GroupAcceleration * substepTime;
                    obj.PhysicsPosition += obj.PhysicsVelocity * substepTime;

                    //euler orientation
                    //Vector3 a =  obj.PhysicsOrientation.Rotate(obj.PhysicsOrientation.InvRotate(new Vector3(10, 0, 0)));
                    Vector3 localAngularV = obj.PhysicsOrientation.InvRotate(obj.PhysicsAngularVelocity);
                    localAngularV += substepTime * obj.PhysicsInverseAngularMass.Multiply(-Vector3.Cross(localAngularV, obj.AngularMass.Multiply(localAngularV)));

                    obj.PhysicsAngularVelocity = obj.PhysicsOrientation.Rotate(localAngularV);
                    //Matrx3x3 inertiaT = obj.AngularMass* obj.PhysicsOrientation;

                    //obj.PhysicsAngularVelocity += substepTime * obj.PhysicsInverseAngularMass.Multiply(-Vector3.Cross(obj.PhysicsAngularVelocity, obj.AngularMass.Multiply(obj.PhysicsAngularVelocity)));
                    //float phi = obj.PhysicsAngularVelocity.Length();
                    //if (phi * substepTime <= 10.5f)
                    //    obj.PhysicsOrientation = Quaternion.Normalize(obj.PhysicsOrientation + substepTime * 0.5f * obj.PhysicsAngularVelocity.Multiply(obj.PhysicsOrientation));
                    //else
                    //    obj.PhysicsOrientation = Quaternion.Normalize(obj.PhysicsOrientation + 1 / phi * obj.PhysicsAngularVelocity.Multiply(obj.PhysicsOrientation));
                    obj.PhysicsOrientation = obj.PhysicsOrientation.RotateBy(obj.PhysicsAngularVelocity, substepTime);

                }


                foreach (Tuple<IPhysicsObject, IPhysicsObject> collisionPair in collisionPairs)
                {
                    SolveCollision(collisionPair, substepTime);
                }


                foreach (IPhysicConstaint constaint in constaints)
                {
                    constaint.SolveConstraint(substepTime);
                }

                foreach (IPhysicsObject obj in physicsObjects)
                {
                    //update everything
                    obj.PhysicsVelocity = (obj.PhysicsPosition - obj.PrevPosition) / substepTime;
                    Quaternion dq = obj.PhysicsOrientation * Quaternion.Invert(obj.PrevOrientation);
                    if (dq.W >= 0)
                        obj.PhysicsAngularVelocity = 2 * new Vector3(dq.X, dq.Y, dq.Z) / substepTime;
                    else
                        obj.PhysicsAngularVelocity = -2 * new Vector3(dq.X, dq.Y, dq.Z) / substepTime;
                }

            }

            foreach (IPhysicsObject obj in physicsObjects)
            {
                obj.UptadePhysicValues();
            }
        }

        private bool PossibleCollision(IPhysicsObject object1, IPhysicsObject object2)
        {
            if (object1.IsActive || object2.IsActive)
            {
                if ((object1.PhysicsPosition - object2.PhysicsPosition).Length() <
                    (object1.BoundingShape.BoundingRadius + object2.BoundingShape.BoundingRadius +
                     object1.PhysicsVelocity.LengthSquared() + object2.PhysicsVelocity.LengthSquared()) * 1.2)
                {
                    return true;
                }
            }
            return false;
        }


        private List<Tuple<Vector3, Vector3>> EPAPoints;
        private List<Tuple<int, int, int, Vector3, float>> EPAFaces;
        public void SolveCollision(Tuple<IPhysicsObject, IPhysicsObject> pair, float dt)
        {
            IPhysicsObject objA = pair.Item1;
            IPhysicsObject objB = pair.Item2;

            IBoundingShape boundA = objA.BoundingShape;
            IBoundingShape boundB = objB.BoundingShape;

            Vector3 Distance = objA.PhysicsPosition - objB.PhysicsPosition;
            if (Distance.Length() < (boundA.BoundingRadius + boundB.BoundingRadius))
            {
                Matrix3x3 RotA = Matrix3x3.RotationQuaternion(objA.PhysicsOrientation);
                Matrix3x3 RotB = Matrix3x3.RotationQuaternion(objB.PhysicsOrientation);
                if (boundA is IGJKBounding bndA && boundB is IGJKBounding bndB)
                {
                    Simplex simplex = new Simplex();
                    Vector3 dir = Vector3.Zero;

                    //search in the direction of origin
                    Vector3 objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(-Distance)));

                    simplex.Add(Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(Distance))),objASup, ref dir);

                    for (int i = 0; i < 100; i++)
                    {
                        objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(dir)));
                        Vector3 v = Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(-dir)));
                        if (Vector3.Dot(v, dir) < 0.01f) return;
                        float aasdf = Vector3.Dot(v, dir);

                        if (simplex.Add(v, objASup, ref dir))
                        {
                            goto Collision;
                        }
                    }
                    return;

                Collision:
                    {

                        EPAPoints.Clear();
                        EPAFaces.Clear();
                        EPAPoints.Add(Tuple.Create(simplex.a, simplex.oa)); EPAPoints.Add(Tuple.Create(simplex.b, simplex.ob)); EPAPoints.Add(Tuple.Create(simplex.c, simplex.oc)); EPAPoints.Add(Tuple.Create(simplex.d, simplex.od));

                        int minTri = 0;
                        float minDist = float.PositiveInfinity;
                        EPAFaces.Add(GetTriNormal(EPAPoints, 0, 1, 2)); EPAFaces.Add(GetTriNormal(EPAPoints, 0, 1, 3));
                        EPAFaces.Add(GetTriNormal(EPAPoints, 0, 2, 3)); EPAFaces.Add(GetTriNormal(EPAPoints, 1, 2, 3));

                        for (int i = 0; i < 4; i++)
                        {
                            float dist = EPAFaces[i].Item5;

                            if (minDist > dist)
                            {
                                minDist = dist;
                                minTri = i;
                            }
                        }

                        try
                        {

                            minDist = float.PositiveInfinity;
                            Vector3 minNormal = EPAFaces[minTri].Item4;
                            for (int iteration = 0; (minDist == float.PositiveInfinity) && iteration < 200; iteration++)
                            {
                                minNormal = EPAFaces[minTri].Item4;
                                minDist = EPAFaces[minTri].Item5;

                                //objASup = bndA.GetSupport(RotA.InvRot(minNormal));
                                objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(minNormal)));
                                Vector3 support = Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(-minNormal)));
                                float supDist = Vector3.Dot(support, minNormal);

                                if (MathF.Abs(supDist - minDist) > 0.0001f)
                                {
                                    minDist = float.PositiveInfinity;

                                    //unique
                                    List<Tuple<int, int>> uniqueEdges = new List<Tuple<int, int>>();

                                    int epaFaceCount = EPAFaces.Count - 1;

                                    for (int i = 0; i <= epaFaceCount; i++)
                                    {
                                        Tuple<int, int, int, Vector3, float> face = EPAFaces[i];
                                        if (Vector3.Dot(face.Item4, support - EPAPoints[face.Item1].Item1) > 0)
                                        {
                                            AddIfUnique(uniqueEdges, face.Item1, face.Item2);
                                            AddIfUnique(uniqueEdges, face.Item1, face.Item3);
                                            AddIfUnique(uniqueEdges, face.Item2, face.Item3);

                                            EPAFaces[i] = EPAFaces[epaFaceCount]; EPAFaces.RemoveAt(epaFaceCount); epaFaceCount--;
                                            i--;
                                        }
                                    }

                                    EPAPoints.Add(Tuple.Create(support, objASup));

                                    float oldMinDistance = float.PositiveInfinity;
                                    for (int i = 0; i < EPAFaces.Count; i++)
                                    {
                                        float dist = EPAFaces[i].Item5;

                                        if (oldMinDistance > dist)
                                        {
                                            oldMinDistance = dist;
                                            minTri = i;
                                        }
                                    }

                                    foreach (Tuple<int, int> edge in uniqueEdges)
                                    {
                                        Tuple<int, int, int, Vector3, float> face = GetTriNormal(EPAPoints, edge.Item1, edge.Item2, EPAPoints.Count - 1);

                                        if (face.Item5 < oldMinDistance)
                                        {
                                            oldMinDistance = face.Item5;
                                            minTri = EPAFaces.Count;
                                        }

                                        EPAFaces.Add(face);
                                    }
                                }

                            }

                            //min normal
                            float depth = minDist;
                            //if (depth > 0.005)
                            //{
                            //    Vector3 pa1 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, 1, 1)));
                            //    Vector3 pa2 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, 1, -1)));
                            //    Vector3 pa3 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, -1, 1)));
                            //    Vector3 pa4 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, -1, -1)));
                            //    Vector3 pa5 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, 1, 1)));
                            //    Vector3 pa6 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, 1, -1)));
                            //    Vector3 pa7 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, -1, 1)));
                            //    Vector3 pa8 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, -1, -1)));

                            //    Vector3 pb1 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(1, 1, 1)));
                            //    Vector3 pb2 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(1, 1, -1)));
                            //    Vector3 pb3 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(1, -1, 1))); 
                            //    Vector3 pb4 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(1, -1, -1)));
                            //    Vector3 pb5 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(-1, 1, 1)));
                            //    Vector3 pb6 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(-1, 1, -1)));
                            //    Vector3 pb7 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(-1, -1, 1)));
                            //    Vector3 pb8 = objB.PhysicsPosition + RotB.Rotate(bndB.GetSupport(new Vector3(-1, -1, -1)));
                            //}

                            Tuple<int, int, int, Vector3, float> Tri = EPAFaces[minTri];
                            Vector3 T1 = EPAPoints[Tri.Item1].Item1;
                            Vector3 T2 = EPAPoints[Tri.Item2].Item1;
                            Vector3 T3 = EPAPoints[Tri.Item3].Item1;

                            NiobMath.Barycentric(minNormal * minDist, T1, T2, T3, out float u, out float v, out float w);
                            if (u is float.NaN || v is float.NaN || w is float.NaN)
                            {
                                return;
                            }

                            //Vector3 objAT1 = RotA.Rotate(bndA.GetSupport(RotA.InvRot(T1)));
                            //Vector3 objAT2 = RotA.Rotate(bndA.GetSupport(RotA.InvRot(T2)));
                            //Vector3 objAT3 = RotA.Rotate(bndA.GetSupport(RotA.InvRot(T3)));


                            Vector3 objAT1 = EPAPoints[Tri.Item1].Item2; //bndA.GetSupport(RotA.InvRot(T1));
                            Vector3 objAT2 = EPAPoints[Tri.Item2].Item2; //bndA.GetSupport(RotA.InvRot(T2));
                            Vector3 objAT3 = EPAPoints[Tri.Item3].Item2; //bndA.GetSupport(RotA.InvRot(T3));



                            Vector3 r1 = objAT1 * u + objAT2 * v + objAT3 * w;

                            //Vector3 objBT1 = objAT1 - T1;//bndB.GetSupport(RotB.InvRot(-T1));
                            //Vector3 objBT2 = objAT2 - T2;//bndB.GetSupport(RotB.InvRot(-T2));
                            //Vector3 objBT3 = objAT3 - T3;//bndB.GetSupport(RotB.InvRot(-T3));

                            Vector3 r2 = r1 - (minNormal * minDist - Distance);//objBT1 * u + objBT2 * v + objBT3 * w;

                            Vector3 r1Local = RotA.InvRot(r1);
                            Vector3 r2Local = RotB.InvRot(r2);

                            Vector3 r1Prior = objA.PrevOrientation.Rotate(r1Local);
                            Vector3 r2Prior = objB.PrevOrientation.Rotate(r2Local);
                            Vector3 r2Prior2 = objB.PhysicsOrientation.Rotate(r2Local);

                            Vector3 dp = (r1 - r2 + Distance) - ((r1Prior + objA.PrevPosition) - (r2Prior + objB.PrevPosition));
                            Vector3 dpt = dp - Vector3.Dot(dp, minNormal) * minNormal;

                            dp = (depth * minNormal) + dpt;


                            depth = dp.Length();
                            if (!MathUtil.IsZero(depth))
                            {
                                minNormal = dp / depth;
                            }


                            float w1 = 0, w2 = 0;
                            if (objA.IsActive)
                            {
                                Vector3 cp1 = RotA.InvRot(Vector3.Cross(r1, minNormal));
                                w1 = objA.PhysicsInverseMass + Vector3.Dot(cp1, objA.PhysicsInverseAngularMass.Multiply(cp1));
                            }

                            if (objB.IsActive)
                            {

                                Vector3 cp2 = RotB.InvRot(Vector3.Cross(r2, minNormal));
                                w2 = objB.PhysicsInverseMass + Vector3.Dot(cp2, objB.PhysicsInverseAngularMass.Multiply(cp2));
                            }

                            float lamda = -depth / (w1 + w2 /*+ 0.001f/dt/dt*/) /* (1 - 0.1f * dt * dt)*/;

                            Vector3 p = lamda * minNormal;
                            if (objA.IsActive)
                            {
                                objA.PhysicsPosition += p * objA.PhysicsInverseMass;
                                Vector3 dq = RotA.Rotate(objA.PhysicsInverseAngularMass.Multiply(RotA.InvRot(Vector3.Cross(r1, p))));
                                objA.PhysicsOrientation = objA.PhysicsOrientation.RotateBy(dq);
                            }

                            if (objB.IsActive)
                            {
                                objB.PhysicsPosition -= p * objB.PhysicsInverseMass;
                                Vector3 dq = -RotB.Rotate(objB.PhysicsInverseAngularMass.Multiply(RotB.InvRot(Vector3.Cross(r2, p))));
                                objB.PhysicsOrientation = objB.PhysicsOrientation.RotateBy(dq);
                            }

                            //RotA = Matrix3x3.RotationQuaternion(objA.PhysicsOrientation);
                            //Vector3 p1 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, 1, 1)));
                            //Vector3 p2 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, 1, -1)));
                            //Vector3 p3 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, -1, 1)));
                            //Vector3 p4 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(1, -1, -1)));
                            //Vector3 p5 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, 1, 1)));
                            //Vector3 p6 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, 1, -1)));
                            //Vector3 p7 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, -1, 1)));
                            //Vector3 p8 = objA.PhysicsPosition + RotA.Rotate(bndA.GetSupport(new Vector3(-1, -1, -1)));

                            //float mind = -0.0001f;
                            //if (p1.Y<mind||p2.Y<mind|| p3.Y < mind|| p4.Y< mind|| p5.Y < mind|| p6.Y < mind|| p7.Y < mind|| p8.Y < mind)
                            //{

                            //}

                        }
                        catch (Exception)
                        {
                            return;
                        }

                    }

                }
            }
        }

        private void AddIfUnique(List<Tuple<int, int>> uniqueEdges, int item1, int item2)
        {
            Tuple<int, int> edge;
            if (item1 > item2)
            {
                edge = Tuple.Create(item1, item2);
            }
            else
            {
                edge = Tuple.Create(item2, item1);
            }


            for (int i = 0; i < uniqueEdges.Count; i++)
            {
                if (uniqueEdges[i].Equals(edge))
                {
                    uniqueEdges.RemoveAt(i);
                    return;
                }
            }
            uniqueEdges.Add(edge);
        }

        private Tuple<int, int, int, Vector3, float> GetTriNormal(List<Tuple<Vector3, Vector3>> Positions, int ia, int ib, int ic)
        {
            Vector3 a = Positions[ia].Item1;
            Vector3 ba = Positions[ib].Item1 - a;
            Vector3 ca = Positions[ic].Item1 - a;

            Vector3 normal = Vector3.Normalize(Vector3.Cross(ba, ca));
            float d = Vector3.Dot(normal, a);

            if (d < 0)
            {
                d = -d;
                normal = -normal;
            }

            return Tuple.Create(ia, ib, ic, normal, d);
        }





        public PhysicsSolver(double dt, int steps)
        {
            timeStep = (float)dt;
            substeps = steps;
            physicsObjects = new HashSet<IPhysicsObject>();
            physicsObjects.EnsureCapacity(100);

            physicGroups = new List<PhysicsGroup>();

            collisionPairs = new HashSet<Tuple<IPhysicsObject, IPhysicsObject>>();
            collisionPairs.EnsureCapacity(1000);

            EPAPoints = new List<Tuple<Vector3, Vector3>>();
            EPAPoints.EnsureCapacity(100);

            EPAFaces = new List<Tuple<int, int, int, Vector3, float>>();
            EPAFaces.EnsureCapacity(100);

        }


    }
}
