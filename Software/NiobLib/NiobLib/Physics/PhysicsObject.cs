﻿using NiobLib.Entities;
using NiobLib.Math;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NiobLib.Physics
{
    public interface IPhysicsObject : ICollideable
    {
        public Vector3 Velocity { get; set; }
        public Vector3 AngularVelocity { get; set; }
        public float Mass { get; set; }
        public Matrix3x3 AngularMass { get; }
        public bool IsActive { get; set; }
        public bool IsStationary { get; set; }
        public bool InterpolateMotion { get; set; }
        public Quaternion Orientation { get; }
        public Vector3 Position { get; }


        internal Vector3 PhysicsPosition { get; set; }
        internal Quaternion PhysicsOrientation { get; set; }

        internal Vector3 PhysicsVelocity { get; set; }
        internal Vector3 PhysicsAngularVelocity { get; set; }

        internal Vector3 GroupAcceleration { get; set; }

        internal Matrix3x3 PhysicsInverseAngularMass { get; }
        internal float PhysicsInverseMass { get; }


        internal Vector3 PrevPosition { get; set; }
        internal Quaternion PrevOrientation { get; set; }


        internal void CachePhysicValues(float timestep);
        internal void UptadePhysicValues();
    }

    public interface ICollideable
    {
        public IBoundingShape BoundingShape { get; set; }
    }

    public class PhysicWrapper : IPhysicsObject
    {
        private Vector3 velocity;
        private Vector3 angularVelocity;
        private float mass;
        private Matrix3x3 angularMass;
        private bool isActive;
        private bool interpolateMotion;
        private bool isStationary;

        private Vector3 physicsPosition;
        private Quaternion physicsOrientation;

        private Vector3 prevPosition;
        private Quaternion prevOrientation;


        private Vector3 physicsVelocity;
        private Vector3 groupAcceleration;
        private Vector3 physicsAngularVelocity;

        private Matrix3x3 inverseAngularMass;
        private float inverseMass;

        private IBoundingShape boundingShape;
        private INiobEntity niobEntity;

        private bool changedVelocity;

        private bool interpolatedMotion;
        private float interpolatedEnergy;

        #region Propertys
        public Vector3 Velocity 
        { 
            get => velocity;
            set { velocity = value; changedVelocity = true; } 
        }

        public Vector3 AngularVelocity 
        { 
            get => angularVelocity; 
            set => angularVelocity = value; 
        }

        public float Mass 
        { 
            get => mass; 
            set => mass = value; 
        }

        public bool IsActive 
        { 
            get => isActive; 
            set => isActive = value&&isStationary; 
        }

        public bool IsStationary
        {
            get => isStationary;
            set
            {
                isActive = isActive && (!value);
                isStationary = value;
            }
        }

        bool IPhysicsObject.InterpolateMotion 
        { 
            get => interpolatedMotion; 
            set => interpolatedMotion = value; 
        }

        public IBoundingShape BoundingShape 
        { 
            get => boundingShape; 
            set => boundingShape = value; 
        }

        public Matrix3x3 AngularMass => angularMass;

        Vector3 IPhysicsObject.Position
        {
            get {
                return niobEntity.Position;
            }
        }

        Quaternion IPhysicsObject.Orientation
        {
            get {
                return niobEntity.Rotation;
            }
        }



        Vector3 IPhysicsObject.PhysicsPosition 
        { 
            get => physicsPosition;
            set
            {
                if (value.X is float.NaN || value.Y is float.NaN || value.Z is float.NaN)
                {
                    return;
                }
                physicsPosition = value;
            }
        }

        Quaternion IPhysicsObject.PhysicsOrientation
        {
            get => physicsOrientation;
            set
            {
                if (value.X is float.NaN || value.Y is float.NaN || value.Z is float.NaN)
                {
                    return;
                }
                physicsOrientation = value;
            }
        }


        Vector3 IPhysicsObject.PrevPosition
        {
            get => prevPosition;
            set => prevPosition = value;
        }

        Quaternion IPhysicsObject.PrevOrientation 
        { 
            get => prevOrientation; 
            set => prevOrientation = value; 
        }


        Vector3 IPhysicsObject.PhysicsVelocity 
        { 
            get => physicsVelocity; 
            set => physicsVelocity = value; 
        }

        Vector3 IPhysicsObject.GroupAcceleration
        {
            get => groupAcceleration;
            set => groupAcceleration = value;
        }

        Vector3 IPhysicsObject.PhysicsAngularVelocity 
        { 
            get => physicsAngularVelocity; 
            set => physicsAngularVelocity = value; 
        }

        Matrix3x3 IPhysicsObject.PhysicsInverseAngularMass => inverseAngularMass;

        float IPhysicsObject.PhysicsInverseMass => inverseMass;

        #endregion

        public PhysicWrapper(INiobEntity entity, IBoundingShape bounds, float mass, bool active)
        {
            niobEntity = entity;
            boundingShape = bounds;
            isActive = active;
            isStationary = false;

            this.mass = mass;
            inverseMass = 1 / mass;
            angularMass = mass * boundingShape.InertiaTensor;
            inverseAngularMass = Matrix3x3.Invert(angularMass);

            velocity = new Vector3();
            physicsVelocity = new Vector3();
            angularVelocity = new Vector3();
            physicsAngularVelocity = new Vector3();
            groupAcceleration = new Vector3();

            physicsPosition = entity.Position;
            physicsOrientation = entity.Rotation;

            interpolatedMotion = false;
            interpolateMotion = false;
            changedVelocity = true;
            interpolatedEnergy = 0;
            
        }


        void IPhysicsObject.CachePhysicValues(float timestep)
        {
            interpolatedMotion = false;

            if (interpolateMotion && !changedVelocity && (niobEntity.Position != physicsPosition))
            {
                velocity = (niobEntity.Position - physicsPosition)/timestep;
                interpolatedEnergy = Vector3.Dot(velocity, velocity);
                interpolatedMotion = true;
            }
            else
            {
                physicsPosition = niobEntity.Position;
            }

            physicsOrientation =Quaternion.Normalize(niobEntity.Rotation);

            physicsVelocity = velocity;
            physicsAngularVelocity = angularVelocity;

            inverseMass = 1 / mass;
            inverseAngularMass = Matrix3x3.Invert(angularMass);

            prevPosition = new Vector3();
            prevOrientation = new Quaternion();

            changedVelocity = false;
        }


        void IPhysicsObject.UptadePhysicValues()
        {
            if (interpolatedMotion) // takes energy out don't know what to do
            {
                float energyout = Vector3.Dot(physicsVelocity, physicsVelocity);
                float resultenergy = MathF.Min(energyout - interpolatedEnergy,0);

                if (!MathUtil.IsZero(energyout))
                {
                    physicsVelocity*=MathF.Sqrt(resultenergy/energyout);
                }
            }

            if (!changedVelocity)
            {
                velocity = physicsVelocity;
            }
            angularVelocity = physicsAngularVelocity;

            //physicsTransform.Scale(scale);

            niobEntity.Position = physicsPosition;
            niobEntity.Rotation = physicsOrientation;


            //System.Diagnostics.Debug.WriteLine(AngularMass.Multiply(physicsOrientation.InvRotate( angularVelocity)).Length().ToString());
        }
    }

}
