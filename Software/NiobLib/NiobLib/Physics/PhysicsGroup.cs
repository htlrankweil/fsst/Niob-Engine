﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Physics
{
    public class PhysicsGroup:SetCollection<IPhysicsObject>
    {
		private Vector3 globalAcceleration;

		public Vector3 GlobalAcceleration
		{
			get { return globalAcceleration; }
			set { globalAcceleration = value; }
		}

		public PhysicsGroup()
		{
			globalAcceleration = new Vector3();
		}


	}
}
