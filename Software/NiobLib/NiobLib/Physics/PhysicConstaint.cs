﻿using SharpDX;
using SharpDX.DXGI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NiobLib.Math;
using System.Windows.Media.Animation;

namespace NiobLib.Physics
{
    public interface IPhysicConstaint
    {
        public void SolveConstraint(float dt);
        public void SolveVelocity(float dt);
    }

    public class DistanceConstraint : IPhysicConstaint
    {
        public DistanceConstraint(IPhysicsObject objA, IPhysicsObject objB, Vector3 localPointA, Vector3 localPointB, float constraintDistance, float InverseStiffness)
        {
            objectA = objA;
            objectB = objB;
            pointA = localPointA;
            pointB = localPointB;
            distance = constraintDistance;
            inverseStiffness = InverseStiffness;
        }

        private IPhysicsObject objectA;
        private IPhysicsObject objectB;
        private float inverseStiffness;
        private float distance;
        private Vector3 pointA;
        private Vector3 pointB;
        


        public Vector3 PointA
        {
            get { return pointA; }
            set { pointA = value; }
        }

        public Vector3 PointB
        {
            get { return pointB; }
            set { pointB = value; }
        }

        public float InverseStiffness
        {
            get { return inverseStiffness; }
            set { inverseStiffness = value; }
        }


        public float Distance
        {
            get { return distance; }
            set { distance = value; }
        }


        public virtual void SolveConstraint(float dt)
        {
            Matrix3x3 rot1 = Matrix3x3.RotationQuaternion(objectA.PhysicsOrientation);
            Matrix3x3 rot2 = Matrix3x3.RotationQuaternion(objectB.PhysicsOrientation);
            Vector3 r1 = rot1.Rotate(PointA); 
            Vector3 r2 = rot2.Rotate(PointB); 
            Vector3 n = (r2 + objectB.PhysicsPosition) - (r1 + objectA.PhysicsPosition);
            float C = n.Length()-distance;
            n.Normalize();
            if (C < 0)
            {
                n = -n;
                C = -C;
            }

            float w = 0;

            if (!objectA.IsStationary)
            {
                Vector3 cp1 = rot1.InvRot(Vector3.Cross(r1, n));
                w += objectA.PhysicsInverseMass + Vector3.Dot(cp1, objectA.PhysicsInverseAngularMass.Multiply(cp1));
            }

            if (!objectB.IsStationary)
            {
                Vector3 cp2 = rot2.InvRot(Vector3.Cross(r2, n));
                w += objectB.PhysicsInverseMass + Vector3.Dot(cp2, objectB.PhysicsInverseAngularMass.Multiply(cp2));
            }
           
            float a = inverseStiffness/(dt * dt);
            float lamda = C / (w + a);
            
            Vector3 p = lamda * n;

            if (!objectA.IsStationary)
            {
                objectA.PhysicsPosition += p * objectA.PhysicsInverseMass;

                Vector3 dq = rot1.Rotate(objectA.PhysicsInverseAngularMass.Multiply(rot1.InvRot(Vector3.Cross(r1, p))));
                objectA.PhysicsOrientation = objectA.PhysicsOrientation.RotateBy(dq);

            }
            if (!objectB.IsStationary)
            {
                objectB.PhysicsPosition -= p * objectB.PhysicsInverseMass;

                Vector3 dq = -rot2.Rotate(objectB.PhysicsInverseAngularMass.Multiply(rot2.InvRot(Vector3.Cross(r2, p))));
                objectB.PhysicsOrientation = objectB.PhysicsOrientation.RotateBy(dq);
            }
        }

        public virtual void SolveVelocity(float dt)
        {

        }
    }

    public class FixedPointDistanceConstraint : IPhysicConstaint
    {
        public FixedPointDistanceConstraint(IPhysicsObject objA, Vector3 localPointA, Vector3 FixedPoint, float constraintDistance, float InverseStiffness)
        {
            objectA = objA;
            pointA = localPointA;
            fixedPoint = FixedPoint;
            distance = constraintDistance;
            inverseStiffness = InverseStiffness;
        }

        private IPhysicsObject objectA;
        private float inverseStiffness;
        private float distance;
        private Vector3 pointA;
        private Vector3 fixedPoint;
        private float rotationDamp = 0.1f;
        private float translationDamp = 0.1f;


        public Vector3 PointA
        {
            get { return pointA; }
            set { pointA = value; }
        }

        public Vector3 FixedPoint
        {
            get { return fixedPoint; }
            set { fixedPoint = value; }
        }

        public float InverseStiffness
        {
            get { return inverseStiffness; }
            set { inverseStiffness = value; }
        }


        public float Distance
        {
            get { return distance; }
            set { distance = value; }
        }


        public virtual void SolveConstraint(float dt)
        {
            if (objectA.IsStationary) return;
            Matrix3x3 rot1 = Matrix3x3.RotationQuaternion(objectA.PhysicsOrientation);
            Vector3 r1 = rot1.Rotate(PointA);
            Vector3 n = fixedPoint - (r1 + objectA.PhysicsPosition);
            float C = n.Length() - distance;
            n.Normalize();

            if (C < 0)
            {
                n = -n;
                C = -C;
            }

            Vector3 cp1 = rot1.InvRot(Vector3.Cross(r1, n));
            float w1 = 1 * objectA.PhysicsInverseMass + Vector3.Dot(cp1, objectA.PhysicsInverseAngularMass.Multiply(cp1));

            float a = inverseStiffness / (dt * dt);
            float lamda = C / (w1 + a);

            Vector3 p = lamda * n;
            Vector3 dp = p * objectA.PhysicsInverseMass;
            objectA.PhysicsPosition += p * objectA.PhysicsInverseMass;
            Vector3 dq = rot1.Rotate(objectA.PhysicsInverseAngularMass.Multiply(rot1.InvRot(Vector3.Cross(r1, p)))); 
            objectA.PhysicsOrientation = objectA.PhysicsOrientation.RotateBy(dq);

        }

        public virtual void SolveVelocity(float dt)
        {
            if (rotationDamp>0)
            {

            }
        }
    }

    //public class FixedHingeConstraint : FixedPointDistanceConstraint
    //{
    //    public FixedHingeConstraint(IPhysicsObject objA, Vector3 localPointA, Vector3 FixedPoint, Vector3 Ob, float constraintDistance, float InverseStiffness) : base()
    //    {

    //    }
    //}

}
