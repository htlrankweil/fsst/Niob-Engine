﻿
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Assimp;
using HelixToolkit.SharpDX.Core.Model.Scene;
using NiobLib;
using NiobLib.Entities;
using NiobLib.Math;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace NiobLib.Physics
{
    public interface IBoundingShape
    {
        public float BoundingRadius { get; }
        public Matrix3x3 InertiaTensor {get ;}
    }

    public interface IGJKBounding
    {
        public Vector3 GetSupport(Vector3 Direction);
    }


    public class ConvexMeshBounding : IBoundingShape, IGJKBounding
    {
        private Vector3[] meshVertexes;

        private float boundingRadius;

        public float BoundingRadius
        {
            get { return boundingRadius; }
        }

        public Vector3[] MeshVertexes
        {
            get { return meshVertexes; }
            set { 
                meshVertexes = value;
                updateRadius();
            }
        }

        public Matrix3x3 InertiaTensor
        {
            get
            {
                return (boundingRadius * boundingRadius * (2f / 5f)) * SharpDX.Matrix3x3.Identity;
            }
        }

        public ConvexMeshBounding(Vector3[] meshVertexes)
        {
            MeshVertexes = meshVertexes;
        }

        public ConvexMeshBounding(NiobEntity3d niobEntity)
        {
            SceneNode node = niobEntity.Node;
            foreach (var item in node.Traverse())
            {
                if (item is MeshNode meshNode)
                {
                    Geometry3D geometry = meshNode.Geometry;
                    MeshVertexes = geometry.Positions.ToArray();
                    return;
                }
            }
            throw new NiobException(niobEntity, Stressgrund.NoLootAtLootLake);
        }

        public ConvexMeshBounding(string filename)
        {
            Assembly assembly = Assembly.GetEntryAssembly();

            string FilePath = assembly.GetManifestResourceNames().Where(x => x.EndsWith(filename)).FirstOrDefault();
            if (FilePath == null) throw new Exception("FileNotFound");

            Assimp.AssimpContext assimpContext = new Assimp.AssimpContext() { Scale = 0.1f };
            Assimp.Scene assimpScene = assimpContext.ImportFileFromStream(assembly.GetManifestResourceStream(FilePath), Assimp.PostProcessSteps.JoinIdenticalVertices
                                                                                                                      | Assimp.PostProcessSteps.FindDegenerates
                                                                                                                      | Assimp.PostProcessSteps.SortByPrimitiveType
                                                                                                                      | Assimp.PostProcessSteps.RemoveRedundantMaterials
                                                                                                                      | Assimp.PostProcessSteps.FlipUVs);

            if (assimpScene.MeshCount == 0)
                throw new NiobException(assimpScene, Stressgrund.NoLootAtLootLake);

            List<int> items = new List<int>() { 0 };
            Assimp.Node currentNode = assimpScene.RootNode;
            while (items.Count > 0)
            {
                if (currentNode.ChildCount > items[items.Count - 1])
                {
                    currentNode = currentNode.Children[items[items.Count - 1]];
                    items[items.Count - 1]++;
                    items.Add(0);

                    if (currentNode.HasMeshes)
                    {
                        Assimp.Matrix4x4 transform = currentNode.Transform;
                        Assimp.Node parent = currentNode.Parent;
                        while (parent is not null)
                        {
                            transform = parent.Transform * transform;
                            parent = parent.Parent;
                        }

                        Matrix transformdx = transform.ToSharpDXMatrix(true);
                        transformdx.TranslationVector = Vector3.Zero;

                        List<Vector3> vectors = new List<Vector3>();
                        foreach (var meshind in currentNode.MeshIndices)
                        {
                            Assimp.Mesh mesh = assimpScene.Meshes[meshind];
                            vectors.AddRange(mesh.Vertices.Select(x => Vector3.Transform(x.ToSharpDXVector3(),transformdx).ToVector3()));
                        }

                        meshVertexes = vectors.Distinct().ToArray();
                        return;
                    }
                }
                else
                {
                    items.RemoveAt(items.Count - 1);
                    currentNode = currentNode.Parent;
                }
            }
            throw new NiobException(assimpScene, Stressgrund.NoLootAtLootLake);
        }

        public void updateRadius()
        {
            float maxd = 0;
            foreach (var item in meshVertexes)
            {
                float d = item.LengthSquared();
                if (d > maxd)
                {
                    maxd = d;
                }
            }
            boundingRadius = MathF.Sqrt(maxd);
        }


        public Vector3 GetSupport(Vector3 Direction)
        {
            float min = float.MaxValue;
            Vector3 minV = Vector3.Zero;
            foreach (Vector3 vertex in meshVertexes)
            {
                float d = Vector3.Dot(Direction, vertex);
                if (d<min)
                {
                    min = d;
                    minV = vertex;
                }
            }

            return minV;
        }

    }

    internal class MultiBounding : IBoundingShape
    {
        private float boundingRadius;

        public struct PositionedShape
        {
            public Vector3 Position;
            public Quaternion Rotation;
            public IBoundingShape BoundingShape;

            public PositionedShape(Vector3 position, Quaternion rotation, IBoundingShape boundingShape) {
                Position = position;
                Rotation = rotation;
                BoundingShape = boundingShape;
            }
        }
        private List<PositionedShape> shapes = new List<PositionedShape>();

        public List<PositionedShape> Shapes
        {
            get { return shapes; }
        }

        public float BoundingRadius
        {
            get { return boundingRadius; }
        }

        public Matrix3x3 InertiaTensor
        {
            get
            {
                return (boundingRadius * boundingRadius * (2f / 5f)) * SharpDX.Matrix3x3.Identity;
            }
        }

        public void updateRadius()
        {
            float maxd = 0;
            foreach (var item in shapes)
            {
                float d = item.Position.Length() + item.BoundingShape.BoundingRadius;
                if (d > maxd)   
                {
                    maxd = d;
                }
            }
            boundingRadius = maxd;
        }

        public MultiBounding()
        {

        }

        public MultiBounding(string filename)
        {
            Assembly assembly = Assembly.GetEntryAssembly();

            string FilePath = assembly.GetManifestResourceNames().Where(x => x.EndsWith(filename)).FirstOrDefault();
            if (FilePath == null) throw new Exception("FileNotFound");

            Assimp.AssimpContext assimpContext = new Assimp.AssimpContext() { Scale = 0.1f };
            Assimp.Scene assimpScene = assimpContext.ImportFileFromStream(assembly.GetManifestResourceStream(FilePath), Assimp.PostProcessSteps.JoinIdenticalVertices
                                                                                                                      | Assimp.PostProcessSteps.FindDegenerates 
                                                                                                                      | Assimp.PostProcessSteps.SortByPrimitiveType 
                                                                                                                      | Assimp.PostProcessSteps.RemoveRedundantMaterials 
                                                                                                                      | Assimp.PostProcessSteps.FlipUVs);
            
            if (assimpScene.MeshCount == 0)
                throw new NiobException(assimpScene, Stressgrund.NoLootAtLootLake);

            List<int> items = new List<int>() { 0};
            Assimp.Node currentNode = assimpScene.RootNode;
            while (items.Count>0)
            {
                if (currentNode.ChildCount>items[items.Count-1])
                {
                    currentNode = currentNode.Children[items[items.Count-1]];
                    items[items.Count - 1]++;
                    items.Add(0);

                    if (currentNode.HasMeshes)
                    {
                        Assimp.Matrix4x4 transform = currentNode.Transform;
                        Assimp.Node parent = currentNode.Parent;
                        while (parent is not null)
                        {
                            transform = parent.Transform * transform;
                            parent = parent.Parent;
                        }

                        transform.Decompose2Sharp(out Matrix3x3 scscTrans, out Quaternion rotation, out Vector3 position);

                        List<Vector3> vectors = new List<Vector3>();
                        foreach (var meshind in currentNode.MeshIndices)
                        {
                            Assimp.Mesh mesh = assimpScene.Meshes[meshind];
                            vectors.AddRange(mesh.Vertices.Select(x => scscTrans.Multiply(x.ToSharpDXVector3())));
                        }

                        ConvexMeshBounding bounding = new ConvexMeshBounding(vectors.Distinct().ToArray());

                        shapes.Add(new PositionedShape(position, rotation, bounding));
                    }
                }
                else
                {
                    items.RemoveAt(items.Count-1);
                    currentNode = currentNode.Parent;
                }
            }
        }
    }

    public class CuboidBounding :IBoundingShape, IGJKBounding
    {
        private Vector3 size;

        public Vector3 Size
        {
            get { return size; }
            set {
                if (value.X <= 0|| value.Y <= 0 || value.Z <= 0) throw new NiobException(value,Stressgrund.OutOfRange);
                size = value;
                boundingRadius =(float)MathF.Sqrt(value.X*value.X + value.Y*value.Y + value.Z*value.Z)/2f;
            }
        }

        private float boundingRadius;

        public float BoundingRadius
        {
            get { return boundingRadius; }
        }

        public Matrix3x3 InertiaTensor
        {
            get
            {
                return new Matrix3x3((size.Y * size.Y + size.Z * size.Z) / 12f, 0, 0,
                                     0, (size.X * size.X + size.Z * size.Z) / 12f, 0,
                                     0, 0, (size.X * size.X + size.Y * size.Y) / 12f);
            }
        }

        public CuboidBounding() : this(1, 1, 1) { }
        public CuboidBounding(float x,float y, float z)
        {
            Size = new Vector3(x,y,z);
        }


        public Vector3 GetSupport(Vector3 Direction)
        {
            return new Vector3(MathF.CopySign(size.X / 2, Direction.X),
                               MathF.CopySign(size.Y / 2, Direction.Y),
                               MathF.CopySign(size.Z / 2, Direction.Z));
        }
    }

    public class EllipsoidBounding : IBoundingShape, IGJKBounding
    {
        private Vector3 size;

        public Vector3 Size
        {
            get { return size; }
            set
            {
                if (value.X <= 0 || value.Y <= 0 || value.Z <= 0) throw new NiobException(value, Stressgrund.OutOfRange);
                size = value;
                boundingRadius = MathF.Max(value.X, MathF.Max(value.Y, value.Z))/2;
            }
        }


        private float boundingRadius;

        public float BoundingRadius
        {
            get { return boundingRadius; }
        }

        public Matrix3x3 InertiaTensor
        {
            get
            {
                return new Matrix3x3((size.Y * size.Y + size.Z * size.Z) / 5f * 4f, 0, 0,
                                     0, (size.X * size.X + size.Z * size.Z) / 5f * 4f, 0,
                                     0, 0, (size.X * size.X + size.Y * size.Y) / 5f * 4f);
            }
        }

        public EllipsoidBounding() : this(1, 1, 1) { }
        public EllipsoidBounding(float x, float y, float z)
        {
            Size = new Vector3(x, y, z);
        }

        public Vector3 GetSupport(Vector3 Direction)
        {
            float x = Direction.X * size.X / 2;
            float y = Direction.Y * size.Y / 2;
            float z = Direction.Z * size.Z / 2;

            float M = 1 / (float)MathF.Sqrt(x * x + y * y + z * z);

            return new Vector3(x * size.X / 2 * M, y * size.Y / 2 * M, z * size.Z / 2 * M);
        }
    }

    public class CylinderBounding:IBoundingShape, IGJKBounding
    {
        private Vector3 size;

        public Vector3 Size
        {
            get { return size; }
            set
            {
                if (value.X <= 0 || value.Y <= 0 || value.Z <= 0) throw new NiobException(value, Stressgrund.OutOfRange);
                size = value;
                boundingRadius = (float)MathF.Sqrt(value.X * value.X + value.Y * value.Y + value.Z * value.Z) / 2f;
            }
        }

        private float boundingRadius;

        public float BoundingRadius
        {
            get { return boundingRadius; }
        }

        private Axis axis;

        public Axis Axis
        {
            get { return axis; }
            set { axis = value; }
        }

        public Matrix3x3 InertiaTensor
        {
            get
            {
                switch (axis)
                {
                    case Axis.X:
                        return new Matrix3x3((size.Y * size.Y + size.Z * size.Z) / 4f * 4f, 0, 0,
                                             0, (size.X * size.X + 3 * size.Z * size.Z) / 12f * 4f, 0,
                                             0, 0, (size.X * size.X + 3 * size.Y * size.Y) / 12f * 4f);
                    case Axis.Y:
                        return new Matrix3x3((size.Y * size.Y + 3 * size.Z * size.Z) / 12f * 4f, 0, 0,
                                             0, (size.X * size.X + size.Z * size.Z) / 4f * 4f, 0,
                                             0, 0, (3 * size.X * size.X + size.Y * size.Y) / 12f * 4f);
                    case Axis.Z:
                        return new Matrix3x3((3 * size.Y * size.Y + size.Z * size.Z) / 12f * 4f, 0, 0,
                                             0, (3 * size.X * size.X + size.Z * size.Z) / 12f * 4f, 0,
                                             0, 0, (size.X * size.X + size.Y * size.Y) / 4f * 4f);
                    default:
                        return new Matrix3x3((size.Y * size.Y + size.Z * size.Z) / 5f * 4f, 0, 0,
                                             0, (size.X * size.X + size.Z * size.Z) / 5f * 4f, 0,
                                             0, 0, (size.X * size.X + size.Y * size.Y) / 5f * 4f);
                }
            }
        }

        public CylinderBounding() : this(1, 1, 1, Axis.X) { }
        public CylinderBounding(float x, float y, float z, Axis axis)
        {
            Size = new Vector3(x, y, z);
            this.axis = axis;
        }

        public Vector3 GetSupport(Vector3 Direction)
        {

            switch (axis)
            {
                case Axis.X:
                    {
                        float y = Direction.Y * size.Y / 2;
                        float z = Direction.Z * size.Z / 2;
                        float M = 1 / (float)MathF.Sqrt(y * y + z * z);
                        if (M is float.PositiveInfinity)
                        {
                            M = 0;
                        }

                        return new Vector3(MathF.CopySign(size.X / 2, Direction.X), 
                                           y * size.Y / 2 * M, 
                                           z * size.Z / 2 * M);
                    }
                case Axis.Y:
                    {
                        float x = Direction.X * size.X / 2;
                        float z = Direction.Z * size.Z / 2;
                        float M = 1 / (float)MathF.Sqrt(x * x + z * z);
                        if (M is float.PositiveInfinity)
                        {
                            M = 0;
                        }

                        return new Vector3(x * size.X / 2 * M,
                                           MathF.CopySign(size.Y / 2, Direction.Y), 
                                           z * size.Z / 2 * M);
                    }
                case Axis.Z:
                    {
                        float x = Direction.X * size.X / 2;
                        float y = Direction.Y * size.Y / 2;
                        float M = 1 / (float)MathF.Sqrt(x * x + y * y);
                        if (M is float.PositiveInfinity)
                        {
                            M = 0;
                        }

                        return new Vector3(x * size.X / 2 * M, 
                                           y * size.Y / 2 * M, 
                                           MathF.CopySign(size.Z / 2, Direction.Z));
                    }
                default:
                    return Vector3.Zero;
            }


        }
    }
}
