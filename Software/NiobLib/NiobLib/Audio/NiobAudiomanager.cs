﻿using OpenTK;
using NAudio;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.IO.Ports;
using NiobLib.Entities;

namespace NiobLib.Audio
{
    /// <summary>
    /// Initialise this class just once and do it before using the sound and music class
    /// </summary>
    public class NiobAudiomanager
    {
        private List<ContextHandle> contextList = new List<ContextHandle>();
        public List<NiobSoundSource> soundList = new List<NiobSoundSource>();
        public NiobMusic music;
        public List<ContextHandle> ContextList
        {
            get => contextList;
            set {contextList = value;}
        }

        public bool DeviceDefined { get; set; } = false;
        public bool ContextDefined { get; set; } = false;
        public bool DeviceGettingDefined { get; set; } = false;
        public IntPtr DeviceOfManager { get; set; }

        public NiobAudiomanager(List<string> musicFilenameList, int sampleFrequency = 0, bool startAfterInit = true, bool loop = true, bool loopCurrent = false)
        {
            music = new NiobMusic(this, musicFilenameList, startAfterInit, loopCurrent, loop, sampleFrequency);
        }

        public NiobAudiomanager(byte[] soundData, int amountOfBitsPerSample, int sampleFrequency, bool stereo = false, bool startAfterInit = true, bool loop = true, bool overrideCurrentQueue = true)
        {
            music = new NiobMusic(this, soundData, sampleFrequency, stereo, amountOfBitsPerSample, startAfterInit, loop, overrideCurrentQueue);
        }
        #region Music
        /// <summary>
        /// Not nessecary, but recommended
        /// </summary>
        /// <param name="closeContext">Needs to be closed for closing the device (do not close when you still want to play other sounds/music)</param>
        /// <param name="closeDevice">Can just be closed when context is getting closed</param>
        public void MusicKill(bool closeContext, bool closeDevice, bool deleteBuffer)
        {
            music.NiobMKill(this, closeContext, closeDevice, deleteBuffer);
        }

        /// <summary>
        /// Make a new music, start from initial (delete Queue/settings)
        /// </summary>
        /// <param name="filename">File to play</param>
        /// <param name="sampleFrequency">0 for auto</param>
        /// <param name="startAfterInit"></param>
        /// <param name="loop">repeat file</param>
        public void MusicMakeNew(List<string> queue, int sampleFrequency = 0, bool startAfterInit = true, bool loopCurrent = false, bool loop = true)
        {
            MusicKill(false, false, true);
            music = new NiobMusic(this, queue, startAfterInit, loopCurrent, loop, sampleFrequency);
        }

        public List<string> MusicQueueGet()
        {
            return music.Queue;
        }

        public bool MusicQueueSet(List<string> queue)
        {
            try
            {
                music.Queue = queue;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool MusicQueueSet(string song)
        {
            try
            {
                List<string> l = new List<string>();
                l.Add(song);
                music.Queue = l;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool MusicQueueAdd(List<string> songs)
        {
            try
            {
                foreach (string song in songs)
                {
                    music.Queue.Add(song);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool MusicQueueAdd(string song)
        {
            try
            {
                music.Queue.Add(song);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Pauses the music</summary>
        /// <returns>true on successful execution of command</returns>
        public void MusicPause()
        {
            music.NiobMPause();
        }

        /// <summary>Stops the player and sets its state to Initial</summary>
        /// <returns>true on successful execution of command</returns>
        public void MusicReset()
        {
            music.NiobMReset();
        }

        /// <summary>Start the player</summary>
        /// <returns>true on successful execution of command</returns>
        /// <param name="filename">File to play</param>
        /// <param name="loopCurrent">Repeat after coming to the end of the file</param>
        public void MusicStart(string filename, bool overrideQueue = false, bool loopCurrent = true)
        {
            music.NiobMStart(filename, loopCurrent);
        }

        /// <summary>
        /// Resumes or replays the current music source
        /// </summary>
        /// <param name="restartWhenAlreadyPlaying">Set this parameter on
        /// true for restarting when called on an already playing source
        /// <br>false by default</br></param>
        /// <returns>true on successful execution of command</returns>
        public void MusicStart(bool restartWhenAlreadyPlaying = false)
        {
            music.NiobMStart(restartWhenAlreadyPlaying);
        }

        /// <summary>Start the player
        /// <br>This function is only needed when you want to play your own array of soundData</br></summary>
        /// <returns>true on successful execution of command</returns>
        /// <param name="amountOfBitsPerSample"></param>
        /// <param name="soundData">Data to play</param>
        /// <param name="loop">Repeat after finishing, true by default</param>
        /// <param name="sampleFrequency">Leave 0 for automatic/not changed to before (when playing file)</param>
        /// <param name="stereo"></param>
        public void MusicMakeNew(byte[] soundData, int sampleFrequency, int amountOfBitsPerSample, bool loop = true, bool stereo = false, bool overrideQueue = true)
        {
            music.NiobMStart(soundData, sampleFrequency, amountOfBitsPerSample, loop, stereo, overrideQueue);
        }

        /// <summary>Stops the music</summary>
        /// <returns>true on successful execution of command</returns>
        public void MusicStop()
        {
            music.NiobMStop();
        }

        /// <summary>
        /// Set the pitch factor for all Sounds
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public void MusicPitch(float pitch)
        {
            music.Pitch = pitch;
        }

        /// <summary>
        /// Set the volume which will not be exceeded
        /// </summary>
        /// <param name="maxVolume"> 1>= float value >=0
        /// <br>if out of bounds -> bound</br>
        /// <br>Logarithmic</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public void MusicVolumeMaxSet(float maxVolume)
        {
            music.NiobMSetMaxVolume(maxVolume);
        }

        /// <summary>
        /// Set the volume which will not be undercut
        /// </summary>
        /// <param name="minVolume">1>=float value >= 0
        /// <br>if out of bounds -> bound</br></param>
        /// <returns>true on successful execution</returns>
        public void MusicVolumeMinSet(float minVolume)
        {
            music.NiobMSetMinVolume(minVolume);
        }

        /// <summary>
        /// Set the volume factor
        /// <br>Alternatively set the Volume Property</br>
        /// </summary>
        /// <param name="volume">float value >= 0, if out of bounds -> bound; don't forget logarithmic scala
        ///     <br>1 means normal volume (±0dB)</br>
        ///     <br>0 means muted (-∞dB)</br>
        ///     <br>2 means +6dB</br>
        ///     <br>0.5 means -6dB</br>
        ///     <br>4 means +12dB</br>
        ///     <br>and so on</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public void MusicVolumeSet(float volume)
        {
            music.NiobMSetVolume(volume);
        }
        #endregion

        #region Sounds
        public void SoundAdd(NiobEntity3d entity, string filename, bool startAfterInit = true, bool loop = false, int sampleFrequency = 0)
        {
            NiobSoundSource sSource = new NiobSoundSource(this, entity, filename, startAfterInit, loop, sampleFrequency);
            soundList.Add(sSource);
        }

        /// <summary>
        /// Set the pitch factor for all Sounds
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public void SoundsPitch(float pitch)
        {
            music.Pitch = pitch;
        }

        public void SoundKill(string nameOfSound_s_to_kill, bool closeContext, bool closeDevice, bool deleteBuffer)
        {
        }

        public void SoundStop()
        {
        }

        public void SoundPause()
        {
        }

        public void SoundVolumeSet(float volume)
        {

        }

        public void SoundVolumeMinSet()
        {

        }

        public void SoundVolumeMaxSet(float maxVolume)
        {

        }
        #endregion

        #region Master
        /// <summary>
        /// Set the pitch factor
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public void MasterPitch(float pitch)
        {
            music.Pitch = pitch;
        }

        public void MasterKill(string nameOfSound_s_to_kill, bool closeContext, bool closeDevice, bool deleteBuffer)
        {
        }

        public void MasterStop()
        {
        }

        public void MasterPause()
        {
        }

        public void MasterVolumeSet(float volume)
        {

        }

        public void MasterVolumeMinSet()
        {

        }

        public void MasterVolumeMaxSet(float maxVolume)
        {

        }
        #endregion

        /// <summary>
        /// Sample for how to create a sound array on yourself
        /// </summary>
        /// <param name="sampleFrequency">Frequency for timestep calculations</param>
        /// <param name="frequencyOfSin">The </param>
        /// <param name="amp"></param>
        /// <returns>an array with sinus data of the specified sample frequency</returns>
        public byte[] GetSinusSoundData(int frequencyOfSin, int sampleFrequency, double amp)
        {
            double dt = 2 * System.Math.PI / sampleFrequency;

            int freq = frequencyOfSin;
            var dataCount = sampleFrequency / freq;

            var sinData = new short[dataCount];
            for (int i = 0; i < sinData.Length; ++i)
            {
                sinData[i] = (short)(amp * short.MaxValue * System.Math.Sin(i * dt * freq));
            }
            byte[] sindataByted = new byte[sinData.Length];
            for (int i = 0; i < sinData.Length; i++)
            {
                sindataByted[i] = Convert.ToByte(sinData[i]);
            }
            return sindataByted;
        }
    }
}
