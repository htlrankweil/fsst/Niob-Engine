﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Audio.OpenAL;
using NAudio.Wave;

namespace NiobLib.Audio
{

    public class NiobMusic
    {

        #region Variables/Properties
        internal int amountOfBits;
        internal int buffer;
        private ContextHandle context;
        private int currentSongPositionInQueue;
        private IntPtr device;
        private string filePlayingRel;
        private string filePlayingAbs;
        private bool loop;
        private bool loopCurrent;
        private float maxVolume;
        private float minVolume;
        private float pitch;
        private List<string> queue;
        private int sampleFrequency;
        private int source;
        private bool stereo;
        private float volume;
        internal float volInternal;

        public int AmountOfBits
        {
            get { return amountOfBits; }
            private set { amountOfBits = value; }
        }
        public int Buffer
        {
            get { return buffer; }
        }
        public ContextHandle Context
        {
            get { return context; }
            set { context = value; }
        }
        public int CurrentSongPositionInQueue
        {
            get { return currentSongPositionInQueue; }
            set { currentSongPositionInQueue = value; }
        }
        public IntPtr Device
        {
            get { return device; }
            set { device = value; }
        }
        public string FilePlayingAbsolute
        {
            get { return filePlayingAbs; }
        }
        public string FilePlayingRelative
        {
            get { return filePlayingRel; }
        }
        public bool Loop
        {
            get { return loop; }
            private set { loop = value; }
        }
        public bool LoopCurrent
        {
            get { return loopCurrent; }
            set { AL.Source(source, ALSourceb.Looping, value); }
        }
        public float MaxVolume
        {
            get { return maxVolume; }
        }
        public float MinVolume
        {
            get { return minVolume; }
        }
        public float Pitch
        {
            get { return pitch; }
            set
            {
                NiobMSetPitch(value);
            }
        }
        public List<string> Queue
        {
            get { return queue; }
            set { queue = value; }
        }
        public int SampleFrequency
        {
            get { return sampleFrequency; }
            private set { sampleFrequency = value; }
        }
        public int Source
        {
            get { return source; }
        }
        public bool Stereo
        {
            get { return stereo; }
            private set { stereo = value; }
        }
        public float Volume
        {
            get { return volume; }
            set { NiobMSetVolume(value); }
        }
        #endregion

        /// <summary>
        /// Initialises a music player for the specified path (.wav-File only) and starts it if param set (or not modified)
        /// <br>Use this constructor when you want to play a file</br>
        /// </summary>
        public NiobMusic(NiobAudiomanager manager, List<string> queueOfFilenames, bool startAfterInit, bool loopCurrent, bool loop = true, int sampleFrequency = 0)
        {
            string filename = queueOfFilenames[0];
            SampleFrequency = sampleFrequency;
            var metadataReader = new WaveFileReader(filename).WaveFormat;
            if (SampleFrequency == 0) SampleFrequency = metadataReader.SampleRate;
            if (metadataReader.Channels > 1) stereo = true;
            else stereo = false;
            amountOfBits = 8;
            if (metadataReader.BitsPerSample == 16) amountOfBits = 16;
            this.loop = loop;
            _NiobMInit(ref manager);
            if (startAfterInit) NiobMStart(filename, loopCurrent);
        }

        /// <summary>
        /// Initialises a music player for an array
        /// <br>Use this constructor when you want to play your own soundData array</br>
        /// </summary>
        /// <param name="startAfterInit">true by default</param>
        public NiobMusic(NiobAudiomanager manager, byte[] soundData, int sampleFrequency, bool stereo, int amountOfBitsPerSample, bool startAfterInit, bool loop, bool overrideQueue)
        {
            SampleFrequency = sampleFrequency;
            this.stereo = stereo;
            amountOfBits = amountOfBitsPerSample;
            _NiobMInit(ref manager);
            if (startAfterInit) NiobMStart(soundData, sampleFrequency, amountOfBitsPerSample, loop, stereo, overrideQueue);
        }

        /// <summary>
        /// Initialise a new device and a new context
        /// <br>(just nessecary once or after kill)</br>
        /// </summary>
        /// <param name="manager"></param>
        /// <returns>The first error occured during the initialisation</returns>
        private unsafe ALError _NiobMInit(ref NiobAudiomanager manager)
        {
            // Initialisieren
            AL.GetError();  // Abfragen und Zurücksetzen der Fehler
            if (!manager.DeviceDefined)
            {
                while (manager.DeviceGettingDefined) ;
                manager.DeviceGettingDefined = true;
                if (!manager.DeviceDefined)
                {
                    manager.DeviceOfManager = Alc.OpenDevice(null);
                    manager.DeviceDefined = true;
                    device = manager.DeviceOfManager;

                    ContextHandle c = Alc.CreateContext(Device, (int*)null);
                    if (!manager.ContextList.Contains(c))
                    {
                        manager.ContextList.Add(c);
                    }
                    context = c;
                    Alc.MakeContextCurrent(Context);    // Festlegen, welcher Context verwendet werden soll
                }
                manager.DeviceGettingDefined = false;
            }
            else
            {
                Device = manager.DeviceOfManager;
                Context = manager.ContextList.Last();
            }

            // Generieren von Sound-Buffer(n) / Soundquellen
            Alc.MakeContextCurrent(context); 
            AL.GenBuffers(1, out buffer);
            AL.GenSources(1, out source);
            return AL.GetError();
        }

        #region Start music
        /// <summary>Start the player</summary>
        /// <returns>true on successful execution of command</returns>
        /// <param name="filename">File to play</param>
        /// <param name="loopCurrent">Repeat after coming to the end of the file</param>
        public bool NiobMStart(string filename, bool loopCurrent)
        {
            try
            {
                filePlayingAbs = Path.GetFullPath(filename);
                filePlayingRel = Path.GetRelativePath(Directory.GetCurrentDirectory(), filename);
                byte[] soundData = System.IO.File.ReadAllBytes(filename);

                return _startMusic(soundData, loopCurrent);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Start the player</summary>
        /// <returns>true on successful execution of command</returns>
        /// <param name="soundData">Data to play</param>
        /// <param name="loop">Repeat after finishing</param>
        /// <param name="sampleFrequency">Leave 0 for automatic/not changed to before (when playing file)</param>
        public bool NiobMStart(byte[] soundData, int sampleFrequency, int amountOfBitsPerSample, bool loop, bool stereo, bool overrideQueue)
        {
            try
            {
                AmountOfBits = amountOfBitsPerSample;
                Loop = loop;
                SampleFrequency = sampleFrequency;
                Stereo = stereo;
                return _startMusic(soundData, loop, sampleFrequency);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool _startMusic(byte[] soundData, bool loopCurrent, int sampleFreq = 0)
        {
            try
            {
                if (sampleFreq != 0)
                {
                    sampleFrequency = sampleFreq;
                }
                Alc.MakeContextCurrent(context);
                // Laden der Sounddaten in einen Buffer
                switch (amountOfBits)   // amountObBits ist die Anzahl der Bits, mit der 
                {
                    case 8:
                        if (stereo)
                            AL.BufferData(buffer, ALFormat.Mono8, soundData, soundData.Length, 2 * sampleFrequency);
                        else
                            AL.BufferData(buffer, ALFormat.Mono8, soundData, soundData.Length, sampleFrequency);
                        break;
                    case 16:
                        if (stereo)
                            AL.BufferData(buffer, ALFormat.Mono16, soundData, soundData.Length, 2 * sampleFrequency);
                        else
                            AL.BufferData(buffer, ALFormat.Mono16, soundData, soundData.Length, sampleFrequency);
                        break;
                    default:
                        AL.BufferData(buffer, ALFormat.Mono16, soundData, soundData.Length, sampleFrequency);
                        break;
                }
                AL.Source(source, ALSourcei.Buffer, buffer);

                if (loop) AL.Source(source, ALSourceb.Looping, true);
                else AL.Source(source, ALSourceb.Looping, false);

                // Klang wirklich abspielen
                AL.SourcePlay(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Resumes or replays the current song
        /// </summary>
        /// <param name="restartWhenAlreadyPlaying">Set this parameter on
        /// true for restarting when called on an already playing source
        /// <br>false by default</br></param>
        /// <returns>true on successful execution of command</returns>
        public bool NiobMStart(bool restartWhenAlreadyPlaying)
        {
            try
            {
                Alc.MakeContextCurrent(context);
                if (restartWhenAlreadyPlaying) AL.SourcePlay(source);
                else if (AL.GetSourceState(source) != ALSourceState.Playing) AL.SourcePlay(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Pause/Stop/Reset music
        /// <summary>Pauses the music</summary>
        /// <returns>true on successful execution of command</returns>
        public bool NiobMPause()
        {
            try
            {
                Alc.MakeContextCurrent(context);
                AL.SourcePause(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Stops the music</summary>
        /// <returns>true on successful execution of command</returns>
        public bool NiobMStop()
        {
            try
            {
                Alc.MakeContextCurrent(context);
                AL.SourceStop(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Stops the player and sets its state to Initial</summary>
        /// <returns>true on successful execution of command</returns>
        public bool NiobMReset()
        {
            try
            {
                Alc.MakeContextCurrent(context);
                AL.SourceRewind(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="closeContext">Needs to be closed for closing the device (do not close when you still want to play other sounds/music)</param>
        /// <param name="closeDevice">Can just be closed when context is getting closed</param>
        public void NiobMKill(NiobAudiomanager manager, bool closeContext, bool closeDevice, bool deleteBuffer)
        {
            if (closeContext)
            {
                if (context != ContextHandle.Zero)
                {
                    Alc.MakeContextCurrent(ContextHandle.Zero);
                    Alc.DestroyContext(context);
                }
                context = ContextHandle.Zero;
                manager.ContextDefined = false;

                if (closeDevice)
                {
                    if (device != IntPtr.Zero)
                    {
                        Alc.CloseDevice(device);
                    }
                    device = IntPtr.Zero;
                    manager.DeviceDefined = false;
                }
            }
            AL.DeleteSource(source);
            if (deleteBuffer) AL.DeleteBuffer(buffer);
        }
        #endregion

        #region Volume
        /// <summary>
        /// Set the volume factor
        /// <br>Alternatively set the Volume Property</br>
        /// </summary>
        /// <param name="volume">float value >= 0, if out of bounds -> bound; don't forget logarithmic scala
        ///     <br>1 means normal volume (±0dB)</br>
        ///     <br>0 means muted (-∞dB)</br>
        ///     <br>2 means +6dB</br>
        ///     <br>0.5 means -6dB</br>
        ///     <br>4 means +12dB</br>
        ///     <br>and so on</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool NiobMSetVolume(float volume)
        {
            try
            {
                if (volume < 0)
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.Gain, 0);
                    this.volume = 0;
                }
                else
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.Gain, volume);
                    this.volume = volume;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Set the volume which will not be undercut
        /// </summary>
        /// <param name="minVolume">1>=float value >= 0
        /// <br>if out of bounds -> bound</br></param>
        /// <returns>true on successful execution</returns>
        public bool NiobMSetMinVolume(float minVolume)
        {
            try
            {
                if (minVolume < 0)
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.MinGain, 0);
                    this.minVolume = 0;
                }
                else
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.MinGain, minVolume);
                    this.minVolume = minVolume;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Set the volume which will not be exceeded
        /// </summary>
        /// <param name="maxVolume"> 1>= float value >=0
        /// <br>if out of bounds -> bound</br>
        /// <br>Logarithmic</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool NiobMSetMaxVolume(float maxVolume)
        {
            try
            {
                if (maxVolume < 0)
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.MaxGain, 0);
                    this.maxVolume = 0;
                }
                else if (maxVolume > 1)
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.MaxGain, 1);
                    this.maxVolume = 1;
                }
                else
                {
                    Alc.MakeContextCurrent(context);
                    AL.Source(source, ALSourcef.MaxGain, maxVolume);
                    this.maxVolume = maxVolume;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Modify music
        /// <summary>
        /// Set the pitch factor
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool NiobMSetPitch(float pitch)
        {
            try
            {
                if (pitch < 0)
                {
                    this.pitch = 0;
                }
                Alc.MakeContextCurrent(context);
                AL.Source(source, ALSourcef.Pitch, this.pitch);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Create
        //public byte[] GetSinusSoundData(int sampleFreq)
        //{
        //    double dt = 2 * System.Math.PI / sampleFreq;
        //    double amp = 0.5;

        //    int freq = 98;
        //    var dataCount = sampleFreq / freq;

        //    var sinData = new short[dataCount];
        //    for (int i = 0; i < sinData.Length; ++i)
        //    {
        //        sinData[i] = (short)(amp * short.MaxValue * System.Math.Sin(i * dt * freq));
        //    }
        //    byte[] sindataByted = new byte[sinData.Length];
        //    for (int i = 0; i < sinData.Length; i++)
        //    {
        //        sindataByted[i] = Convert.ToByte(sinData[i]);
        //    }
        //    return sindataByted;
        //}


        /// <summary>
        /// <br>Use like:</br>
        /// <br>NiobMusic player = new NiobMusic(startAfterInit: false);</br>
        /// <br>player.NiobMStart(player.NiobMP3ToByteArray(@"C:\sound.mp3"));</br>
        /// </summary>
        /// <param name="filename">File to convert to a byte-array</param>
        /// <returns>The converted MP3-File (in byte[]-format</returns>
        public byte[] NiobMP3ToByteArray(string filename)
        {
            using (var mp3Reader = new Mp3FileReader(filename))
            {
                using (var pcmStream = WaveFormatConversionStream.CreatePcmStream(mp3Reader))
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        var waveWriter = new WaveFileWriter(memoryStream, pcmStream.WaveFormat);
                        pcmStream.CopyTo(waveWriter);
                        waveWriter.Flush();
                        var byteArray = memoryStream.ToArray();
                        return byteArray;
                    }
                }
            }
        }

        // */
        #endregion
    }
}


