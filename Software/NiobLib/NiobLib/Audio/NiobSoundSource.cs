﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NAudio.Gui;
using OpenTK.Audio.OpenAL;
using NAudio.Wave;
using OpenTK;
using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D9;
using NiobLib.Entities;

namespace NiobLib.Audio
{
    public class NiobSoundSource
    {
        internal NSource leftNSource;
        internal NSource rightNSource;
        #region Variables/Properties

        internal int amountOfBits;
        internal int buffer;
        internal ContextHandle context;
        internal IntPtr device;
        internal IntPtr disableAirAbsorption;
        internal IntPtr disableDoppler;
        internal string fileToPlay;
        internal bool loop;
        internal float maxVolume = 1;
        internal float minVolume = 0;
        internal string name;
        internal float pitch = 1;
        internal SharpDX.Vector3 orientationListener = new SharpDX.Vector3();
        internal SharpDX.Vector3 orientationSource = new SharpDX.Vector3();
        internal SharpDX.Vector3 positionListener = new SharpDX.Vector3();
        internal SharpDX.Vector3 positionSource = new SharpDX.Vector3();
        internal int sampleFrequency;
        internal bool stereo;
        internal bool swapLR;
        internal SharpDX.Vector3 velocityListener = new SharpDX.Vector3();
        internal SharpDX.Vector3 velocitySource = new SharpDX.Vector3();
        internal float volume;

        internal int Buffer { get => buffer; private set => buffer = value; }
        internal ContextHandle Context { get => context; private set => context = value; }
        internal IntPtr Device { get => device; private set => device = value; }

        public int AmountOfBits
        { get => amountOfBits; private set => amountOfBits = value; }
        public string FileToPlay
        { get => fileToPlay; private set => fileToPlay = value; }
        public int SampleFrequency
        { get => sampleFrequency; private set => sampleFrequency = value; }
        public bool Stereo { get => stereo; private set => stereo = value; }
        public string Name { get => name; set => name = value; }

        public NiobEntity3d entity
        {
            get => entity;
            set
            { entity = value; }
        }
        public bool Loop
        {
            get => loop;
            set
            {
                loop = value;
                leftNSource.SLoop(true);
            }
        }
        public float MaxVolume
        {
            get => maxVolume; private set
            {
                maxVolume = value;
                SSetMaxVolume(value);
            }
        }
        public float MinVolume
        {
            get => minVolume;
            set
            {
                minVolume = value;
                SSetMinVolume(value);
            }
        }
        public float Pitch
        {
            get => pitch;
            set
            {
                pitch = value;
                SSetPitch(value);
            }
        }
        public SharpDX.Vector3 OrientationListener
        {
            get { return orientationListener; }
            set
            {
                SSetOrientationOfListener(value);
            }
        }
        public SharpDX.Vector3 PositionListener
        {
            get { return positionListener; }
            set
            {
                SSetPositionOfListener(value);
            }
        }
        public SharpDX.Vector3 PositionSource
        {
            get { return positionSource; }
            set
            {
                SSetPositionOfSource(value);
            }
        }
        public bool SwapLR
        {
            get { return swapLR; }
            set
            {
                SwapLeftAndRight(value);
            }
        }

        private void SwapLeftAndRight(bool value)
        {
            
        }

        public SharpDX.Vector3 VelocityListener
        {
            get { return velocityListener; }
            set
            {
                SSetVelocityOfListener(value);
            }
        }
        public SharpDX.Vector3 VelocitySource
        {
            get { return velocitySource; }
            set
            {
                SSetVelocityOfSource(value);
            }
        }
        public float Volume
        {
            get { return volume; }
            set
            {
                volume = value;
                SSetVolume(value);
            }
        }
        #endregion

        /// <summary>
        /// Initialises a sound player for the specified path (.wav-File only) and starts it if param set (modified)
        /// <br>All parameters (except the mangager) can be changed about the properties</br>
        /// </summary>
        /// <param name="manager">NiobAudioManager for no crashes</param>
        /// <param name="filename">.WAV-file to play</param>
        /// <param name="startAfterInit">start when finishied with initialising</param>
        /// <param name="loop">repeat after finishing playing the file</param>
        /// <param name="sampleFrequency">Will be detected automatically when value=0</param>
        public NiobSoundSource(NiobAudiomanager manager, NiobEntity3d entity, string filename = "Wow.wav", bool startAfterInit = true, bool loop = false, int sampleFrequency = 0)
        {
            this.entity = entity;
            var metadataReader = new WaveFileReader(filename).WaveFormat;
            if (sampleFrequency == 0) this.sampleFrequency = metadataReader.SampleRate;
            if (metadataReader.Channels > 1) stereo = true;
            else stereo = false;
            amountOfBits = 8;
            if (metadataReader.BitsPerSample == 16) amountOfBits = 16;
            fileToPlay = filename;
            this.loop = loop;

            Init(ref manager);
            leftNSource = new NSource(true, startAfterInit, this, ref manager);
            rightNSource = new NSource(false, startAfterInit, this, ref manager);
        }

        /// <summary>
        /// Initialise a new device and a new context
        /// <br>(just nessecary once or after one or both of them killed)</br>
        /// </summary>
        /// <param name="manager"></param>
        /// <returns>The first error occured during the initialisation</returns>
        private unsafe ALError Init(ref NiobAudiomanager manager)
        {
            // Initialise
            AL.GetError();  // Get and reset all errors
            if (!manager.DeviceDefined)
            {
                while (manager.DeviceGettingDefined) ;
                manager.DeviceGettingDefined = true;
                if (!manager.DeviceDefined)
                {
                    manager.DeviceOfManager = Alc.OpenDevice(null);
                    manager.DeviceDefined = true;
                    //Device = manager.DeviceOfManager;

                    ContextHandle c = Alc.CreateContext(Device, (int*)null);
                    if (!manager.ContextList.Contains(c))
                    {
                        manager.ContextList.Add(c);
                    }
                    Context = c;
                    Alc.MakeContextCurrent(Context);
                }
                manager.DeviceGettingDefined = false;
            }
            else
            {
                device = manager.DeviceOfManager;
                context = manager.ContextList.Last();
            }
            return AL.GetError();
        }

        #region Start sound(s)
        /// <summary>Start the player</summary>
        /// <returns>true on successful execution of command</returns>
        /// <param name="filename">File to play</param>
        /// <param name="loop">Repeat after coming to the end of the file</param>
        public bool SStart(string filename, bool loop = false)
        {
            return (leftNSource.SStart(this) && rightNSource.SStart(this));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="soundData">Data to play</param>
        /// <param name="sampleFrequency">Leave 0 for automatic/not changed to before (when playing file)</param>
        /// <param name="amountOfBitsPerSample">Bits per Sample, defined by the soundData generation settings (resp. the WAV-File compilation )</param>
        /// <param name="loop">Repeat after finishing, true by default</param>
        /// <param name="stereo">True for stereo, false for mono</param>
        /// <returns></returns>
        public bool SStart(byte[] soundData, NiobEntity3d entity, int sampleFrequency, int amountOfBitsPerSample, bool loop = false, bool stereo = true)
        {
            this.entity = entity;
            return (leftNSource.SStart(this, soundData, sampleFrequency, amountOfBitsPerSample, loop, stereo) && rightNSource.SStart(this, soundData, sampleFrequency, amountOfBitsPerSample, loop, stereo));
        }

        /// <summary>
        /// Resumes or replays the current song
        /// </summary>
        /// <param name="restartWhenAlreadyPlaying">Set this parameter on true for restarting when called on an already playing source
        /// <br>false by default</br></param>
        /// <returns>true on successful execution of command</returns>
        public bool SStart(bool restartWhenAlreadyPlaying = false)
        {
            return (leftNSource.SStart(this, restartWhenAlreadyPlaying) && rightNSource.SStart(this, restartWhenAlreadyPlaying));
        }
        #endregion

        #region Pause/Stop/Reset Sound(s)
        /// <summary>Pauses the sound(s)</summary>
        /// <returns>true on successful execution of command</returns>
        public bool SPause()
        { return (leftNSource.SPause(this) && rightNSource.SPause(this)); }

        /// <summary>Stops the sound(s)</summary>
        /// <returns>true on successful execution of command</returns>
        public bool SStop()
        { return true; }

        /// <summary>Stops the sound source and sets its state to Initial</summary>
        /// <br>When possible recommended every few minutes</br>
        /// <br>(May be helpful for avoiding sound errors)</br>
        /// <returns>true on successful execution of command</returns>
        public bool SReset()
        { return (leftNSource.SReset(this) && rightNSource.SReset(this)); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="closeContext">Needs to be closed for closing the device (do not close when you still want to play other sounds/music)</param>
        /// <param name="closeDevice">Can just be closed when context is getting closed</param>
        public void SKill(NiobAudiomanager manager, bool closeContext, bool closeDevice, bool deleteBuffer)
        {
            if (closeContext)
            {
                if (context != ContextHandle.Zero)
                {
                    Alc.MakeContextCurrent(ContextHandle.Zero);
                    Alc.DestroyContext(context);
                }
                context = ContextHandle.Zero;
                manager.ContextDefined = false;


                if (closeDevice)
                {
                    if (device != IntPtr.Zero)
                    {
                        Alc.CloseDevice(device);
                    }
                    device = IntPtr.Zero;
                    manager.DeviceDefined = false;
                }
            }
            if (deleteBuffer) AL.DeleteBuffer(buffer);

            leftNSource.SKill();
            rightNSource.SKill();
        }
        #endregion

        #region Volume
        /// <summary>
        /// Set the volume factor
        /// <br>Alternatively set the Volume Property</br>
        /// </summary>
        /// <param name="volume">float value >= 0, if out of bounds -> bound; don't forget logarithmic scala
        ///     <br>1 means normal volume (±0dB)</br>
        ///     <br>0 means muted (-∞dB) (excepted from normal scala)</br>
        ///     <br>2 means +6dB</br>
        ///     <br>0.5 means -6dB</br>
        ///     <br>4 means +12dB</br>
        ///     <br>and so on</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetVolume(float volume)
        { return true; }

        /// <summary>
        /// Set the volume which will not be undercut
        /// </summary>
        /// <param name="minVolume">1>=float value >= 0, when larger than maxvolume => minvolume = maxvolume
        /// <br>if out of bounds -> bound</br></param>
        /// <returns>true on successful execution</returns>
        public bool SSetMinVolume(float minVolume)
        { return true; }

        /// <summary>
        /// Set the volume which will not be exceeded
        /// </summary>
        /// <param name="maxVolume"> 1>= float value >=0
        /// <br>if out of bounds -> bound</br>
        /// <br>Logarithmic</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetMaxVolume(float maxVolume)
        { return true; }
        #endregion

        #region Modify Sound(s)

        #region General
        /// <summary>
        /// Set the pitch factor (it also changess the time needed to play the sound)
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetPitch(float pitch)
        { return true; }
        #endregion

        #region 3D

        #region Listener
        public bool SSetPositionOfListener(SharpDX.Vector3 pos)
        { return true; }

        public bool SSetVelocityOfListener(SharpDX.Vector3 vel)
        { return true; }

        public bool SSetOrientationOfListener(SharpDX.Vector3 ori)
        { return true; }

        #endregion

        #region Source
        public bool SSetPositionOfSource(SharpDX.Vector3 pos)
        { return true; }

        public bool SSetVelocityOfSource(SharpDX.Vector3 vel)
        { return true; }
        #endregion

        #endregion

        #endregion
    }





    internal class NSource
    {
        #region Variables/Properties
        public bool isLeft;
        private int source;

        public int Source { get => source; set => source = value; }
        #endregion

        // NiobSoundSource does not need ref because classes always are reference types
        public NSource(bool isLeft, bool startAfterInit, NiobSoundSource s, ref NiobAudiomanager manager)
        {
            this.isLeft = isLeft;

            _NiobSInit(ref s, ref manager);
            if (startAfterInit) SStart(ref s);
        }

        /// <summary>Initialise</summary>
        /// <param name="amountOfSoundsToPlay">Just give a group of sounds, it's not implemented to use them seperated</param>
        /// <returns></returns>
        private unsafe ALError _NiobSInit(ref NiobSoundSource s, ref NiobAudiomanager manager, int amountOfSoundsToPlay = 1)
        {

            // Generate Sound-Buffer(s) / Process
            Alc.MakeContextCurrent(s.Context);
            AL.GenBuffers(amountOfSoundsToPlay, out int buffer);
            AL.GenSources(1, out int source1);
            s.buffer = buffer;
            source = source1;
            return AL.GetError();
        }

        #region Start sound(s)
        public bool SStart(NiobSoundSource s)
        {
            return SStart_(ref s);
        }
        public bool SStart(ref NiobSoundSource s)
        {
            return SStart_(ref s);
        }
        private bool SStart_(ref NiobSoundSource s)
        {
            try
            {
                byte[] soundData = File.ReadAllBytes(s.fileToPlay);

                return _startSoundSource(ref s, soundData, s.loop);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SStart(NiobSoundSource s, byte[] soundData, int sampleFrequency, int amountOfBitsPerSample, bool loop = false, bool stereo = false)
        {
            try
            {
                s.amountOfBits = amountOfBitsPerSample;
                s.loop = loop;
                s.sampleFrequency = sampleFrequency;
                s.stereo = stereo;
                return _startSoundSource(ref s, soundData, loop, sampleFrequency);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool _startSoundSource(ref NiobSoundSource s, byte[] soundData, bool loop, int sampleFreq = 0)
        {
            try
            {
                if (sampleFreq != 0)
                {
                    s.sampleFrequency = sampleFreq;
                }
                _LoadBuffer(s, soundData);

                if (loop) AL.Source(source, ALSourceb.Looping, true);
                else AL.Source(source, ALSourceb.Looping, false);

                // Klang abspielen
                AL.SourcePlay(source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool _LoadBuffer(NiobSoundSource s, byte[] soundData)
        {
            try
            {
                Alc.MakeContextCurrent(s.Context);
                switch (s.AmountOfBits)
                {
                    case 8:
                        if (s.stereo)
                            AL.BufferData(s.buffer, ALFormat.Mono8, soundData, soundData.Length, 2 * s.sampleFrequency);
                        else
                            AL.BufferData(s.buffer, ALFormat.Mono8, soundData, soundData.Length, s.sampleFrequency);
                        break;
                    case 16:
                        if (s.stereo)
                            AL.BufferData(s.buffer, ALFormat.Mono16, soundData, soundData.Length, 2 * s.sampleFrequency);
                        else
                            AL.BufferData(s.buffer, ALFormat.Mono16, soundData, soundData.Length, s.sampleFrequency);
                        break;
                    default:
                        AL.BufferData(s.buffer, ALFormat.Mono16, soundData, soundData.Length, s.sampleFrequency);
                        break;
                }
                AL.Source(source, ALSourcei.Buffer, s.buffer);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Resumes or replays the current song
        /// </summary>
        /// <param name="restartWhenAlreadyPlaying">Set this parameter on
        /// true for restarting when called on an already playing source
        /// <br>false by default</br></param>
        /// <returns>true on successful execution of command</returns>
        public bool SStart(NiobSoundSource s, bool restartWhenAlreadyPlaying = false)
        {
            try
            {
                Alc.MakeContextCurrent(s.Context);
                if (restartWhenAlreadyPlaying) AL.SourcePlay(Source);
                else if (AL.GetSourceState(Source) != ALSourceState.Playing) AL.SourcePlay(Source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Pause/Stop/Reset Sound(s)
        /// <summary>Pauses the sound(s)</summary>
        /// <returns>true on successful execution of command</returns>
        public bool SPause(NiobSoundSource s)
        {
            try
            {
                Alc.MakeContextCurrent(s.Context);
                AL.SourcePause(Source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>Stops the sound(s)</summary>
        /// <returns>true on successful execution of command</returns>
        public bool SStop(NiobSoundSource s)
        {
            try
            {
                Alc.MakeContextCurrent(s.Context);
                AL.SourceStop(Source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal void SLoop(bool loop)
        {
            AL.Source(source, ALSourceb.Looping, loop);
        }

        /// <summary>Stops the sound source and sets its state to Initial</summary>
        /// <returns>true on successful execution of command</returns>
        public bool SReset(NiobSoundSource s)
        {
            try
            {
                Alc.MakeContextCurrent(s.Context);
                AL.SourceRewind(Source);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void SKill()
        {
            try
            {
                AL.DeleteSource(source);
                source = 0;
            }
            catch (Exception) { }
        }
        #endregion

        #region Volume
        /// <summary>
        /// Set the volume factor
        /// <br>Alternatively set the Volume Property</br>
        /// </summary>
        /// <param name="volume">float value >= 0, if out of bounds -> bound; don't forget logarithmic scala
        ///     <br>1 means normal volume (±0dB)</br>
        ///     <br>0 means muted (-∞dB)</br>
        ///     <br>2 means +6dB</br>
        ///     <br>0.5 means -6dB</br>
        ///     <br>4 means +12dB</br>
        ///     <br>and so on</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetVolume(NiobSoundSource s, float volume)
        {
            try
            {
                if (volume < 0)
                {
                    s.volume = 0;
                    Alc.MakeContextCurrent(s.Context);
                    AL.Source(Source, ALSourcef.Gain, 0);
                }
                else
                {
                    s.volume = volume;
                    Alc.MakeContextCurrent(s.Context);
                    AL.Source(Source, ALSourcef.Gain, s.Volume);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Set the volume which will not be undercut
        /// </summary>
        /// <param name="minVolume">1>=float value >= 0, when larger than maxvolume => minvolume = maxvolume
        /// <br>if out of bounds -> bound</br></param>
        /// <returns>true on successful execution</returns>
        public bool SSetMinVolume(NiobSoundSource s, float minVolume)
        {
            try
            {
                if (minVolume < 0) s.minVolume = 0;
                else if (minVolume > s.MaxVolume) s.minVolume = s.MaxVolume;
                else s.minVolume = minVolume;
                Alc.MakeContextCurrent(s.Context);
                AL.Source(Source, ALSourcef.MinGain, s.MinVolume);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Set the volume which will not be exceeded
        /// </summary>
        /// <param name="maxVolume"> 1>= float value >=0
        /// <br>if out of bounds -> bound</br>
        /// <br>Logarithmic</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetMaxVolume(NiobSoundSource s, float maxVolume)
        {
            try
            {
                if (maxVolume < 0) s.maxVolume = 0;
                else if (maxVolume < s.MinVolume) s.maxVolume = s.MinVolume;
                else if (maxVolume > 1) s.maxVolume = 1;
                else s.maxVolume = maxVolume;
                Alc.MakeContextCurrent(s.Context);
                AL.Source(Source, ALSourcef.MaxGain, s.MaxVolume);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Modify Sound(s)

        #region General
        /// <summary>
        /// Set the pitch factor (it also changess the time needed to play the sound)
        /// </summary>
        /// <param name="pitch"> value >= 0; if out of bounds -> bound
        ///     <br> 1 for normal pitch (no pitch)</br>
        ///     <br> 2 for pitch on 200% (or +100%) (everything double frequency)</br>
        ///     <br> 0.5 for pitch on 50%  (or -50% pitch) (everything half frequency)</br>
        ///     <br> 0.1 for pitch on 10% (or -90% pitch)</br>
        /// </param>
        /// <returns>true on successful execution</returns>
        public bool SSetPitch(NiobSoundSource s, float pitch)
        {
            try
            {
                if (pitch < 0)
                {
                    s.pitch = 0;
                }
                else s.pitch = pitch;
                Alc.MakeContextCurrent(s.Context);
                AL.Source(Source, ALSourcef.Pitch, s.pitch);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region 3D

        //
        /*
        #region Listener
        public bool SSetPositionOfListener(NiobSoundSource s, SharpDX.Vector3 pos)
        {
            try
            {
                positionListener = pos; //positionListener.X = -pos.X;
                Alc.MakeContextCurrent(Context);
                AL.Listener(ALListener3f.Position, positionListener.X, positionListener.Y, positionListener.Z);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SSetVelocityOfListener(NiobSoundSource s, SharpDX.Vector3 vel)
        {
            try
            {
                velocityListener = vel; //velocityListener.X = -vel.X;
                Alc.MakeContextCurrent(Context);
                AL.Listener(ALListener3f.Velocity, velocityListener.X, velocityListener.Y, velocityListener.Z);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SSetOrientationOfListener(NiobSoundSource s, SharpDX.Vector3 ori)
        {
            try
            {
                orientationListener = ori; //orientationListener.X = -ori.X;
                Alc.MakeContextCurrent(Context);
                AL.Source(Source, ALSource3f.Direction, orientationListener.X, orientationListener.Y, orientationListener.Z);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion


        private void SwapLeftAndRight(bool value)
        {
            return 1;
        }

        #region Source
        public bool SSetPositionOfSource(NiobSoundSource s, SharpDX.Vector3 pos, bool sourceRel = false)
        {
            try
            {
                positionSource = pos; //positionSource.X = -pos.X;
                Alc.MakeContextCurrent(Context);
                AL.Source(Source, ALSource3f.Position, positionSource.X, positionSource.Y, positionSource.Z);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SSetVelocityOfSource(NiobSoundSource s, SharpDX.Vector3 vel)
        {
            try
            {
                velocitySource = vel; //velocitySource.X = -vel.X;
                Alc.MakeContextCurrent(Context);
                AL.Source(Source, ALSource3f.Velocity, velocitySource.X, velocitySource.Y, velocitySource.Z);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SSetOrientationOfSource(NiobSoundSource s, SharpDX.Vector3 ori)
        {
            try
            {
                orientationSource = ori; //orientationSource.X = -ori.X;
                Alc.MakeContextCurrent(Context);
                AL.Source(Source, ALSource3f.Direction, orientationSource.X, orientationSource.Y, orientationSource.Z);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        #endregion
        // */

        #endregion

        #endregion
    }
}
