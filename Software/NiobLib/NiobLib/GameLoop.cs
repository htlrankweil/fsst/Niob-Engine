﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;


namespace NiobLib
{
    public class GameLoop
    {
        DispatcherTimer gameloop;

        private long lastTick;
        public long LastTick { get {return lastTick;} }

        public TimeSpan GameLoopTicksSpeed { get { return gameloop.Interval; } set { gameloop.Interval = value; } }

        public GameLoop(EventHandler defaultLoop) {
            gameloop = new DispatcherTimer();
            gameloop.Tick += SetLastTick;
            gameloop.Tick += defaultLoop;
            gameloop.Start();
            GameLoopTicksSpeed = TimeSpan.FromMilliseconds(20);
        }

        private void SetLastTick(object? sender, EventArgs e)
        {
            lastTick = Stopwatch.GetTimestamp();

        }

        public void AddEventHandler(EventHandler extraHandler) { gameloop.Tick += extraHandler; }
        public void RemoveEventHandler(EventHandler extraHandler) { gameloop.Tick -= extraHandler; }
    }

}