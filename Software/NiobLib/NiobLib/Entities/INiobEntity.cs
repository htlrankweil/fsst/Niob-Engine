﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using HelixToolkit;
using HelixToolkit.SharpDX.Core.Model;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.SharpDX.Core.Model.Scene2D;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Model;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct2D1.Effects;

namespace NiobLib.Entities
{
    public interface INiobEntity : IRotatable, IPositionable
    {
        public SceneNode Node { get; }

        public Matrix Transform { get; set; }
        //public Vector3 Position { get; set; }
        //public Quaternion Rotation { get; set; }
        public Vector3 PrevPos { get; }
        //public Vector3 Position { get; set;}
        public Quaternion PrevRot { get;}
        public long TimeLast { get;}
        public void Render(long tickLength, long lastTick);       
    }
}
