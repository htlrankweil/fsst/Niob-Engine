﻿using HelixToolkit.SharpDX.Core.Model.Scene;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HelixToolkit;
using HelixToolkit.SharpDX.Core;

using System.Diagnostics;
using HelixToolkit.Wpf.SharpDX.Utilities;
using System.ComponentModel;

namespace NiobLib.Entities
{
    internal class NiobEntityTextBilly : INiobEntity
    {
        BillboardNode node = new BillboardNode();
        bool isFresh = false;
        public SceneNode Node { get { return node; } }
        public Matrix Transform { get { return node.ModelMatrix; } set { node.ModelMatrix = value; } }


        private Vector3 position;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position
        {
            get { return position; }
            set
            {
                prevPos = position;
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Vector3 prevPos;
        public Vector3 PrevPos
        {
            get { return prevPos; }
        }
        private Quaternion rotation;

        [TypeConverter(typeof(QuaternionConverter))]
        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Quaternion prevRot;
        public Quaternion PrevRot
        {
            get { return prevRot; }
        }


        private long timeLast;
        public long TimeLast { get { return timeLast; } }

        public NiobEntityTextBilly()
        {
            
        }

        public void Render(long tickLength, long lastTick)
        {
            if (timeLast >= lastTick)
            {
                //https://github.com/sharpdx/SharpDX/blob/master/Source/SharpDX.Mathematics/Matrix.cs

                float amount = (Stopwatch.GetTimestamp() - lastTick) / tickLength * 1f;

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling
                node.ModelMatrix = freshMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;
                node.ModelMatrix = freshMatrix;
                isFresh = true;
            }
        }
    }
}
