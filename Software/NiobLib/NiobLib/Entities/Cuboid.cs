﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using SharpDX;
using NAudio.Gui;
using HelixToolkit.Wpf.SharpDX.Utilities;
using System.ComponentModel;
using HelixToolkit.SharpDX.Core.Model;

namespace NiobLib.Entities
{

    public class Cuboid : INiobEntity
    {
        #region Interface Propterties
        bool isFresh = false;
        private SceneNode node;
        public SceneNode Node { get { return node; } }


        private Matrix transform;

        public Matrix Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                isFresh = false;
            }
        }

        private Vector3 position;
        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position 
        {
            get { return position; }
            set 
            {
                prevPos = position;
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            } 
        }
       
        private Vector3 prevPos;
        public Vector3 PrevPos
        {
            get { return prevPos; }
        }
        private Quaternion rotation;
        [TypeConverter(typeof(QuaternionConverter))]
        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Quaternion prevRot;
        public Quaternion PrevRot { get { return prevRot; } } 

        private long timeLast;
        public long TimeLast { get { return timeLast; } }
        #endregion


        public Material Material
        {
            get { return ((MeshNode)node).Material.ConvertToMaterial(); }
            set { ((MeshNode)node).Material = value; }
        }

        private Vector3 size;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Size
        {
            get { return size; }
            set
            {
                size = value;
                isFresh = false;
            }
        }

        [TypeConverter(typeof(Color4Converter))]
        public Color4 Color
        {
            get
            {
                switch (((MeshNode)node).Material.ConvertToMaterial())
                {
                    case PhongMaterial pm:
                        return pm.DiffuseColor;
                    case DiffuseMaterial dm:
                        return dm.DiffuseColor;
                    default:
                        return Color4.Black;
                }
            }
            set
            {
                switch (((MeshNode)node).Material)
                {
                    case PhongMaterialCore pm:
                        pm.DiffuseColor = value;
                        break;
                    case DiffuseMaterialCore dm:
                        dm.DiffuseColor = value;
                        break;
                    default:
                        break;
                }
            }
        }

        public Cuboid() : this(Vector3.Zero, 1,1,1, PhongMaterials.Red) { }
        public Cuboid(Vector3 center, float lengthX, float lengthY, float lengthZ, Material material)
        {   
            Position = center;
            Position = center;
            Rotation = Quaternion.Identity;
            Rotation = Quaternion.Identity;
            Size = new Vector3(lengthX, lengthY, lengthZ);

            transform = Matrix.Identity;


            MeshBuilder builder = new MeshBuilder();
            builder.AddBox(Vector3.Zero, 1, 1, 1);
            MeshNode cuboidModel = new MeshNode();
            cuboidModel.Geometry = builder.ToMesh();

            node = cuboidModel;
            Material = material;
        }


        public void Render(long tickLength, long lastTick)
        {

            if (timeLast >= lastTick)
            {
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength), 1);

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling

                Matrix transMatrix = transform * freshMatrix;
                transMatrix.M11 *= size.X; transMatrix.M12 *= size.X; transMatrix.M13 *= size.X;
                transMatrix.M21 *= size.Y; transMatrix.M22 *= size.Y; transMatrix.M23 *= size.Y;
                transMatrix.M31 *= size.Z; transMatrix.M32 *= size.Z; transMatrix.M33 *= size.Z;
                node.ModelMatrix = transMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;

                Matrix transMatrix = transform * freshMatrix;
                transMatrix.M11 *= size.X; transMatrix.M12 *= size.X; transMatrix.M13 *= size.X;
                transMatrix.M21 *= size.Y; transMatrix.M22 *= size.Y; transMatrix.M23 *= size.Y;
                transMatrix.M31 *= size.Z; transMatrix.M32 *= size.Z; transMatrix.M33 *= size.Z;
                node.ModelMatrix = transMatrix;

                isFresh = true;
            }
        }
    }
}
