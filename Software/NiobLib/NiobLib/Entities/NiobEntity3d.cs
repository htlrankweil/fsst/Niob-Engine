﻿using System.Diagnostics;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Assimp;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using SharpDX;

namespace NiobLib.Entities
{
    public class NiobEntity3d : INiobEntity
    {
        private SceneNode node;
        //private SetCollection<Bone> bones;

        private Matrix transform;

        private long timeLast;
        bool isFresh = false;

        private Vector3 position;
        private Vector3 prevPos;

        private Quaternion rotation;
        private Quaternion prevRot;

        public Matrix Transform
        {
            get { return transform; }
            set { 
                transform = value;
                isFresh = false;
            }
        }



        public SceneNode Node { 
            get { return node; } 
        }


        //public SetCollection<Bone> Bones
        //{
        //    get { return bones; }
        //}

        public Vector3 Position
        {
            get { return position; }
            set 
            {
                prevPos = position;  
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        public Vector3 PrevPos
        {
            get { return prevPos; }
        }


        public Quaternion Rotation
        {
            get { return rotation; }
            set 
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        public Quaternion PrevRot 
        {
            get { return prevRot; }
        }


        public long TimeLast { 
            get { return timeLast; } 
        }

        IList<HelixToolkit.SharpDX.Core.Animations.Animation> animations; //?!


        public NiobEntity3d(string embedFilename, string embedTextureDir)
        {
            HelixToolkitScene scene = SceneImporter.ImportEmbeded(embedFilename, embedTextureDir);
            transform = scene.Root.ModelMatrix;
            node = scene.Root;

            animations = scene.Animations;
            rotation = Quaternion.Identity;
        }

        public NiobEntity3d(string embedFilename, string embedTextureDir, bool ResetPosition)
        {
            HelixToolkitScene scene = SceneImporter.ImportEmbeded(embedFilename, embedTextureDir);
            transform = scene.Root.ModelMatrix;
            node = scene.Root;

            animations = scene.Animations;
            rotation = Quaternion.Identity;

            if (ResetPosition)
            {
                foreach (SceneNode ScnNode in node.Traverse())
                {
                    Matrix m =  ScnNode.ModelMatrix;
                    m.TranslationVector = Vector3.Zero;
                    ScnNode.ModelMatrix = m;
                }
                transform.TranslationVector = Vector3.Zero;
            }
        }

        public NiobEntity3d(SceneNode node)
        {
            transform = node.ModelMatrix;
            this.node = node;

            rotation = Quaternion.Identity;
        }

        public void Render(long tickLength, long lastTick)
        {

            if (timeLast >= lastTick)
            {
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength),1);
                
                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling
                node.ModelMatrix = transform*freshMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;
                node.ModelMatrix = transform*freshMatrix;
                isFresh = true;
            }
        }
    }
}
