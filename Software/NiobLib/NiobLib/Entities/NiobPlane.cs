﻿using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Utilities;
using NiobLib.Math;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct2D1.Effects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace NiobLib.Entities 
{
    public class NiobPlane : INiobEntity
    {
		private AxisPlaneGridNode node = new AxisPlaneGridNode();
        #region Interface Properties
        public SceneNode Node { get { return node; } }

        private Matrix transform;
        public Matrix Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                isFresh = false;
            }
        }

        private Vector3 position;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position
		{
			get { return position; }
			set {
                prevPos = position;
                position = value;
				timeLast = Stopwatch.GetTimestamp();
				isFresh = false; }
		}
		private Vector3 prevPos;
		public Vector3 PrevPos { get { return prevPos; } }
		
		
		private Quaternion rotation;

        [TypeConverter(typeof(QuaternionConverter))]
        public Quaternion Rotation
		{
			get { return rotation; }
			set { 
				prevRot = rotation;
				rotation = value;
				timeLast = Stopwatch.GetTimestamp();
				isFresh = false;
			}
		}

		private long timeLast;
		public long TimeLast { get { return timeLast; } }

		private Quaternion prevRot;
        public Quaternion PrevRot
		{ 
			get; 
		}

        #endregion


        [TypeConverter(typeof(Color4Converter))]
        public Color4 GridCol
		{
			get { return node.GridColor; }
			set { node.GridColor = value; }
		}

        [TypeConverter(typeof(Color4Converter))]
        public Color4 PlaneCol
		{
			get { return node.PlaneColor; }
			set { node.PlaneColor = value; }
		}

        public Axis UpAxis
		{
			get { return node.UpAxis; } set {node.UpAxis = value; }	
		}

		public float Offset
		{
			get { return node.Offset;}
			set { node.Offset = value; }
		}

		private bool isFresh;

		#region Constructors
		public NiobPlane() 
		{
			Rotation = Quaternion.Identity;
			Position = Vector3.Zero;
			prevPos = Vector3.Zero;
			prevRot = Quaternion.Identity;


            transform = Matrix.Identity;

            Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
            freshMatrix.TranslationVector = Position;
            node.ModelMatrix = freshMatrix;

            Offset = 0;
            UpAxis = Axis.Y;
			GridCol = new Color4(0.3f, 0.3f, 0.3f, 1f);
			PlaneCol = new Color4(0.8f, 1f, 1f, 1f);

			node.GridThickness = 1f;

        }

		public NiobPlane(Color4 gridColor, Color4 planeColor, Axis upAxis, float offset) 
		{
			Offset = offset;
			UpAxis = upAxis;
			GridCol = gridColor;
			PlaneCol = planeColor;
            Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
            freshMatrix.TranslationVector = Position;
            node.ModelMatrix = freshMatrix;
        }
        #endregion

		public void Render(long tickLength, long lastTick)
		{
			if (timeLast >= lastTick)
			{
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength), 1);

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
				Vector3 t = Vector3.Lerp(prevPos, Position, amount);
				Matrix freshMatrix = Matrix.RotationQuaternion(q);
				freshMatrix.TranslationVector = t;
			
				switch (UpAxis)
				{
					case Axis.X:
						Offset = t.X;
						break;
					case Axis.Y:
                        Offset = t.Y;
                        break;
					case Axis.Z:
                        Offset = t.Z;
                        break;
					default:
						break;
				}

                // transformmatrix * freshmatrix for scaling
                node.ModelMatrix = transform * freshMatrix;
            }
			else if (! isFresh)
			{
				Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
				freshMatrix.TranslationVector = Position;
                node.ModelMatrix = transform * freshMatrix;

                switch (UpAxis)
                {
                    case Axis.X:
                        Offset = freshMatrix.TranslationVector.X;
                        break;
                    case Axis.Y:
                        Offset = freshMatrix.TranslationVector.Y;
                        break;
                    case Axis.Z:
                        Offset = freshMatrix.TranslationVector.Z;
                        break;
                    default:
                        break;
                }
				
                isFresh = true;
            }
		}
    }
}
