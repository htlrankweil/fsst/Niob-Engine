﻿using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Utilities;
using SharpDX;
using System.ComponentModel;
using System.Diagnostics;

namespace NiobLib.Entities
{
    public class Arrow : INiobEntity
    {
        #region Interface Propterties
        bool isFresh = false;
        private SceneNode node;
        public SceneNode Node { get { return node; } }
        private Matrix transform;

        public Matrix Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                isFresh = false;
            }
        }

        private Vector3 position;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position
        {
            get { return position; }
            set
            {
                prevPos = position;
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }

        private Vector3 prevPos;
        public Vector3 PrevPos { get { return prevPos; } }

        private Quaternion rotation;

        [TypeConverter(typeof(QuaternionConverter))]
        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Quaternion prevRot;
        public Quaternion PrevRot { get { return prevRot; } }

        private long timeLast;
        public long TimeLast { get { return timeLast; } }
        #endregion


        public Material Material
        {
            get { return ((MeshNode)node).Material.ConvertToMaterial(); }
            set { ((MeshNode)node).Material = value; }
        }

        [TypeConverter(typeof(Color4Converter))]
        public Color4 Color
        {
            get
            {
                switch (((MeshNode)node).Material.ConvertToMaterial())
                {
                    case PhongMaterial pm:
                        return pm.DiffuseColor;
                    case DiffuseMaterial dm:
                        return dm.DiffuseColor;
                    default:
                        return Color4.Black;
                }
            }
            set
            {
                switch (((MeshNode)node).Material)
                {
                    case PhongMaterialCore pm:
                        pm.DiffuseColor = value;
                        break;
                    case DiffuseMaterialCore dm:
                        dm.DiffuseColor = value;
                        break;
                    default:
                        break;
                }
            }
        }


        public Arrow(Vector3 modelPoint1, Vector3 modelPoint2, double diameter, double headLength, int divisions, Material material)
        {
            Position = modelPoint1;
            Position = modelPoint1;
            Rotation = Quaternion.Identity;
            Rotation = Quaternion.Identity;

            transform = Matrix.Identity;


            MeshBuilder builder = new MeshBuilder();
            builder.AddArrow(Vector3.Zero, modelPoint2-Position, diameter, headLength, divisions);
            MeshNode arrowModel = new MeshNode();
            arrowModel.Geometry = builder.ToMesh();
            arrowModel.Material = Material;
            node = arrowModel;
        }


        public void Render(long tickLength, long lastTick)
        {

            if (timeLast >= lastTick)
            {
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength), 1);

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling
                node.ModelMatrix = transform * freshMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;
                node.ModelMatrix = transform * freshMatrix;
                isFresh = true;
            }
        }
    }
}
