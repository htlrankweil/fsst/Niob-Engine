﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HelixToolkit;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Model;
using HelixToolkit.SharpDX.Core.Model.Scene;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Utilities;
using NiobLib.Physics;
using SharpDX;

namespace NiobLib.Entities
{
    public class BasicBall : INiobEntity
    {

        private bool isFresh = false;
        #region Interface Properties
        private SceneNode node;
        public SceneNode Node { get { return node; } }
        private Matrix transform;

        public Matrix Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                isFresh = false;
            }
        }


        private Vector3 position;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position
        {
            get { return position; }
            set
            {
                prevPos = position;
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }

        private Vector3 prevPos;
        public Vector3 PrevPos{ get { return prevPos; }}

        private Quaternion rotation;

        [TypeConverter(typeof(QuaternionConverter))]
        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Quaternion prevRot;
        public Quaternion PrevRot { get { return prevRot; } }

        private long timeLast;
        public long TimeLast { get { return timeLast; } }

        public Material Material
        {
            get { return ((MeshNode)node).Material.ConvertToMaterial(); }
            set { ((MeshNode)node).Material = value; }
        }
        #endregion

        private Vector3 size;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Size
        {
            get { return size; }
            set { size = value;
                isFresh = false;
            }
        }

        [TypeConverter(typeof(Color4Converter))]
        public Color4 Color
        {
            get {
                switch (((MeshNode)node).Material.ConvertToMaterial())
                {
                    case PhongMaterial pm:
                        return pm.DiffuseColor;
                    case DiffuseMaterial dm:
                        return dm.DiffuseColor;
                    default:
                        return Color4.Black;
                }
            }
            set
            {
                switch (((MeshNode)node).Material)
                {
                    case PhongMaterialCore pm:
                        pm.DiffuseColor = value;
                        break;
                    case DiffuseMaterialCore dm:
                        dm.DiffuseColor = value;
                        break;
                    default:
                        break;
                }
            }
        }

        public BasicBall() : this(Vector3.Zero, 0.5f, 10, PhongMaterials.Red) { }
        public BasicBall(Vector3 pos, float radius, int divisions, Material material)
        {
            Position = pos;
            Position = pos;
            Rotation = Quaternion.Identity;
            Rotation = Quaternion.Identity;
            Size = new Vector3(radius * 2, radius * 2, radius * 2);

            transform = Matrix.Identity;

            MeshBuilder builder = new MeshBuilder();
            builder.AddSphere(Vector3.Zero, 0.5f, divisions);


            MeshNode meshnode = new MeshNode();
            meshnode.Geometry = builder.ToMeshGeometry3D();
            node = meshnode;
            Material = material;
        }






        public void Render(long tickLength, long lastTick)
        {

            if (timeLast >= lastTick)
            {
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength), 1);

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling

                Matrix transMatrix = transform*freshMatrix;
                transMatrix.M11 *= size.X; transMatrix.M12 *= size.X; transMatrix.M13 *= size.X;
                transMatrix.M21 *= size.Y; transMatrix.M22 *= size.Y; transMatrix.M23 *= size.Y;
                transMatrix.M31 *= size.Z; transMatrix.M32 *= size.Z; transMatrix.M33 *= size.Z;
                node.ModelMatrix = transMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;

                Matrix transMatrix = transform * freshMatrix;
                transMatrix.M11 *= size.X; transMatrix.M12 *= size.X; transMatrix.M13 *= size.X;
                transMatrix.M21 *= size.Y; transMatrix.M22 *= size.Y; transMatrix.M23 *= size.Y;
                transMatrix.M31 *= size.Z; transMatrix.M32 *= size.Z; transMatrix.M33 *= size.Z;
                node.ModelMatrix = transMatrix;

                isFresh = true;
            }
        }

    }

    //public enum Materials { phongBlue = PhongMaterials.Blue;  }
}
