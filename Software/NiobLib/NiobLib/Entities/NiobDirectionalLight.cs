﻿using HelixToolkit.SharpDX.Core.Model.Scene;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using HelixToolkit.Wpf.SharpDX;
using System.Diagnostics;
using System.Windows.Markup;
using System.ComponentModel;
using NiobLib.MISC;
using HelixToolkit.Wpf.SharpDX.Utilities;

namespace NiobLib.Entities
{
    public class NiobDirectionalLight : INiobEntity
    {
        private DirectionalLightNode node;
        bool isFresh = false;
        public SceneNode Node { get { return node; } }
       private Matrix transform;

        public Matrix Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                isFresh = false;
            }
        }

        private Vector3 position;

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Position
        {
            get { return position; }
            set
            {
                prevPos = position;
                position = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Vector3 prevPos;
        public Vector3 PrevPos
        {
            get { return prevPos; }
        }

        [TypeConverter(typeof(QuaternionConverter))]
        private Quaternion rotation;
        public Quaternion Rotation
        {
            get { return rotation; }
            set
            {
                prevRot = rotation;
                rotation = value;
                timeLast = Stopwatch.GetTimestamp();
                isFresh = false;
            }
        }
        private Quaternion prevRot;
        public Quaternion PrevRot
        {
            get { return prevRot; }
        }


        private long timeLast;
        public long TimeLast { get { return timeLast; } }

        [TypeConverter(typeof(Color4Converter))]
        public Color4 Col
        {
            get { return node.Color; }
            set { node.Color = value; }
        }

        [TypeConverter(typeof(Vector3Converter))]
        public Vector3 Direction
        {
            get { return node.Direction; }
            set { node.Direction = value; }
        }

        public NiobDirectionalLight() : this(Color4.White, new Vector3(1, 0, 0)) {}

        public NiobDirectionalLight(Color4 col, Vector3 dir)
        {
            node = new DirectionalLightNode();
            node.Visible = true;
            Direction = dir;
            Col = col;
            transform = Matrix.Identity;
            rotation = Quaternion.Identity;

        }

        public void Render(long tickLength, long lastTick)
        {

            if (timeLast >= lastTick)
            {
                float amount = MathF.Min((Stopwatch.GetTimestamp() - lastTick) / ((float)tickLength), 1);

                Quaternion q = Quaternion.Lerp(prevRot, Rotation, amount);
                Vector3 t = Vector3.Lerp(prevPos, Position, amount);
                Matrix freshMatrix = Matrix.RotationQuaternion(q);
                freshMatrix.TranslationVector = t;
                // transformmatrix * freshmatrix for scaling
                node.ModelMatrix = transform * freshMatrix;
            }
            else if (!isFresh)
            {
                Matrix freshMatrix = Matrix.RotationQuaternion(Rotation);
                freshMatrix.TranslationVector = Position;
                node.ModelMatrix = transform * freshMatrix;
                isFresh = true;
            }
        }
    }
}
