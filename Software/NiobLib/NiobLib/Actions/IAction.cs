﻿using NiobLib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Actions
{
    public interface IAction
    {
        public void Step(float timestep);
    }
}
