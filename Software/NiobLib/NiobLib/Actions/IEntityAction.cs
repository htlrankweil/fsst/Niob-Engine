﻿using NiobLib.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Actions
{
    public interface IEntityAction : IAction
    {
        public INiobEntity Entity {get; set;}
    }
}
