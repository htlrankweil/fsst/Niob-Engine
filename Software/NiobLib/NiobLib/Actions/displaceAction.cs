﻿using NiobLib.Entities;
using NiobLib.Physics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Actions
{
    public class displaceAction : IAction
	{
		INiobEntity entity;

		private Vector3 pos;

		public Vector3 Pos
		{
			get { return pos; }
			set { pos = value; }
		}

        public displaceAction(Vector3 pos, INiobEntity ent, bool isRelative)
        {
            Pos = pos;
            entity = ent;
        }

        public void Step(float timestep)
		{
			
		}
		
	}
}
