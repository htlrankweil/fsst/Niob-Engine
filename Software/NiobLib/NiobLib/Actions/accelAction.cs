﻿using NiobLib.Entities;
using NiobLib.Physics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiobLib.Actions
{
    internal class accelAction : IEntityAction
    {
        private INiobEntity entity;
        public INiobEntity Entity
        {
            get { return entity; }
            set { entity = value; }
        }

        private Vector3 accel;

        public Vector3 Accel
        {
            get { return accel; }
            set { accel = value; }
        }



        float remainingTime = 0;

        public accelAction(INiobEntity NiobEntity, Vector3 accel, float Time)
        {
            entity = NiobEntity;
            Accel = accel;
            remainingTime = Time;
            currentVelocity = Vector3.Zero;
        }

        private Vector3 currentVelocity;
        public Vector3 CurrentVelocity
        {
            get { return currentVelocity; }
        }


        public void Step(float timestep)
        {
            //if (remainingTicks <= 0) return;
            //currentVelocity += accel;
            //entity.Position += currentVelocity;
            //remainingTicks--;
            
        }
    }
}
