﻿using Assimp;
using NiobLib.Entities;
using NiobLib.Input;
using NiobLib.Math;
using NiobLib.Physics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NiobLib.Actions
{
    public class InputMoveAction : IEntityAction
    {
        
        private float velocity;

        public float Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        private INiobEntity entity;

        public INiobEntity Entity
        {
            get { return entity; }
            set { entity = value; }
        }

        private List<IInput> ins;
        public List<IInput> Ins 
        {
            get { return ins; }
        }

        private bool projectVelocity;

        public bool ProjectVelocity
        {
            get { return projectVelocity; }
            set { projectVelocity = value; }
        }




        public InputMoveAction(INiobEntity NiobEntity, float velocity, params IInput[] inputs)
        {
            ins = inputs.ToList();
            entity = NiobEntity;
            projectVelocity = true;
            Velocity = velocity;   
        }

        public void Step(float timestep)
        {
            IInput lastIn = ins.MaxBy(x => x.Timestamp);
            if (lastIn.Intermediate is Vector3 InputVect)
            {
                Vector3 dp = InputVect;
                if (projectVelocity)
                {
                    dp = entity.Rotation.Rotate(dp);
                    dp.Y = 0;
                    dp.Normalize();
                    dp *= InputVect.Length();
                }

                entity.Position += dp * velocity;
            }
        }
        
    }
}
