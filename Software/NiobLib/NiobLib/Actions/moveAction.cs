﻿using Assimp;
using NiobLib.Entities;
using NiobLib.Physics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NiobLib.Actions
{
    internal class moveAction : IEntityAction
    {
        private Vector3 velocity;

        public Vector3 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        private INiobEntity entity;

        public INiobEntity Entity
        {
            get { return entity; }
            set { entity = value; }
        }


        public float remainingTime = 0;

        public moveAction(INiobEntity NiobEntity, Vector3 velocity, float Time)
        {
            Entity = NiobEntity;
            Velocity = velocity;
            remainingTime = Time;

            
        }

        public void Step(float timestep)
        {
            if (remainingTime <= 0) return;
            entity.Position += velocity;
           
        }
    }
}
