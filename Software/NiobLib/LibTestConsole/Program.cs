﻿using NiobLib.Input;
using System.IO.Ports;
using System.Windows;
using NiobLib.Entities;
using SharpDX;
using NiobLib.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK;

namespace LibTestConsole
{
    public class Program
    {
        //
        /* Input-Test

        public static void Main(string[] args)
        {
            string[] tmp = SerialPort.GetPortNames();
            Console.WriteLine(tmp[0] + " | " + tmp[1] + "  - " + tmp.Length);
            MEGACardNewInput("COM8");

            Console.Read();
        }
        // */
        private static void MEGACardNewInput(string com, bool msbFirst = true)
        {
            //MEGACard_Input megaInput = new MEGACard_Input(com, msbFirst);
        }

        private static string Fill18(string v)
        {
            if (v.Length < 18)
            {
                string tmp = "";
                for (int i = v.Length; i < 18; i++)
                {
                    tmp += "0";
                }
                return tmp + v;
            }
            else
            {
                return v;
            }

            //// template
            //public static void Main(string[] args)
            //{
            //    MEGACardNewInput("COM8");
            //
            //    Console.Read();
            //}

            //private static void MEGACardNewInput(string com)
            //{
            //    MEGACard_Input megaInput = new MEGACard_Input(com, true);
            //    megaInput.CompleteDataPackageRcv += OnCompleteDatapackageRcv;
            //}

            //private static void OnCompleteDatapackageRcv(object sender, EventArgs e)
            //{
            //    MEGACard_Input.DataAllRecievedEventArgs eventArgs = e as MEGACard_Input.DataAllRecievedEventArgs;
            //    // Write what to do on data recieved here
            //}
        }
        // */

        // /* Music-Test
        static void Main(string[] args)
        {
            List<string> queue = new List<string>();
            queue.Add(@"C:\Users\Matteo\Downloads\48000Hz_16BpS_stereo_PCM-S16-LE.wav");

            NiobAudiomanager myAudioManager = new NiobAudiomanager(queue);
            //myAudioManager.SoundAdd(myNiobEntity, @"C:\GhettoBlasterSound.wav");
            if (true)// Hier der Code, wann der Sound abgespielt werden soll
            {
                myAudioManager.soundList[0].SStart();
            }

            NiobMusic niobMusic = new NiobMusic(myAudioManager, queue, false, true);
            Console.WriteLine("Playing " + niobMusic.FilePlayingAbsolute);
            Console.WriteLine("Playing " + niobMusic.FilePlayingRelative);

            // NiobMusic player = new NiobMusic(startAfterInit: false);
            // player.NiobMStart(player.NiobMP3ToByteArray(@"C:\Users\Matteo\Downloads\Confetti - Ghost.mp3"));

            Console.WriteLine("Press Enter to start testing...");
            Console.ReadLine();
            niobMusic.NiobMSetVolume((float)0.5);
            Console.ReadLine();
            niobMusic.NiobMReset();
            Console.ReadLine();
            niobMusic.NiobMStart(true);
            Console.ReadLine();
            niobMusic.NiobMSetPitch((float)Math.PI);
            Console.ReadLine();
            niobMusic.NiobMSetVolume((float)2.5);
            Console.ReadLine();
            niobMusic.NiobMSetVolume((float)2);
            Console.ReadLine();
            niobMusic.NiobMSetVolume(1);
            Console.ReadLine();
            niobMusic.NiobMPause();
            Console.ReadLine();
            //niobMusic.NiobMStart();
            //Console.ReadLine();
            niobMusic.NiobMSetMaxVolume((float)0.5);
            Console.ReadLine();
            niobMusic.NiobMSetMinVolume((float)0.5);
            Console.ReadLine();
            //niobMusic.NiobMKill();  // Not necessary, but recommended
            Console.ReadLine();
        }
        // */

        //
        /* Soundcheck
        static void Main(string[] args)
        {
            NiobAudiomanager audiomanager = new NiobAudiomanager();
            //NiobMusic nMusic = new NiobMusic(ref audiomanager);

            //Console.ReadLine();
            NiobSoundSource nSound1 = new NiobSoundSource(ref audiomanager, loop: true);
            //NiobSoundSource nSound2 = new NiobSoundSource(ref audiomanager, loop: true);
            Console.WriteLine("Playing now");
            Console.WriteLine("Press Enter to start testing...");
            //
            /* Testing Listener
            Console.ReadLine();
            nSound.SSetPositionOfListener(new SharpDX.Vector3(10, 0, 0));
            //Console.ReadLine();
            //nSound.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            Console.ReadLine();
            nSound.SSetPositionOfListener(new SharpDX.Vector3((float)0.1, 0, 0));
            //Console.ReadLine();
            //nSound.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            Console.ReadLine();
            nSound.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 10));
            Console.ReadLine();
            nSound.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            Console.ReadLine();
            // */
            //nSound.SSetVelocityOfListener(new SharpDX.Vector3(10000, 0, 0));

            //
            /*
            nSound.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 0, 0));
            nSound.SSetPositionOfSource(new SharpDX.Vector3(1, 0, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(1, 0, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(-1, 0, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(100, 0, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(-100, 0, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 1, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 100, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 100, 0));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 0, 100));
            Console.ReadLine();
            nSound.SSetOrientationOfListener(new SharpDX.Vector3(0, 0, -100));
            Console.ReadLine();
            // */
            /*
            nSound1.SSetPositionOfListener(new SharpDX.Vector3(0, 1, 0));
            Console.ReadLine();
            nSound1.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            Console.ReadLine();
            nSound1.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 1));
            Console.ReadLine();
            nSound1.SSetPositionOfListener(new SharpDX.Vector3(0, 0, 0));
            Console.ReadLine();
            nSound1.SSetVolume((float)0.5);
            //Console.ReadLine();
            //nSound1.SReset();
            Console.ReadLine();
            nSound1.SStart();
            Console.ReadLine();
            nSound1.SSetPitch((float)Math.PI);
            Console.ReadLine();
            nSound1.SSetVolume((float)2.5);
            Console.ReadLine();
            nSound1.SSetVolume(2);
            Console.ReadLine();
            nSound1.SSetVolume(1);
            Console.ReadLine();
            nSound1.SPause();
            Console.ReadLine();
            nSound1.SStart();
            //Console.ReadLine();
            //nSound1.SStart(true);
            Console.ReadLine();
            //nMusic.NiobMStart();
            nSound1.SSetMaxVolume((float)0.8);
            Console.ReadLine();
            nSound1.SSetMinVolume((float)0.5);
            ///
            Console.ReadLine();
            nSound1.SKill(false, false);  // Not necessary, but recommended
            //nSound2.SKill(false, false);  // Not necessary, but recommended
            //nMusic.NiobMKill();
            //Console.ReadLine();
        }
        // */
    }
}


//// Input template
//public static void Main(string[] args)
//{
//    MEGACardNewInput("COM8");
//
//    Console.Read();
//}

//private static void MEGACardNewInput(string com)
//{
//    MEGACard_Input megaInput = new MEGACard_Input(com, true);
//    megaInput.CompleteDataPackageRcv += OnCompleteDatapackageRcv;
//}

//private static void OnCompleteDatapackageRcv(object sender, EventArgs e)
//{
//    MEGACard_Input.DataAllRecievedEventArgs eventArgs = e as MEGACard_Input.DataAllRecievedEventArgs;
//    // Write what to do on data recieved here
//}