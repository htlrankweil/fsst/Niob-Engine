﻿using System.IO.Ports;

internal class SerialDataStream
{
    private static void Main(string[] args)
    {
        SerialPort serialPort = new SerialPort("COM8", 9600, Parity.None, 8, StopBits.One);

        serialPort.Open();

        while (true)
        {
            Console.Write(Fill8(Convert.ToString((int)serialPort.ReadByte(), 2)));
            Console.WriteLine();
        }

        string Fill18(string v)
        {
            if (v.Length < 18)
            {
                string tmp = "";
                for (int i = v.Length; i < 18; i++)
                {
                    tmp += "0";
                }
                return tmp + v;
            }
            else
            {
                return v;
            }
        }

        Console.ReadLine();
    }
}