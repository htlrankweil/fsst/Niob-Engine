﻿using HelixToolkit.Wpf.SharpDX;
using NiobLib.Physics;
using NiobLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NiobLib.Entities;
using SharpDX;
using NiobLib.Actions;
using NiobLib.Input;
using System.Collections.ObjectModel;
using NiobLib.Math;
using NiobInput;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.SharpDX.Core.Assimp;
using HelixToolkit.SharpDX.Core.Model.Scene;
using NiobLib.MISC;
using NiobLib.Audio;

namespace DemoProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DistanceConstraint shoveljoint;
        NiobAudiomanager myAudioManager;
        NiobSoundSource sound;
        public MainWindow()
        {
            InitializeComponent();


            NiobEntity3d BaggerBody = new NiobEntity3d("Bagger.fbx", "",true);
            BaggerBody.Position = new Vector3(0,5+5,0);
            PhysicWrapper PBaggerBody = new PhysicWrapper(BaggerBody, new CuboidBounding(20, 10, 10), 1000, true);

            NiobEntity3d BaggerShovel = new NiobEntity3d("Schaufel.fbx", "",true);
            BaggerShovel.Position = new Vector3(14, 4.25f+5, 0);
            PhysicWrapper PBaggerShovel = new PhysicWrapper(BaggerShovel, new CuboidBounding(10, 10, 15f), 10, true);

            NiobEntity3d BaggerWheel1 = new NiobEntity3d("Wheel.fbx", "",true);
            BaggerWheel1.Position = new Vector3(7f, 0+5, 7f);
            PhysicWrapper PBaggerWheel1 = new PhysicWrapper(BaggerWheel1, new CylinderBounding(4f, 4f, 4f, HelixToolkit.SharpDX.Core.Axis.Z), 500, true);

            NiobEntity3d BaggerWheel2 = new NiobEntity3d("Wheel.fbx", "",true);
            BaggerWheel2.Position = new Vector3(-7f, 0+5, 7f);
            PhysicWrapper PBaggerWheel2 = new PhysicWrapper(BaggerWheel2, new CylinderBounding(4f, 4f, 4f, HelixToolkit.SharpDX.Core.Axis.Z), 500, true);

            NiobEntity3d BaggerWheel3 = new NiobEntity3d("Wheel.fbx", "",true);
            BaggerWheel3.Position = new Vector3(7f, 0+5, -7f);
            PhysicWrapper PBaggerWheel3 = new PhysicWrapper(BaggerWheel3, new CylinderBounding(4f, 4f, 4f, HelixToolkit.SharpDX.Core.Axis.Z), 500, true);

            NiobEntity3d BaggerWheel4 = new NiobEntity3d("Wheel.fbx", "",true);
            BaggerWheel4.Position = new Vector3(-7f, 0+5, -7f);
            PhysicWrapper PBaggerWheel4 = new PhysicWrapper(BaggerWheel4, new CylinderBounding(4f, 4f, 4f, HelixToolkit.SharpDX.Core.Axis.Z), 500, true);
            //PBaggerBody.AngularVelocity = new Vector3(1, 1, 1);
            //PBaggerWheel4.AngularVelocity = new Vector3(.1f, .1f, 1f);
            PBaggerBody.Velocity = new Vector3(10f, 0, 0);

            Nscene.Physics.Constaints.Add(new DistanceConstraint(PBaggerBody, PBaggerShovel, new Vector3(0, 0, 5f), new Vector3(-14f, 0, 5f), 0, 0.000001f));
            Nscene.Physics.Constaints.Add(new DistanceConstraint(PBaggerBody, PBaggerShovel, new Vector3(0, 0, -5f), new Vector3(-14f, 0, -5f), 0, 0.000001f));
            shoveljoint = new DistanceConstraint(PBaggerBody, PBaggerShovel, new Vector3(-7f, 0, 0), new Vector3(-20f, 0, 0), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(shoveljoint);

            DistanceConstraint FrontWheelCLeft1 = new DistanceConstraint(PBaggerBody, PBaggerWheel1, new Vector3(7f, -5, 14), new Vector3(0, 0, 7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(FrontWheelCLeft1);
            DistanceConstraint FrontWheelCLeft2 = new DistanceConstraint(PBaggerBody, PBaggerWheel1, new Vector3(7f, -5, 0), new Vector3(0, 0, -7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(FrontWheelCLeft2);


            DistanceConstraint BackWheelCLeft1 = new DistanceConstraint(PBaggerBody, PBaggerWheel2, new Vector3(-7f, -5, 14), new Vector3(0, 0, 7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(BackWheelCLeft1);
            DistanceConstraint BackWheelCLeft2 = new DistanceConstraint(PBaggerBody, PBaggerWheel2, new Vector3(-7f, -5, 0), new Vector3(0, 0, -7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(BackWheelCLeft2);

            DistanceConstraint FrontWheelCRight1 = new DistanceConstraint(PBaggerBody, PBaggerWheel3, new Vector3(7f, -5, 0), new Vector3(0, 0, 7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(FrontWheelCRight1);
            DistanceConstraint FrontWheelCRight2 = new DistanceConstraint(PBaggerBody, PBaggerWheel3, new Vector3(7f, -5, -14), new Vector3(0, 0, -7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(FrontWheelCRight2);


            DistanceConstraint BackWheelCRight1 = new DistanceConstraint(PBaggerBody, PBaggerWheel4, new Vector3(-7f, -5, 0), new Vector3(0, 0, 7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(BackWheelCRight1);
            DistanceConstraint BackWheelCRight2 = new DistanceConstraint(PBaggerBody, PBaggerWheel4, new Vector3(-7f, -5, -14), new Vector3(0, 0, -7), 0, 0.000001f);
            Nscene.Physics.Constaints.Add(BackWheelCRight2);


            NiobPlane Nplane = new NiobPlane();
            Nplane.Position = new Vector3(1, -10, 0);
            Np.Position = new Vector3(1, -0, 0);
            PhysicWrapper GroundCollider = new PhysicWrapper(Nplane, new CuboidBounding() { Size = new Vector3(200,20,200) }, 50, false) { IsStationary = true};

            Vector3 grav = new Vector3(0, -10, 0);
            PhysicsGroup Ground1 = new PhysicsGroup() { PBaggerBody, GroundCollider };
            Ground1.GlobalAcceleration = grav;
            PhysicsGroup Ground2 = new PhysicsGroup() { PBaggerShovel, GroundCollider };
            Ground2.GlobalAcceleration = grav;
            PhysicsGroup Ground3 = new PhysicsGroup() { PBaggerWheel1, GroundCollider };
            Ground3.GlobalAcceleration = grav;
            PhysicsGroup Ground4 = new PhysicsGroup() { PBaggerWheel2, GroundCollider };
            Ground4.GlobalAcceleration = grav;
            PhysicsGroup Ground5 = new PhysicsGroup() { PBaggerWheel3, GroundCollider };
            Ground5.GlobalAcceleration = grav;
            PhysicsGroup Ground6 = new PhysicsGroup() { PBaggerWheel4, GroundCollider};
            Ground6.GlobalAcceleration = grav;

            Nscene.Physics.PhysicsGroups.Add(Ground1);
            Nscene.Physics.PhysicsGroups.Add(Ground2);
            Nscene.Physics.PhysicsGroups.Add(Ground3);
            Nscene.Physics.PhysicsGroups.Add(Ground4);
            Nscene.Physics.PhysicsGroups.Add(Ground5);
            Nscene.Physics.PhysicsGroups.Add(Ground6);

            Nscene.Entities.Add(BaggerBody);
            Nscene.Entities.Add(BaggerShovel);
            Nscene.Entities.Add(BaggerWheel1);
            Nscene.Entities.Add(BaggerWheel2);
            Nscene.Entities.Add(BaggerWheel3);
            Nscene.Entities.Add(BaggerWheel4);




            WASDKeyboardInput inputWASD = new WASDKeyboardInput(NViewPort, System.Windows.Input.Key.W, System.Windows.Input.Key.S, System.Windows.Input.Key.A, System.Windows.Input.Key.D);

            mEGACard_Input = new MEGACard_Input("COM15");
            WASDMegaInput inputMega = new WASDMegaInput(mEGACard_Input, 0);
            PotentiometerMegaInput inputPoti = new PotentiometerMegaInput(mEGACard_Input, 1);
            NumericKeyboardInput inputUDKey = new NumericKeyboardInput(NViewPort, System.Windows.Input.Key.Q, System.Windows.Input.Key.E, 0.5, 0.5);

            ShovelAction shovelAction = new ShovelAction(shoveljoint, 50 * Math.PI /150);
            

            BackWheelAction driveLeft = new BackWheelAction(PBaggerWheel4, new Vector3(0, 0, -3f));
            BackWheelAction driveRight = new BackWheelAction(PBaggerWheel2, new Vector3(0, 0, -3f));

            TurnWheelAction turnLeft = new TurnWheelAction(FrontWheelCLeft1, FrontWheelCLeft2, new Vector3(7, -5, 7), 30*Math.PI/180);
            TurnWheelAction turnRight = new TurnWheelAction(FrontWheelCRight1, FrontWheelCRight2, new Vector3(7, -5, -7), 30 * Math.PI / 180);

            TurnWheelAction turnLeftB = new TurnWheelAction(BackWheelCLeft1, BackWheelCLeft2, new Vector3(-7, -5, 7), -10*Math.PI/180);
            TurnWheelAction turnRightB = new TurnWheelAction(BackWheelCRight1, BackWheelCRight2, new Vector3(-7, -5, -7), -10 * Math.PI / 180);

            driveLeft.Ins.Add(inputWASD);
            driveRight.Ins.Add(inputWASD);
            turnLeft.Ins.Add(inputWASD);
            turnRight.Ins.Add(inputWASD);
            turnLeftB.Ins.Add(inputWASD);
            turnRightB.Ins.Add(inputWASD);
            
            driveLeft.Ins.Add(inputMega);
            driveRight.Ins.Add(inputMega);
            turnLeft.Ins.Add(inputMega);
            turnRight.Ins.Add(inputMega);
            turnLeftB.Ins.Add(inputMega);
            turnRightB.Ins.Add(inputMega);

            shovelAction.Ins.Add(inputPoti);
            shovelAction.Ins.Add(inputUDKey);


            Nscene.Actions.Add(shovelAction);
            Nscene.Actions.Add(driveLeft);
            Nscene.Actions.Add(driveRight);
            Nscene.Actions.Add(turnLeft);
            Nscene.Actions.Add(turnRight);
            Nscene.Actions.Add(turnLeftB);
            Nscene.Actions.Add(turnRightB);


            BoundingShapeVisualizer boundingShapeVisualizer = new BoundingShapeVisualizer(Nscene);
            this.Closed += MainWindow_Closed;

            // Queue erzeugen und befüllen
            List<string> queue = new List<string>();
            queue.Add(@"C:\Blue-Wednesday-One.wav");
            // Audiomanager erstellen und befüllen mit Queue
            myAudioManager = new NiobAudiomanager(queue);

            // Sound erstellen
            sound = new NiobSoundSource(myAudioManager, BaggerBody, @"C:\Users\Matteo\Schuuuh01.wav");
            myAudioManager.soundList.Add(sound);
        }
        MEGACard_Input mEGACard_Input;

        private void MainWindow_Closed(object? sender, EventArgs e)
        {
            if (mEGACard_Input.IsConnected)
            {
                mEGACard_Input.Disconnect();
            }
        }

        public class TurnWheelAction : IAction
        {
            private DistanceConstraint FrontWheel1;
            private DistanceConstraint FrontWheel2;

            private Vector3 pivotPoint;

            public Vector3 PivotPoint
            {
                get { return pivotPoint; }
                set { pivotPoint = value; }
            }

            private double maxTurnAngle;

            public double MaxTurnAngle
            {
                get { return maxTurnAngle; }
                set { maxTurnAngle = value; }
            }

            private List<IInput> ins;
            public List<IInput> Ins
            {
                get { return ins; }
            }


            public TurnWheelAction(DistanceConstraint frontWheel1, DistanceConstraint frontWheel2, Vector3 pivotPoint, double maxTurnAngle)
            {
                ins = new List<IInput>();
                FrontWheel1 = frontWheel1;
                FrontWheel2 = frontWheel2;
                PivotPoint = pivotPoint;
                MaxTurnAngle = maxTurnAngle;
            }

            public void Step(float timestep)
            {
                IInput? lastIn = ins.MaxBy(x => x.Timestamp);
                if (lastIn is not null && lastIn.Intermediate is Vector3 input)
                {
                    Vector3 currentPos = FrontWheel1.PointA - pivotPoint;
                    double currentAngle = Math.Atan2(currentPos.X, currentPos.Z);
                    double da = currentAngle + input.Z * maxTurnAngle;
                    da = Math.CopySign(Math.Min(Math.Abs(da), Math.Abs(maxTurnAngle*2*timestep)), da);
                    double newAngle = currentAngle - da;

                    Vector3 dp = 7 * new Vector3((float)Math.Sin(newAngle), 0, (float)Math.Cos(newAngle));

                    FrontWheel1.PointA = pivotPoint + dp;
                    FrontWheel2.PointA = pivotPoint - dp;
                }
            }
        }
        
        public class BackWheelAction : IAction
        {
            private IPhysicsObject Wheel;

            private Vector3 velocity;

            public Vector3 Velocity
            {
                get { return velocity; }
                set { velocity = value; }
            }


            private List<IInput> ins;
            public List<IInput> Ins
            {
                get { return ins; }
            }

            public BackWheelAction(IPhysicsObject wheel, Vector3 velocity)
            {
                ins = new List<IInput>();
                Velocity = velocity;
                Wheel = wheel;
            }

            public void Step(float timestep)
            {
                IInput? lastIn = ins.MaxBy(x => x.Timestamp);
                if (lastIn is not null && lastIn.Intermediate is Vector3 input) {

                    Vector3 wheelVelGlobal = Wheel.AngularVelocity;
                    Vector3 wheelVelLocal = Wheel.Orientation.InvRotate(wheelVelGlobal);

                    Vector3 maxaccel = velocity * timestep;

                    Vector3 dv = wheelVelLocal - velocity * input.X;
                    Vector3 acellby = new Vector3(
                        MathF.Min(MathF.Abs(maxaccel.X), MathF.Abs(dv.X)),
                        MathF.Min(MathF.Abs(maxaccel.Y), MathF.Abs(dv.Y)),
                        MathF.Min(MathF.Abs(maxaccel.Z), MathF.Abs(dv.Z)));

                    Vector3 newVelLocal = wheelVelLocal - new Vector3(
                            MathF.CopySign(acellby.X, dv.X),
                            MathF.CopySign(acellby.Y, dv.Y),
                            MathF.CopySign(acellby.Z, dv.Z)
                        );

                    Wheel.AngularVelocity = Wheel.Orientation.Rotate(newVelLocal);
                }


            }
        }

        public class ShovelAction : IAction
        {
            private DistanceConstraint ShovelConstraint;

            private Vector3 pivotPoint;

            public Vector3 PivotPoint
            {
                get { return pivotPoint; }
                set { pivotPoint = value; }
            }

            private double angle;

            public double Angle
            {
                get { return angle; }
                set { angle = value; }
            }

            private List<IInput> ins;
            public List<IInput> Ins
            {
                get { return ins; }
            }


            public ShovelAction(DistanceConstraint shovelConstraint, double angle)
            {
                ins = new List<IInput>();
                ShovelConstraint = shovelConstraint;
                PivotPoint = pivotPoint;
                Angle = angle;
            }

            public void Step(float timestep)
            {
                IInput? lastIn = ins.MaxBy(x => x.Timestamp);
                if (lastIn is not null && lastIn.Intermediate is double input)
                {
                    double prevangle = Math.Atan2(-ShovelConstraint.PointA.Y, -ShovelConstraint.PointA.X);
                    double angle = (Angle * (input - 0.5)*2);
                    double da = prevangle - angle;
                    da = Math.CopySign(Math.Min(Math.Abs(da), Math.Abs(Angle * 2 * timestep)), da);
                    angle = prevangle - da;

                    ShovelConstraint.PointA = new Vector3(-7 * MathF.Cos((float)angle), -7 * MathF.Sin((float)angle), 0);

                    myAudioManager.Add();
                }
            }
        }




        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            float maxangle = 30;
            float angle = (float)(maxangle * (sender as Slider).Value * Math.PI / 180);
            shoveljoint.PointA = new Vector3(-7 * MathF.Cos(angle), -7 * MathF.Sin(angle), 0);
        }
    }
}
