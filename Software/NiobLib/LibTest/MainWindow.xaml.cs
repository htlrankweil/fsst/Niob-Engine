﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using NiobLib.Entities;
using SharpDX;
using Media = System.Windows.Media;
using NiobLib.Physics;
using HelixToolkit.SharpDX.Core;
using HelixToolkit.Wpf.SharpDX;
using NiobLib.Actions;
using NiobLib.Input;
using System.Collections.ObjectModel;
using NiobLib.MISC;
using HelixToolkit.SharpDX.Core.Model.Scene;
using NiobInput;

namespace LibTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Func<int> Funcion;
        public MainWindow()
        {

            InitializeComponent();
            //Sus();


            testScene.Entities.Add(new NiobDirectionalLight(Color4.White, new Vector3(0, -1, -1)));

            NiobPlane plane = new NiobPlane(Color4.Black, Color4.White, Axis.Y,0);
            testScene.Entities.Add(plane);

            testScene.Physics.Substeps = 100;
            testScene.GameLoop.GameLoopTicksSpeed = TimeSpan.FromMilliseconds(20);
            PhysicsGroup g = new PhysicsGroup() { GlobalAcceleration = new Vector3(0, 0, 0) };
            testScene.Physics.PhysicsGroups.Add(g);

            PhongMaterial mat= PhongMaterials.Red;
           // mat.EnableFlatShading = true;
            INiobEntity ballerWieMoneyBoy = /*new NiobEntity3d("Wheel.fbx", "", true);*/new BasicBall(new Vector3(1, 1, 1), 1, 5, mat);
            //((MeshNode)ballerWieMoneyBoy.Node).RenderWireframe = true;
            //((MeshNode)ballerWieMoneyBoy.Node).WireframeColor = Color.Green;

            testScene.Entities.Add(ballerWieMoneyBoy);

            WASDKeyboardInput ballInput = new WASDKeyboardInput(TestPort, System.Windows.Input.Key.W, System.Windows.Input.Key.S, System.Windows.Input.Key.A, System.Windows.Input.Key.D);
            
            InputMoveAction ballerMoved = new InputMoveAction(ballerWieMoneyBoy, 1f, ballInput);

            testScene.Actions.Add(ballerMoved);

            g.Add(new PhysicWrapper(ballerWieMoneyBoy, new ConvexMeshBounding("Wheel.fbx"), 1, false));
            //g.Add(new PhysicWrapper(ballerWieMoneyBoy, new CylinderBounding() { Size=new Vector3(5,10,5), Axis = Axis.Z}, 1, false));

            BoundingShapeVisualizer boundingShapeVisualizer = new BoundingShapeVisualizer(testScene);

            //Entity erstellen
            NiobEntity3d caracter = new NiobEntity3d("Dr_Bre.fbx", "");
            testScene.Entities.Add(caracter);

            //keyboard Inputs
            WASDKeyboardInput inputKeyboard = new WASDKeyboardInput(TestPort, System.Windows.Input.Key.W, System.Windows.Input.Key.S, System.Windows.Input.Key.A, System.Windows.Input.Key.D);

            //Megacard Inputs
            MEGACard_Input mInput = new MEGACard_Input("COM15");
            WASDMegaInput inputMega = new WASDMegaInput(mInput, 0, MEGAbuttons.s0, MEGAbuttons.s1, MEGAbuttons.s2, MEGAbuttons.s3);

            //Action Hinzufügen
            InputMoveAction moveInput = new InputMoveAction(caracter, 1f, inputKeyboard, inputMega);
            testScene.Actions.Add(moveInput);

            //List<IPhysicsObject> pchains = new List<IPhysicsObject>();

            //NiobEntity3d chain0 = new NiobEntity3d("chain.fbx", "");
            //testScene.Entities.Add(chain0);

            //PhysicWrapper pchain0 = new PhysicWrapper(chain0, new CuboidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false) { IsStationary = true };
            //g.Add(pchain0);
            //pchains.Add(pchain0);

            //int amtExtraChainLinks = 30;
            //float chainXOffset = 5;

            //for (int i = 0; i < amtExtraChainLinks; i++)
            //{
            //    float chainY = -18 * (i + 1);
            //    float chainX = chainXOffset * (i + 1);

            //    NiobEntity3d chain = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(chainX, chainY, 0) };
            //    PhysicWrapper pchain = new PhysicWrapper(chain, new CuboidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            //    pchains.Add(pchain);
            //    testScene.Physics.Constaints.Add(new DistanceConstraint(pchains[i], pchains[i+1], new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.002f));
            //    testScene.Physics.Constaints.Add(new DistanceConstraint(pchains[i], pchains[i+1], new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.002f));

            //    g.Add(pchain);
            //    testScene.Entities.Add(chain);
            //}


            /*
            NiobEntity3d chain0 = new NiobEntity3d("chain.fbx", "");
            NiobEntity3d chain1 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(0, -18, 0) };
            NiobEntity3d chain2 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(5, -36, 5) };
            NiobEntity3d chain3 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(10, -54, 10) };
            NiobEntity3d chain4 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(15, -72, 10) };
            NiobEntity3d chain5 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(20, -90, 10) };
            NiobEntity3d chain6 = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(20, -108, 15) };
            */

            /*
            PhysicWrapper pchain1 = new PhysicWrapper(chain1, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            PhysicWrapper pchain2 = new PhysicWrapper(chain2, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            PhysicWrapper pchain3 = new PhysicWrapper(chain3, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            PhysicWrapper pchain4 = new PhysicWrapper(chain4, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            PhysicWrapper pchain5 = new PhysicWrapper(chain5, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            PhysicWrapper pchain6 = new PhysicWrapper(chain6, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
            */
            /*
            testScene.Physics.Constaints.Add(new FixedPointDistanceConstraint(pchain1, new Vector3(0, 9, 4), new Vector3(4, -9, 0), 0, 0.0001f));
            testScene.Physics.Constaints.Add(new FixedPointDistanceConstraint(pchain1, new Vector3(0, 9, -4), new Vector3(-4, -9, 0), 0, 0.0001f));

            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain1, pchain2, new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain1, pchain2, new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));

            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain2, pchain3, new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain2, pchain3, new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));

            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain3, pchain4, new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain3, pchain4, new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));

            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain4, pchain5, new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain4, pchain5, new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));

            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain5, pchain6, new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
            testScene.Physics.Constaints.Add(new DistanceConstraint(pchain5, pchain6, new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));


            g.Add(pchain1);
            g.Add(pchain2);
            g.Add(pchain3);
            g.Add(pchain4);
            g.Add(pchain5);
            g.Add(pchain6);

            testScene.Physics.PhysicsGroups.Add(g);

            testScene.Entities.Add(chain0);
            testScene.Entities.Add(chain1);
            testScene.Entities.Add(chain2);
            testScene.Entities.Add(chain3);
            testScene.Entities.Add(chain4);
            testScene.Entities.Add(chain5);
            testScene.Entities.Add(chain6);


             NiobScene scene = new NiobScene();
            scene.Entities.Add(new NiobPlane());
            scene.Entities.Add(
                    new NiobEntity3d("chain.fbx", "") { 
                        Position = new Vector3(0, 250, 0)
                    });

            PhysicsGroup group = new PhysicsGroup();
            group.GlobalAcceleration = new Vector3(0, -300, 0);
            scene.Physics.PhysicsGroups.Add(group);
            var constraints = scene.Physics.Constaints;

            IPhysicsObject preChain = null;
            for (int i = 0; i < 8; i++)
            {
                var chain = new NiobEntity3d("chain.fbx", "");
                chain.Position = new Vector3(0, 232 - 18 * i, 0);

                PhysicWrapper pchain = new PhysicWrapper(
                    chain, new CubeoidBounding(0.2f, 3, 2), 1, false);
                
                group.Add(pchain);
                scene.Entities.Add(chain);
                if (i==0)
                {
                    constraints.Add(new FixedPointDistanceConstraint(
                        pchain, new Vector3(0, 9, 4), 
                        new Vector3(4, 241, 0), 0, 0.0001f));

                    constraints.Add(new FixedPointDistanceConstraint(
                        pchain, new Vector3(0, 9, -4),
                        new Vector3(-4, 241, 0), 0, 0.0001f));
                }
                else
                {
                    constraints.Add(new DistanceConstraint(
                        preChain, pchain, new Vector3(0, -9, 4), 
                        new Vector3(4, 9, 0), 0, 0.001f));

                    constraints.Add(new DistanceConstraint(
                        preChain, pchain, new Vector3(0, -9, -4), 
                        new Vector3(-4, 9, 0), 0, 0.001f));
                }

                preChain = pchain;
            }

            TestPort.Scene = scene;
            */







            /*
           // NiobDirectionalLight lighterus = new NiobDirectionalLight(Color4.White);
            //NiobEntityTextBilly billfried = new NiobEntityTextBilly() { };
            NiobPlane testplane = new NiobPlane(Color4.White, Color4.Black,Axis.Z,0);
            //TestPort.Camera = testCam.HelixCamera;
            //(SceneImporter.ImportEmbeded("Dr_Bre.fbx", "", 0.1f, true).Root);
            //testScene.Entities.Add(lighterus);


            NiobEntity3d bre = new NiobEntity3d("Dr_Bre.fbx", "");
            NiobEntity3d bre2 = new NiobEntity3d("Dr_Bre.fbx", "");
            NiobEntity3d bre3 = new NiobEntity3d("Dr_Bre.fbx", "");
            NiobEntity3d Wheel = new NiobEntity3d("Wheel.fbx", "", true) { Position = new Vector3(10, 40, 10) };
            testScene.Entities.Add(bre);
            //testScene.Entities.Add(bre2);
            //testScene.Entities.Add(bre3);
            testScene.Entities.Add(Wheel);


          
            //TestPort.Items.Add(new AmbientLight3D() { Color = Colors.White });

            PhysicWrapper physicsBre = new PhysicWrapper(bre, new CubeoidBounding() { Size = new Vector3(3, 40, 10) }, 1, true);
            PhysicWrapper physicsBre2 = new PhysicWrapper(bre2, new CubeoidBounding() { Size = new Vector3(2, 40, 10) }, 1, false);
            PhysicWrapper physicsBre3 = new PhysicWrapper(bre3, new CubeoidBounding() { Size = new Vector3(2, 40, 10) }, 1, false);

            physicsBre.AngularVelocity = new Vector3(0.01f, 0.01f, 10);
            //physicsBre.AngularVelocity = new Vector3(1f, 1f, 1f);
            bre.Transform = bre.Transform * SharpDX.Matrix.RotationY(MathF.PI / 3);
            PhysicsGroup physicgroup = new PhysicsGroup();
            physicgroup.GlobalAcceleration = new Vector3(0, -100, 0);
            bre.Position += new Vector3(-10, 30, -10);
            FixedPointDistanceConstraint c = new FixedPointDistanceConstraint(physicsBre, new Vector3(0, 20, 0), new Vector3(10, 40, 10), 0, 0.001f);
            DistanceConstraint c2 = new DistanceConstraint(physicsBre, physicsBre2, new Vector3(0, -20, 0), new Vector3(0, -20, 0), 0, 0.0005f);
            DistanceConstraint c6 = new DistanceConstraint(physicsBre2, physicsBre3, new Vector3(0, 20, 0), new Vector3(0, 20, 0), 0, 0.0005f);
            DistanceConstraint c3 = new DistanceConstraint(physicsBre, physicsBre2, new Vector3(-1, -20, 0), new Vector3(0, -20, -1), 0, 0.0005f);
            DistanceConstraint c4 = new DistanceConstraint(physicsBre, physicsBre2, new Vector3(0, 0, 10), new Vector3(10, 0, 0), 0, 0.0005f);
            physicgroup.Add(physicsBre);
            //physicgroup.Add(physicsBre2);
            //physicgroup.Add(physicsBre3);
            physicgroup.Add(new PhysicWrapper(new NiobPlane() { Position = new Vector3(0,0,0)}, new CubeoidBounding() { Size = new Vector3(100, 2, 100) }, 1000, false) { IsStationary=true});
            testScene.Physics.Substeps = 10;
            
            //testScene.Physics.TimeStep /= 10;

            //FixedPointDistanceConstraint c2 = new FixedPointDistanceConstraint(physicobject, new Vector3(0, -20, 0), new Vector3(-10, 40, -10), 0, 0.1f);
            //FixedPointDistanceConstraint c3 = new FixedPointDistanceConstraint(physicobject, new Vector3(0, 0, 20), new Vector3(0, 60, 0), 0, 0.1f);

            Funcion = () =>
            {
                testScene.Physics.PhysicsGroups.Add(physicgroup);
                //testScene.Physics.Constaints.Add(c);
                //testScene.Physics.Constaints.Add(c2);
                //testScene.Physics.Constaints.Add(c6);
                //testScene.Physics.Constaints.Add(c3);
                //testScene.Physics.Constaints.Add(c4);
                return 1;
            };

            //testScene.Physics.Constaints.Add(c2);
            //testScene.Physics.Constaints.Add(c3);


            // HelixToolkit.Wpf.SharpDX.AxisPlaneGridModel3D helixplanetest = new HelixToolkit.Wpf.SharpDX.AxisPlaneGridModel3D();
            // WALTERWOLFGANG.Items.Add(testplane.helixElement);

           // MEGACardNewInput("COM8");

            */
        }

        // MegaInputTranslator inps = new MegaInputTranslator("COM8");

        //public delegate Vector3 Derivative2(float time, Vector3 value, Vector3 deriv1);
        //public void SolveEulerOde2(ref float time,ref Vector3 value,ref Vector3 deriv1, float timeStep, Derivative2 deriv2)
        //{
        //    value += deriv1 * timeStep;
        //    deriv1 += deriv2(time, value, deriv1) * timeStep;
        //    time += timeStep;
        //}

        //public void SolveRungeOde2(ref float time, ref Vector3 value, ref Vector3 deriv1, float timeStep, Derivative2 deriv2)
        //{
        //    float halfTime = time + 0.5f * timeStep;
        //    Vector3 k1 = deriv1;
        //    Vector3 l1 = deriv2(time, value, k1);

        //    Vector3 k2 = deriv1 + 0.5f * timeStep * l1;
        //    Vector3 l2 = deriv2(halfTime, value + timeStep * k1 / 2, k2);

        //    Vector3 k3 = deriv1 + 0.5f * timeStep * l2;
        //    Vector3 l3 = deriv2(halfTime, value + timeStep * k2 / 2, k3);

        //    time += timeStep;
        //    Vector3 k4 = deriv1 + timeStep * l3;
        //    Vector3 l4 = deriv2(time, value + timeStep * k3, k4);

        //    value += timeStep / 6 * (k1 + 2 * k2 + 2 * k3 + k4);
        //    deriv1 += timeStep / 6 * (l1 + 2 * l2 + 2 * l3 + l4);
        //}


        //void Sus()
        //{
        //    float dt = 0.1f;
        //    Vector3 p1 = new Vector3(0, 0, 0);
        //    float lspring = 4,kspring = 1,mass = 2;
        //    Vector3 Accel(float time, Vector3 position, Vector3 velocity)
        //    {
        //        Vector3 p1p = position - p1;
        //        return p1p.Normalized() * (p1p.Length() - lspring) * kspring / mass;
        //    }

        //    //Simulate 10 seconds with euler
        //    Vector3 p = new Vector3(6, 0, 0), v = Vector3.Zero;
        //    List<Vector3> simValuesE = new List<Vector3>() { p};
        //    for (float t = 0; t < 10; )
        //    {
        //        SolveEulerOde2(ref t, ref p, ref v, dt, Accel);
        //        simValuesE.Add(p);
        //    }


        //    //Simulate 10 seconds with RungeKutta
        //    p = new Vector3(6, 0, 0); v = Vector3.Zero;
        //    List<Vector3> simValuesR = new List<Vector3>() { p };
        //    for (float t = 0; t < 10;)
        //    {
        //        SolveRungeOde2(ref t, ref p, ref v, dt, Accel);
        //        simValuesR.Add(p);
        //    }

        //    string se = string.Join("\n", simValuesE.Select(x => x.X));
        //    string sr = string.Join("\n", simValuesR.Select(x => x.X));

        //}









        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Funcion();

        }
    }
}
