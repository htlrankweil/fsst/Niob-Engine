~~~plantuml

class Scene {
	- int runtime
	==
	+ Inputs
	+ Triggers
	+ Physics groups
	+ Objects
	==
	Events
	--
	- Gameloop
	- Viewport

}

class Object {
	- string name/designation
	- point location
	- point locationOrigin
	- quoternion rotation
	- point speed
	==
	+ point Location
	+ quoternion Rotation
	+ point Speed
	==
}

class PhysicsGroup{
	- string name/designation
	- List<Object>
	==
}



~~~


Möglicherweise:
Die Objekte in 2-Teile Splitten (1 Teil für physik Relevantes, 1 Tiel alles Restliche)

