~~~ plantuml
@startuml
project start 2022-10-03

[Architecture] starts 2022-10-03
[Architecture] ends 2022-10-10

[CollisionPhysics PW] ends 2022-11-14
[ForcesPhysics JH] ends 2022-11-14
[InverseKinematics PW] ends 2022-11-14
[Rigging JK] ends 2022-11-04
[Fertigstellung Physics] ends 2022-12-12
[SerialInput] ends 2022-12-12
[PrototypeEngine] ends 2023-01-01
[Shader] ends 2022-12-12


[CollisionPhysics PW] starts at [Architecture]'s end
[ForcesPhysics JH] starts at [Architecture]'s end
[InverseKinematics PW] starts at [Architecture]'s end
[Rigging JK] starts at [Architecture]'s end
[Fertigstellung Physics] starts at [Rigging JK]'s end
[Fertigstellung Physics] starts at [ForcesPhysics JH]'s end
[Shader] starts at [Rigging JK]'s end
[Fertigstellung Physics] starts at [CollisionPhysics PW]'s end
[SerialInput] starts at [Architecture]'s end
[PrototypeEngine] starts at [SerialInput]'s end
[PrototypeEngine] starts at [Fertigstellung Physics]'s end
[PrototypeEngine] starts at [Shader]'s end

[PrototypeDemoGame] ends 2023-02-01
[EngineFinal] ends 2023-03-03
[GameFinal] ends 2023-03-03


[PrototypeDemoGame] starts 2023-01-01
[EngineFinal] starts 2023-01-01
[GameFinal] starts at [PrototypeDemoGame]'s end

[FinishDocumentation] starts at [GameFinal]'s end
[FinishDocumentation] starts at [EngineFinal]'s end
[FinishDocumentation] ends 2023-03-17
~~~

~~~plantuml
Project starts 2023-01-01

[PrototypeDemoGame] ends 2023-02-01
[EngineFinal] ends 2023-03-03
[GameFinal] ends 2023-03-03


[PrototypeDemoGame] starts 2023-01-01
[EngineFinal] starts 2023-01-01
[GameFinal] starts at [PrototypeDemoGame]'s end

[FinishDocumentation] starts at [GameFinal]'s end
[FinishDocumentation] starts at [EngineFinal]'s end
[FinishDocumentation] ends 2023-03-17
~~~
