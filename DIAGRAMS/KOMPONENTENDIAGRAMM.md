

~~~plantuml
@startuml

[Scene]
[Gameloop]

[Scene] --> [Triggers]


component PhysicsGroup {
	[Grp 1]
	[Grp 2]
	portin p2
}


component Objekte {
	[Obj 1]
	[Obj 2]
	[Obj 3]
	[Obj 4]
	portin p1
}

[Scene] <-- p1
[Scene] --> [Gameloop]

[Triggers] --> [Obj 1]
[Triggers] --> [Obj 3]

[Grp 1] --> [Obj 1]
[Grp 1] --> [Obj 2]
[Grp 2] --> [Obj 3]
[Grp 2] --> [Obj 4]



[Scene] <-- [Viewport]
[Viewport] --> [Kamera]

@enduml
~~~


