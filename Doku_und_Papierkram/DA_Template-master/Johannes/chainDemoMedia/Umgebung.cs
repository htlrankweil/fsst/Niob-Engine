            NiobPlane plane = new NiobPlane(Color4.Black, Color4.White, Axis.Y,0);
            testScene.Entities.Add(plane);

            testScene.Physics.Substeps = 100;
            testScene.GameLoop.GameLoopTicksSpeed = TimeSpan.FromMilliseconds(20);
            PhysicsGroup g = new PhysicsGroup() { GlobalAcceleration = new Vector3(0, -300, 0) };
            testScene.Physics.PhysicsGroups.Add(g);