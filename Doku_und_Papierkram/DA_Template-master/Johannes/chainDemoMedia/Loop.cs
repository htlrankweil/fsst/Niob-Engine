            int amtExtraChainLinks = 30;
            float chainXOffset = 5;

            for (int i = 0; i < amtExtraChainLinks; i++)
            {
                float chainY = -18 * (i + 1);
                float chainX = chainXOffset * (i + 1);

                NiobEntity3d chain = new NiobEntity3d("chain.fbx", "") { Position = new Vector3(chainX, chainY, 0) };
                PhysicWrapper pchain = new PhysicWrapper(chain, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false);
                pchains.Add(pchain);
                testScene.Physics.Constaints.Add(new DistanceConstraint(pchains[i], pchains[i+1], new Vector3(0, -9, 4), new Vector3(4, 9, 0), 0, 0.001f));
                testScene.Physics.Constaints.Add(new DistanceConstraint(pchains[i], pchains[i+1], new Vector3(0, -9, -4), new Vector3(-4, 9, 0), 0, 0.001f));
                
                g.Add(pchain);
                testScene.Entities.Add(chain);
            }