            List<IPhysicsObject> pchains = new List<IPhysicsObject>();

            NiobEntity3d chain0 = new NiobEntity3d("chain.fbx", "");
            testScene.Entities.Add(chain0);

            PhysicWrapper pchain0 = new PhysicWrapper(chain0, new CubeoidBounding() { Size = new Vector3(0.2f, 3, 2) }, 1, false) { IsStationary = true };
            g.Add(pchain0);
            pchains.Add(pchain0);