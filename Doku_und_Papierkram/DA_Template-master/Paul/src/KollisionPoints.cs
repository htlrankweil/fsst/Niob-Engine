Tuple<int, int, int, Vector3, float> Tri = EPAFaces[minTri];
Vector3 T1 = EPAPoints[Tri.Item1].Item1 - Distance;
Vector3 T2 = EPAPoints[Tri.Item2].Item1 - Distance;
Vector3 T3 = EPAPoints[Tri.Item3].Item1 - Distance;

NiobMath.Barycentric(minNormal * minDist, T1, T2, T3, out float u, out float v, out float w);


Vector3 objAT1 = EPAPoints[Tri.Item1].Item2;
Vector3 objAT2 = EPAPoints[Tri.Item2].Item2;
Vector3 objAT3 = EPAPoints[Tri.Item3].Item2;

Vector3 r1 = objAT1 * u + objAT2 * v + objAT3 * w;

r2 = r1 - (minNormal * minDist – Distance);
