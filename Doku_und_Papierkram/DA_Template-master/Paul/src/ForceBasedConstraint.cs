// Beschleunigung mit Constraint-Kraft berechnen
Vector3 SolveAccel(float time, Vector3 position, Vector3 velocity)
{
   Vector3 a = GetAccel(time, position, velocity);
   Vector3 P2p = position - P;
   return a-P2p * (Vector3.Dot(P2p, a) + velocity.LengthSquared()) / P2p.LengthSquared();
}

//Simulate 10 seconds with Force Based System
Vector3 p = new Vector3(4, 0, 0), v = Vector3.Zero;
List<Vector3> simValuesForce = new List<Vector3>() { p };
for (float t = 0; t < 10;)
{
   SolveRungeOde2(ref t, ref p, ref v, dt, SolveAccel);
   simValuesForce.Add(p);
}
