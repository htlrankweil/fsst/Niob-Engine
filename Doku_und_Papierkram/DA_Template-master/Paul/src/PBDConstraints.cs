//Position mit PBD Berechnen
Vector3 SolvePosition(float time, Vector3 position)
{
   Vector3 P2p = position - P;
   return position - P2p * (1 - distance / P2p.Length());
}

//Simulate 10 seconds with PBD
p = new Vector3(4, 0, 0); v = Vector3.Zero;
List<Vector3> simValuesPBD = new List<Vector3>() { p };
for (float t = 0; t < 10;)
{
   for (int i = 0; i < substeps; i++)
   {
      Vector3 pold = p;
      SolveEulerOde2(ref t, ref p, ref v, dt/substeps, GetAccel);
      p = SolvePosition(t, p);
      v = (p - pold) / (dt / substeps);
   }
   simValuesPBD.Add(p);
}
