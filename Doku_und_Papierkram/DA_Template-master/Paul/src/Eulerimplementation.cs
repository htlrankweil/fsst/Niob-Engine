public delegate Vector3 Derivative2(float time, Vector3 value, Vector3 deriv1);
public void SolveEulerOde2(ref float time,ref Vector3 value,ref Vector3 deriv1, float timeStep, Derivative2 deriv2)
{
   value += deriv1 * timeStep;
   deriv1 += deriv2(time, value, deriv1) * timeStep;
   time += timeStep;
}
