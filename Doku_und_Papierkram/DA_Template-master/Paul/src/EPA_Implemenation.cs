minDist = float.PositiveInfinity;
Vector3 minNormal = EPAFaces[minTri].Item4;

for (int iter = 0; (minDist == float.PositiveInfinity)&& iter < 200; iter ++)
{
    minNormal = EPAFaces[minTri].Item4;
    minDist = EPAFaces[minTri].Item5;

    objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(minNormal)));
    Vector3 support = Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(-minNormal)));
    float supDist = Vector3.Dot(support, minNormal);

    if (MathF.Abs(supDist - minDist) > 0.01f)
    {
        minDist = float.PositiveInfinity;
        //expand Polytope 
        ...
    }
}
