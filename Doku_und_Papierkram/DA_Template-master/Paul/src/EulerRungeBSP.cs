float dt = 1f;
Vector3 p1 = new Vector3(0, 0, 0);
float lspring = 4, kspring = 1, mass = 2;
Vector3 Accel(float time, Vector3 position, Vector3 velocity)
{
   Vector3 p1p = position - p1;
   return -p1p.Normalized() * (p1p.Length() - lspring) * kspring / mass;
}

//Simulate 10 seconds with euler
Vector3 p = new Vector3(6, 0, 0), v = Vector3.Zero;
List<Vector3> simValuesE = new List<Vector3>() { p };
for (float t = 0; t < 10;)
{
   SolveEulerOde2(ref t, ref p, ref v, dt, Accel);
   simValuesE.Add(p);
}


//Simulate 10 seconds with RungeKutta
p = new Vector3(6, 0, 0); v = Vector3.Zero;
List<Vector3> simValuesR = new List<Vector3>() { p };
for (float t = 0; t < 10;)
{
   SolveRungeOde2(ref t, ref p, ref v, dt, Accel);
   simValuesR.Add(p);
}
