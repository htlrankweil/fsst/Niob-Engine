public Vector3 GetSupport(Vector3 Direction)
{
    float min = float.MaxValue;
    Vector3 minV = Vector3.Zero;
    foreach (Vector3 vertex in meshVertex)
    {
        float d = Vector3.Dot(Direction, vertex);
        if (d < min)
        {
            min = d;
            minV = vertex;
        }
    }
    return minV;
}
