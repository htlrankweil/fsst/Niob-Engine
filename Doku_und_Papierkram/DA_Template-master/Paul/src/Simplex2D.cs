private bool Add2Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
{
    c = v;
    oc = ov;
    Vector3 ac = a - c;
    Vector3 bc = b - c;
    Vector3 abc = Vector3.Cross(ac, bc);

    //Origin lies in B
    if (Vector3.Dot(Vector3.Cross(abc, ac), c) > 0)
    {
        a = c;
        oa = oc;
        dir = -(a - b).GetNormal(b);
        return false;
    }

    //Origin lies in C
    if (Vector3.Dot(Vector3.Cross(bc, abc), c) > 0)
    {
        b = c;
        ob = oc;
        dir = -(a - b).GetNormal(b);
        return false;
    }

    if (Vector3.Dot(abc, a) <= 0)
        dir = abc;
    else
        dir = -abc;

    l = 3;
    return false;
}
