Vector3 Distance = objA.PhysicsPosition - objB.PhysicsPosition;
Simplex simplex = new Simplex();
Vector3 dir = Vector3.Zero;

//first search in the direction of origin
Vector3 objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(-Distance)));
simplex.Add(Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(Distance))),objASup, ref dir);

for (int i = 0; i < 100; i++)
{
   objASup = RotA.Rotate(bndA.GetSupport(RotA.InvRot(dir)));
   Vector3 v = Distance + objASup - RotB.Rotate(bndB.GetSupport(RotB.InvRot(-dir)));

   if (Vector3.Dot(v, dir) < 0.01f) return;

   if (simplex.Add(v, objASup, ref dir))
   {
      goto Collision;
   }
}
return;    //no collision found within the maximum iteration count

Collision: //collsion found
