public void SolveStep()
{
   //Alle Physikobjekte und Kollisionspaare in Liste geben
   AccumulatePhysicObjects();

   //XPBD
   float substepTime = timeStep / substeps;
   for (int i = 0; i < substeps; i++)
   {
      foreach (IPhysicsObject obj in physicsObjects)
      {
         if (obj.IsStationary) continue;
         //Positionen Integrieren
         //...
      }

      foreach (Tuple<IPhysicsObject, IPhysicsObject> collisionPair in collisionPairs)
      {
         SolveCollision(collisionPair, substepTime);
      }

      foreach (IPhysicConstaint constaint in constaints)
      {
         constaint.SolveConstraint(substepTime);
      }

      foreach (IPhysicsObject obj in physicsObjects)
      {
         //Geschwindigkeiten erneuern
         //...
      }

      foreach (IPhysicConstaint constaint in constaints)
      {
         constaint.SolveVelocity(substepTime);
      }
   }

   foreach (IPhysicsObject obj in physicsObjects)
   {
      obj.UptadePhysicValues();
   }
}
