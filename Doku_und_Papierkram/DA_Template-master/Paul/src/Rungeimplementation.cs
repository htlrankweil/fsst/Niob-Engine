public void SolveRungeOde2(ref float time, ref Vector3 value, ref Vector3 deriv1, float timeStep, Derivative2 deriv2)
{
   float halfTime = time + 0.5f * timeStep;
   Vector3 k1 = deriv1;
   Vector3 l1 = deriv2(time, value, k1);

   Vector3 k2 = deriv1 + 0.5f * timeStep * l1;
   Vector3 l2 = deriv2(halfTime, value + timeStep * k1 / 2, k2);

   Vector3 k3 = deriv1 + 0.5f * timeStep * l2;
   Vector3 l3 = deriv2(halfTime, value + timeStep * k2 / 2, k3);

   time += timeStep;
   Vector3 k4 = deriv1 + timeStep * l3;
   Vector3 l4 = deriv2(time, value + timeStep * k3, k4);

   value += timeStep / 6 * (k1 + 2 * k2 + 2 * k3 + k4);
   deriv1 += timeStep / 6 * (l1 + 2 * l2 + 2 * l3 + l4);
}
