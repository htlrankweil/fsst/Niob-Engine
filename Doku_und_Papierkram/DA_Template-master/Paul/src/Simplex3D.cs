private bool Add3Dimension(Vector3 v, Vector3 ov, ref Vector3 dir)
{
    d = v;
    od = ov;
    Vector3 ad = d - a;
    Vector3 bd = d - b;
    Vector3 cd = d - c;

    Vector3 dab = Vector3.Cross(ad, bd);
    Vector3 dac = Vector3.Cross(ad, cd);
    Vector3 dbc = Vector3.Cross(bd, cd);

    bool IDAB = Vector3.Dot(dab, cd) < 0;
    bool IDAC = Vector3.Dot(dac, bd) < 0;
    bool IDBC = Vector3.Dot(dbc, ad) < 0;

    bool IsDAB = Vector3.Dot(dab, d) < 0 ^ IDAB;
    bool IsDAC = Vector3.Dot(dac, d) < 0 ^ IDAC;
    bool IsDBC = Vector3.Dot(dbc, d) < 0 ^ IDBC;

    if (IsDAB) //origin is over Triangle DAB
    {
        if (IDAB)
            dir = -dab;
        else
            dir = dab;

        c = d;
        oc = od;
        return false;
    }

    if (IsDAC) //origin is over Triangle DAC
    {
        if (IDAC)
            dir = -dac;
        else
            dir = dac;

        b = d;
        ob = od;
        return false;
    }

    if (IsDBC) //origin is over Triangle DBC
    {
        if (IDBC)
            dir = -dbc;
        else
            dir = dbc;

        a = d;
        oa = od;
        return false;
    }

    l = 4;
    return true;
}
