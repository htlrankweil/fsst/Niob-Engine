public struct Simplex
{
    public Vector3 a, oa;
    public Vector3 b, ob;
    public Vector3 c, oc;
    public Vector3 d, od;
    public int l;

    public bool Add(Vector3 v, Vector3 ov, ref Vector3 dir)
    {
        switch (l)
        {
            case 0:
                return Add0Dimension(v, ov, ref dir);
            case 1:
                return Add1Dimension(v, ov, ref dir);
            case 2:
                return Add2Dimension(v, ov, ref dir);
            case 3:
                return Add3Dimension(v, ov, ref dir);
            default:
                break;
        }
        return true;
    }
}
