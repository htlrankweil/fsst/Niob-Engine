\section{Physik Engines}

Das Ziel von einer Physik Engine ist es physikalische Abl�ufe zu simulieren. Daf�r m�ssen Systeme zuerst mathematisch beschrieben und dargestellt werden, um dann mit verschiedenen L�sungsalgorithmen eine L�sung f�r das System zu finden. Physik Engins in Videospielen k�nnen in allen Gr��en vorkommen.

\subsection{Arten von Systemen}
Nicht jede Physik Engine kann alle physikalischen Abl�ufe simulieren. Je nach Anwendung sind Physik Engines f�r andere System ausgelegt. Oft ist es auch sehr komplex, verschiedene Systeme miteinander zu verbinden. So kann es auch vorkommen, dass ein System das andere beeinflussen kann, aber nicht umgekehrt ist das \dq One Way Coupling\dq. Wenn sich die beiden Systeme gegenseitig beeinflussen k�nnen, nennt man das \dq Two Way Coupling\dq. In Videospielen kommen meistens nur visuell interessante Systeme vor, die auch in Realtime simuliert werden k�nnen.

\subsubsection{Particle-Dynamics}
Ein Partikel kann als Punkt im Raum beschrieben werden, der eine Masse, Position und Translationsgeschwindigkeit besitzt. Bei einer Partikel Simulation werden die Bewegungen der Partikel simuliert. Zus�tzlich k�nnen Kr�fte auf die Partikel einwirken sowie auch zwischen den Partikeln auftreten, die auch in der Simulation ber�cksichtigt werden m�ssen. Die Partikel k�nnen je nach Anwendung einzeln oder als Menge betrachtet werden. \\
In einer typischen Partikel Engine, wie sie in Videospielen Verwendung findet, werden zum Beispiel Partikel verwendet um visuelle Effekte wie Rauch oder Feuer darzustellen. Dabei werden die einzelnen Partikel als animierte oder auch statische Bilder (\dq Sprites\dq) im Spiel angezeigt. Siehe Abbildung \ref{fig:particleengine}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/ParticleEngine}
	\caption{Partikelsystem in Videospielen}
	\label{fig:particleengine}
\end{figure}

Partikel k�nnen auch als einzelne Teilchen von gr��eren Systemen wie zum Beispiel Fl�ssigkeiten betrachtet werden. Dabei zeihen oder sto�en sich die Teichen gegenseitig ab, um in der Menge das Verhalten von Fl�ssigkeiten nachzuahmen.

\subsubsection{Spring Dynamics}

Mit Spring Dynamics werden physikalische Systeme durch ein Masse Feder System dargestellt und simuliert. Dabei wird ein K�rper als Menge von Punkten, die mit Federn verbunden sind, dargestellt. Diese Methode wird vor allem f�r das Simulieren von Seilen oder Stoffen verwendet. Vergleiche Abbildung \ref{fig:springdynamicsrope}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/SpringDynamics_Rope}
	\caption{Simulation eines Seils}
	\label{fig:springdynamicsrope}.
\end{figure}

Im Gegensatz zu einem Reinen Partikel Systems kann bei einem Masse Feder System auch die Verbindung zwischen den Teilchen beachtet werden. So wird zum Beispiel bei der Simulation von Stoffen darauf geachtet, dass die einzelnen Teilchen, mit dem dieser simuliert wird, sich nicht durch den Stoff bewegen k�nnen. Daf�r wird nicht nur auf die Kollisionen von Teilchen mit Teilchen geachtet, sondern auch auf Kollisionen zwischen Teilchen und Verbindungen. Siehe Abbildung \ref{fig:clothsimulationselfcollision}.


\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/ClothSimulation_SelfCollision}
	\caption{Selbstkollision bei einer Cloth Simulation}
	\label{fig:clothsimulationselfcollision}
\end{figure}


\subsubsection{Rigid-Body Dynamics}

Ein Rigid-Body ist ein K�rper, der nicht verformt werden kann. Zum Beschreiben eines K�rpers braucht es im Vergleich zu einem Partikel zus�tzlich eine Orientierung, Winkelgeschwindigkeit und Tr�gheitsmoment. Siehe Abbildung \ref{fig:unterschiedpartikelkoerper}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Unterschied_Partikel_Koerper}
	\caption{Vergleich Partikel und K�rper}
	\label{fig:unterschiedpartikelkoerper}
\end{figure}

Bei Rigid-Body Dynamics werden die Bewegungen und Interaktionen zwischen dieser Starren K�rper berechnet. In der Simulation werden die Rigid-Bodys zum Gro�teil als Partikel mit Orientierung im Masseschwerpunkt betrachtet, die Form der K�rper kommt bei der Simulation nur bei der Kollisionserkennung zum Einsatz. Je nach der Komplexit�t der K�rper werden anspruchsvolle Kollisionserkennungsalgorithmen ben�tigt, um den Kollisionspunkt und Normalvektor bei einer Kollision zu berechnen.

\subsubsection{Soft-Body Dynamics}

Im Gegensatz zu einem Rigid-Body kann sich ein Soft-Body Krafteinwirkung verformen. Ein Soft-Body kann auf verschiedene Wege in der Simulation modelliert werden. \\
Eine M�glichkeit w�re den Soft-Body als Masse Feder System darzustellen. Vergleiche Abbildung \ref{fig:softbodyspringsystem}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/SoftBody_SpringSystem}
	\caption{Soft-Body Masse Feder System}
	\label{fig:softbodyspringsystem}
\end{figure}

F�r diese Methode m�ssen die Federn oft sehr steif gemacht werden, dies hat dann zufolge, dass das System schwierig zum Simulieren ist, ohne dass es instabile wird. Ebenfalls hat die Masse Feder zur Folge, dass das Volumen des K�rpers nicht immer erhalten bleibt.\\
Deshalb k�nnte auch statt federn zwischen den Massepunkten, versucht werden das Volumen zwischen den Massepunkten konstant zu halten. Je nach der Abweichung werden die Punkte dann weiter auseinander oder zusammen gezogen. \\
Als Voraussetzung f�r solche Modellierungsans�tze m�ssen die K�rper zuvor in m�glichst gleichm��ig verteilte punkte aufgeteilt werden.

\subsubsection{Fluid Dynamics}

Fluide sind entweder Fl�ssigkeiten oder Gase und k�nnen auf mehrere Arten simuliert werden. \\
Die Arten von Fluid Simulationen k�nnen in zwei gro�e Klassen aufgeteilt werden. Eine M�glichkeit ist es, das Flussfeld mit der Methode von Lagrange als Teilchensystem zu simulieren. Dabei werden Fluidteilchen einzeln betrachtet und die Summe der Bewegungen ergeben die Bewegung des Fluides. Vergleiche Abbildung \ref{fig:lagrangianfluid}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/LagrangianFluid}
	\caption{Teilchenmodell eines Fluiden}
	\label{fig:lagrangianfluid}
\end{figure}

Ebenso ist es m�glich Fluide ohne Partikel mit der Eulerschen Methode zu simulieren. Daf�r wird die Simulationsdom�ne in ein Gitter aufgeteilt und jeder Zelle wird ein Vektor zugewiesen, mit dem der Fluss zwischen den Zellen beschrieben werden kann, zus�tzlich herrscht in jeder Zelle auch ein gewisser Druck. Der n�chste Zustand des Gitters kann man mit dem momentanen Zustand ermitteln und so kann dann ein Fluid simuliert werden. Vergleiche Abbildung \ref{fig:eulerianfluid}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/EulerianFluid}
	\caption{Ein Fluid als Gitter}
	\label{fig:eulerianfluid}
\end{figure}

Die Niob Physik Engine sollte Rigid-Body Systeme l�sen k�nnen und auch die M�glichkeit bieten die K�rper mechanisch mit Federn und Gelenken verbinden zu k�nnen.


\subsection{L�sen von Differentialgleichungen}
Da die physikalischen Systeme mit Differentialgleichungen beschrieben werden m�ssen diese f�r die Simulation gel�st werden. Differentialgleichungen k�nnen Analytische oder auch Numerische L�sungsverfahren, jedoch kommen in Programmen meist nur Numerische verfahren zum Einsatz mit denen die L�sung der Differenzialgleichung beliebig genau approximiert werden k�nnen. \\
Daf�r werden meistens die Differenzialgleichungen in Differenzengleichungen umgewandelt, die dann berechnet werden k�nnen. Bei dieser Umwandlung werden die Gleichungen temporal diskretisiert, dadurch k�nnen Ungenauigkeiten sowie instabile Systeme entstehen. Der L�sung der Differenzialgleichung kann mit setzen eines kleineren Zeitabstands zwischen den Berechnungsschritten beliebig nahegekommen werden.\\
Um die Simulation m�glichst performant zu halten, werden im Normalfall die Zeitabst�nde m�glichst gro� gehalten, das ist aber nur mit raffinierten Numerischen L�sungsverfahren m�glich, weil diese auch bei gr��eren abst�nden die Abweichungen geringhalten k�nnen.

\subsubsection{Eulersche Integration}

Die wohl einfachste M�glichkeit eine Differentialgleichung zu simulieren, bietet die Eulersche Methode erster Ordnung. Bei diesem Verfahren wird angenommen, dass die Ableitung w�hrend eines Zeitabstands konstant bleibt.
Eine Simple Anwendung einer Differentialgleichung in Physik ist die Bewegung eines fallenden K�rpers. Dabei entspricht die Ableitung der Position der momentanen Geschwindigkeit und die Ableitung der Geschwindigkeit der Beschleunigung. Im Fall eines fallenden K�rpers ist die Beschleunigung Konstant und entspricht der Erdbeschleunigung.
\[\frac{d}{dt}\left(\vec{p}\left(t\right)\right)=\vec{v}\left(t\right)\]
\[\frac{d}{dt}\left(\vec{v}\left(t\right)\right)=\vec{a}\left(t\right)\]
$\vec{p\left(t\right)}$ ... Positionsvektor\\
$\vec{v\left(t\right)}$ ... Geschwindigkeitsvektor\\
$\vec{a\left(t\right)}$ ... Beschleunigungsvektor\\

Die Ableitung einer Funktion entspricht der Tangenten Steigung der Funktion und kann durch die Sekanten Steigung beliebig nahe approximiert werden. Ist der Abstand zwischen den beiden Punkten null entspricht die Sekanten Steigung der Tangentensteigung.

\[\frac{d}{dt}\left(f(t)\right)=\lim\limits_{\Delta t\to0}{\left(\frac{f\left(t+\Delta t\right)-f\left(t\right)}{\Delta t}\right)}\]

F�r die Eulersche Integration wird f�r $\Delta t$ ein Wert gr��er 0 angenommen. Wird dies beim obigen Beispiel Angewendet Entstehen folgende Gleichungen. In diesem Schritt entsteht auch die Abweichung zur exakten L�sung der Differentialgleichung. Mit der Wahl von $\Delta t$ kann der Fehler gew�hlt werden.

\[\frac{\vec{p}\left(t+\Delta t\right)-\vec{p}\left(t\right)}{\Delta t}=\vec{v\left(t\right)}\]
\[\frac{\vec{v}\left(t+\Delta t\right)-\vec{v}\left(t\right)}{\Delta t}=\vec{a}\left(t\right)\]

Durch Umformen dieser Gleichungen erh�lt man folgende Rekursionsgleichungen. Die Position nach einem Zeitschritt $\Delta t$ entspricht der jetzigen Position plus der Geschwindigkeit mal dem Zeitschritt $\Delta t$.

\[\vec{p}\left(t+\Delta t\right)=\vec{p}\left(t\right)+\vec{v}\left(t\right)\ast\Delta t\]
\[\vec{v}\left(t+\Delta t\right)=\vec{v}\left(t\right)+\vec{a}\left(t\right)\ast\Delta t\]

Nach dieser Umformung ist es Trivial diese Gleichungen In Code umzusetzen. Siehe \ref{code:Eulerimp}.

\begin{longlisting}
	\csharpcode{Paul/src/Eulerimplementation.cs}
	\caption{Implementation der Euler-Methode}
	\label{code:Eulerimp}
\end{longlisting}

Das Eulersche L�sungsverfahren entspricht der Integration mit Rechtecken, dabei wird versucht die Fl�che unter der Kurve mit Rechtecken zu approximieren. Werden unendlich viele Rechtecke verwendet ist die N�herung �quivalent zur Fl�che. Vergleiche Abbildung \ref{fig:integrationrechteck}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Integration_Rechteck}
	\caption{Approximation mit Rechtecken}
	\label{fig:integrationrechteck}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Abweichung_Euler}
	\caption{Abweichung zwischen der exakten L�sung und Approximation}
	\label{fig:abweichungeuler}
\end{figure}

Bei diesem Beispiel wurden die obigen Differentialgleichungen mit Zeitabst�nden von 0.5 Sekunden simuliert. Die exakte L�sung f�r dieses System ist einfach zu berechnen. Bei Gr��eren Systemen ist es oft sehr kompliziert oder auch �berhaupt nicht m�glich die exakte L�sung des Systems zu erhalten


\subsubsection{Runge-Kutta-Verfahren}

Ein weiteres Approximationsverfahren, das in Physik Engines Anwendung findet, ist das Runge-Kutta-Verfahren. Dabei wird mit der Hilfe von Hilfs Steigungen zwischen dem Zeitpunkt $t_1$ und $t_2=t_1+\Delta t$ eine Sch�tzung f�r die Mittlere Steigung zwischen $t_1$ und $t_2$ erhalten mit der dann eine Genauere L�sung ermittelt werden kann. Da die Steigung einer Funktion $y_i=f\left(t_i\right)$ auch von dem Funktionswert $y_i$ abh�ngig sein kann muss f�r die Berechnung der Hilfssteigung auch der Funktionswert an diesem Zeitpunkt gesch�tzt werden. Beim Klassischen Runge-Kutta-Verfahren werden vier von diesen Hilfs Steigungen ben�tigt.

\[k_1=f^\prime\left(t_i,y_i\right)\]
\[k_2=f\prime(t_i+\frac{\Delta t}{2},y_i+\frac{\Delta t}{2}k_1)\]
\[k_3=f\prime(t_i+\frac{\Delta t}{2},y_i+\frac{\Delta t}{2}k_2)\]
\[k_4=f\prime(t_i+\Delta t,y_i+\Delta tk_3)\]
$\Delta t$ ... Zeitabstand der Approximation\\
$k_1$ ... die momentane Steigung der Funktion\\
$k_2$ ... die Steigung der Funktion bei der halben Zeit zwischen $t_i$ und $t_{i+1}$, unter der Annahme die durchschnittliche Steigung zwischen $t_i$ und $t_i+\frac{\Delta t}{2}$ entspricht Steigung $k_1$.\\
$k_3$ ... die Steigung der Funktion bei der halben Zeit zwischen $t_i$ und $t_{i+1}$, unter der Annahme die durchschnittliche Steigung zwischen $t_i$ und $t_i+\frac{\Delta t}{2}$ entspricht Steigung $k_2$.\\
$k_3$ ... die Steigung der Funktion bei $t_{i+1}$, unter der Annahme die durchschnittliche Steigung zwischen $t_i$ und $t_i+\frac{\Delta t}{2}$ entspricht Steigung $k_3$.\\

Mit den Hilfssteigungen kann dann die mittlere Steigung zwischen $t_i$ und $t_{i+1}$ approximiert werden.

\[\Phi=\frac{1}{6}\left(k_1+2k_2+2k_3+k_4\right)\]

Danach kann der Funktionswert an der n�chsten Stelle wie bei dem Euler-Verfahren berechnet werden.

\[f\left(t_{i+1}\right)=f\left(t_i\right)+\Delta t\ \Phi=f\left(t\right)+\Delta t\frac{1}{6}\left(k_1+2k_2+2k_3+k_4\right)\]

F�r welchen Zeitpunkt und mit welchem Mittleren Steigung die Hilfssteigungen berechnet werden und welchen Einfluss die einzelnen Hilfssteigungen auf das Ergebnis haben, kann in einem Butcher-Tableau gezeigt werden.

\begin{figure}[H]
	\centering
	\includesvg{DA_Template-master/Paul/IMG/Butcher-Tableau_Quelle_Wikipedia_RungeKutta.svg}
	\caption{Butcher-Tableau des klassischen Runge-Kutta-Verfahrens \footfullcite{wikiRungeKutta}}
	\label{fig:wikipedakopiert}
\end{figure}

Wie zu sehen ist beschreibt die Zeilenbeschriftung zu welchem Zeitpunkt die Hilfssteigung berechnet wird, dabei entspricht die Zahl wie viele Zeitschritte $\Delta t$ zum jetzigen Zeitpunkt $t_i$ addiert werden. In der Tabelle wird festgelegt welcher Funktionswert verwendet wird. zum Beispiel wird in der dritten Zeile festgelegt, dass $k_3$ zum Zeitpunkt $t_i+\frac{\Delta t}{2}$ und bei dem Funktionswert $y_i+\frac{\Delta t}{2}k_2$ berechnet wird. In der Spaltenbeschriftung werden die Koeffizienten angef�hrt, die f�r die Gewichtung der einzelnen Hilfssteigungen n�tig sind. $k_3$ wird so zu einem Drittel im Ergebnis verwendet\\

F�r Differentialgleichungen h�herer Ordnungen wird bei der Runge-Kutta-Methode die Hilfsterme f�r alle Ableitungen Nacheinander gel�st. Nach dem man den ersten Hilfsterm f�r alle Ableitungen hat kann mit diesen die zweiten Hilfsterme bestimmt werden. Dabei muss nur die Funktion f�r die h�chste Ableitung aufgerufen werden, die Weiteren Hilfs Werte ergeben sich dann.\\
F�r eine Differentialgleichung zweiter Ordnung funktioniert das Runge-Kutta-Verfahren wie folgt:


\begin{table}[H]
	\begin{tabular}[H]{p{0.1\textwidth}p{0.4\textwidth}p{0.5\textwidth}}
	&$y_i=f\left(t_i\right)$&${y\prime}_i=f^\prime\left(t_i\right)$\\\\
	&$k_1={y\prime}_i$&$l_1=f^{\prime\prime}\left(t_i,y_i,{y\prime}_i\right)$\\
	&$k_2={y\prime}_i+\frac{\Delta t}{2}l_1$&$l_2=f^{\prime\prime}\left(t_i+,y_i+\frac{\Delta t}{2}k_1,{y\prime}_i+\frac{\Delta t}{2}l_1\right)$\\
	&$k_3={y\prime}_i+\frac{\Delta t}{2}l_2$&$l_3=f^{\prime\prime}\left(t_i+,y_i+\frac{\Delta t}{2}k_2,{y\prime}_i+\frac{\Delta t}{2}l_2\right)$\\
	&$k_4={y\prime}_i+\frac{\Delta t}{2}l_3$&$l_4=f^{\prime\prime}\left(t_i+,y_i+\Delta t\ k_3,{y\prime}_i+\Delta t\ l_3\right)$\\
	\end{tabular}
\end{table}

\[f\left(t_{i+1}\right)=f\left(t_i\right)+\Delta t\frac{1}{6}\left(k_1+2k_2+2k_3+k_4\right)\]
\[f^\prime\left(t_{i+1}\right)=f^\prime\left(t_i\right)+\Delta t\frac{1}{6}\left(l_1+2l_2+2l_3+l_4\right)\]
$l_1$ ... erste Hilfsbeugung\\
$l_2$ ... zweite Hilfsbeugung\\
$l_3$ ... dritte Hilfsbeugung\\
$l_4$ ... vierte Hilfsbeugung\\
Das Verfahren kann wie im Codeabschnitt \ref{code:rungeimp} implementiert werden.


\begin{longlisting}
	\csharpcode{Paul/src/Rungeimplementation.cs}
	\caption{Implementation der Runge-Kutta-Methode}
	\label{code:rungeimp}
\end{longlisting}

\subsubsection{Beispiel unterschied Runge-Kutta und Euler}

Um den Unterschied zwischen dem Runge-Kutta-Verfahren und Euler darzustellen wurde ein System mit den zwei Verfahren gel�st und verglichen. Das gew�hlte System verbindet eine Masse mit einer Feder mit einem fixierten Punkt. Siehe Abbildung \ref{fig:massonspring}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Mass_On_Spring}
	\caption{System mit Masse und Feder}
	\label{fig:massonspring}
\end{figure}

Dabei wird die Masser von der Feder angezogen oder abgesto�en, wenn der Abstand zwischen Masse und Punkt gr��er oder kleiner ist als die L�nge der Feder in Ruhelage. Die Kraft der Feder ist proportional zur Auslenkung.

\[F=k\ast\Delta l\]

In drei Dimensionen ergeben sich diese Differentialgleichungen f�r das System.
\[\frac{d}{dt}\left(\vec{p}\left(t\right)\right)=\vec{v}\left(t\right)\]
\[\frac{d}{dt}\left(\vec{v}\left(t\right)\right)=\vec{a}\left(\vec{p}\left(t\right)\right)\]
\[\vec{a}\left(\vec{p}\right)=\frac{1}{m}k\frac{\left(\vec{p}-\vec{p_1}\right)}{\left|\vec{p}-\vec{p_1}\right|}(\left|\vec{p}-\vec{p_1}\right|-l_0)\]

$\vec{p}$ ... Position der Masse\\
$\vec{p_1}$... Position des Fixpunkts\\
$\vec{v}$ ... Geschwindigkeit der Masse\\
$\vec{a}$ ... Beschleunigung der Masse\\
$m$... Masse der Masse\\
$k$ ... Federkonstante der Feder\\

Nach dem Aufstellen der Differentialgleichungen k�nnen beide Varianten implementiert werden.\\
Um ein Differentialgleichungs-System der zweiten Ordnung l�sen zu k�nnen muss man nur die Funktion f�r die zweite Ableitung implementieren und diese mit dem momentanen Stand des Systems dem Differentialgleichungs-Solver �bergeben. Siehe Codeausschnitt \ref{code:Eulerrungebsp}.



\begin{longlisting}
	\csharpcode{Paul/src/EulerRungeBSP.cs}
	\caption{Implementation Simulation}
	\label{code:Eulerrungebsp}
\end{longlisting}

Bei diesem Beispiel war die Funktion der Beschleunigung, \textbf{Accel} zu implementieren die dann an \textbf{SolveEulerOde2()} oder \textbf{SolveRungeOde2} zu �bergeben. Vergleiche Codeausschnitt \ref{code:Eulerrungebsp}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/EulervsRunge}
	\caption{Simulation von Euler und Runge-Kutta Verfahren mit Schrittgr��e 1s}
	\label{fig:eulervsrunge}
\end{figure}

Wie in Abbildung \ref{fig:eulervsrunge} zu sehen ist liegen die Werte, die mit dem Runge-Kutta-Verfahren berechnet wurden, fast genau auf der exakten L�sung und die Euler Methode hat einen sichtbaren Fehler.
\smallskip
Der Vergleich von den beiden Methoden bei Gleichen Schrittgr��en ist aber etwas unfair, da das Runge-Kutta-Verfahren vier Mal so viele Funktionsaufrufe und so auch die vierfache Rechenzeit braucht um einen Schritt zu berechnen. Deshalb kann das Euler-Verfahren durch Substeps erweitern, dabei wird f�r jeden Schritt das Euler-Verfahren mehrere Male ausgef�hrt.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/EulervsRungeSubsteps}
	\caption{Simulation von Euler mit Substeps und Runge-Kutta mit Schrittgr��e 1s}
	\label{fig:eulervsrungesubsteps}
\end{figure}

Wenn das Euler-Verfahren vier Mal pro Schritt ausgef�hrt wird, wird zwar die Abweichung geringer, aber nicht kleiner als beim Runge-Kutta-Verfahren. Bei ungef�hr gleicher Rechenzeit ist das Runge-Kutta-Verfahren immer noch genauer. Vergleiche Abbildung \ref{fig:eulervsrunge}. \\
Kleinere Abweichungen des Systems in Videospielen machen nicht so viel aus. Probleme gibt es, wenn das System Instabil wird, wie es bei gro�en Schrittgr��en passieren kann. Siehe Abbildung \ref{fig:eulervsrungeinstabil}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/EulervsRungeInstabil}
	\caption{Instabilit�t bei gro�en Schritten}
	\label{fig:eulervsrungeinstabil}
\end{figure}

Da in der Niob Physik Engine wegen dem L�sen von Kollisionen und Einschr�nkungen ohnehin schon sehr kleine Zeitabst�nde simuliert werden, kann ein ungenaueres L�sungsverfahren verwendet werden, da die Abweichungen ohnehin schon gering sind. Zus�tzlich sind kleine Abweichungen in Videospielen verkraftbar, solange die Dynamik des Systems nicht grundlegend ver�ndert wird.

\subsection{Constraints}

Um bei einer Physik Engine zum Beispiel Verbindungen von zwei K�rpern darzustellen werden Constraints verwendet. Das sind Bedingungen, die bei der Simulation eingehalten werden m�ssen. So kann beispielsweise angeben werden, dass ein K�rper zu einem anderen einen Gewissen abstand einhalten soll, oder dass diese in die Gleiche Richtung schauen m�ssen. 
\smallskip
Wenn eine Bedingung nicht eingehalten wird, muss eine Kraft, die Constraint-Kraft daf�r sorgen, dass die Bedingung wieder eingehalten wird. Die von der Constraint ausge�bte Kraft zeigt immer, in die Richtung, bei der sich die Abweichung am meisten verkleinert. 
\smallskip
Je nach Bedingungsart ist die Kraft unterschiedlich gro�. 
Bei Hard-Constraints wird vorgegeben, dass die Bedingung unter allen Umst�nden eingehalten werden muss, und bei Soft-Constraints werden die K�rper je nach Starrheit mehr oder weniger stark in die Ideale Position gezogen.
\smallskip
Oft alle Interaktionen zwischen K�rpern in Physik Engines als Constraints dargestellt. Federn k�nnen auch als Soft-Constraint angesehen werden, bei der die zwei K�rper einen gewissen zueinander einhalten sollten. Wird diese Bedingung nicht eingehalten wird eine Kraft proportional zur Abweichung auf die beiden K�rper ausge�bt. Wie stark die Kraft mit steigendem Abstand anw�chst, repr�sentiert dabei die Federkonstante der Feder\\
Kollisionen k�nnen mit Hard-Constraints, die den Minimalabstand zwischen zwei K�rpern immer gr��er null halten sollten, beschrieben werden.

\subsubsection{Arten von Constraint Engines}
Bei einer Kraftbasierten Physik Engine werden Kr�fte ausge�bt, um diese auf den Vorgesehenen Bahnen oder Positionen zu halten. Liegt ein K�rper auf einer dem Boden so wirkt die Normalkraft des Bodens der Anziehungskraft der Erde entgegen. Siehe Abbildung \ref{fig:normalforces}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/NormalForces}
	\caption{Normalkr�fte bei aufeinanderliegenden K�rpern}
	\label{fig:normalforces}
\end{figure}

Bei Kollisionen h�ngt die ausge�bte kraft meist davon ab wie tief die K�rper ineinander eingedrungen sind.\\
Die Constraint und Kollisionskr�fte k�nnen dem Differentialgleichungs-Solver �bergeben werden der dann das System simuliert.\\
Da zum Beispiel bei Kollisionen Rigid-Bodies im Idealfall instantan einen Richtungswechsel vornehmen k�nnen die Kr�fte bei einer Kraft-Basierten Physik Engine sehr gro� werden.
\[\Delta v=\Delta t\ast\frac{F}{m}\]

\subsubsection{Impulsbasierte Physik Engine}
Bei Impulsbasierten Physik Engines werden die Kollisionen von Rigid-Bodies mit Impulsen gel�st. Dabei werden die Geschwindigkeiten der Beiden K�rper nach dem Zusammensto� anhand der Masse und Geschwindigkeiten vor dem Zusammensto� berechnet. Die L�sung des Systems folgt aus dem Energieerhaltungssatz und Impulserhaltungssatz. Siehe Abbildung \ref{fig:impulskollisionwikipediakopiert}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/ImpulsKollisionWikipediaKopiert}
	\caption{Impulse bei einem Zusammensto� \footfullcite{wikiCollisionResponse}}
	\label{fig:impulskollisionwikipediakopiert}
\end{figure}

Die �nderungen der Geschwindigkeiten werden direkt auf die K�rper angewandt und nicht von Differentialgleichungs-Solver beachtet. Da bei Aufeinander liegenden Objekten die Kollision mit fast keiner Geschwindigkeit auftreten, werden die Objekte auch nicht auseinandergehalten. Um dies zu verhindern, m�ssen zus�tzlich noch kleinere Kollisionskr�fte auf die K�rper ausge�bt werden, die auftreten, wenn sich die K�rper in einem bestimmten Abstand zueinander befinden.

\subsubsection{Positionsbasierte Physik Engine}

Bei positionsbasierten Systemen werden die Positionen der K�rper so angepasst, dass die Anforderung des Systems erf�llt werden. Durch die Anpassung der Positionen die Bewegungsgeschwindigkeit nicht angepasst werden, m�ssten die gleichen Anpassungen der Positionen bei jedem Zeitschritt erneut get�tigt werden. Deshalb werden die Geschwindigkeiten bei jedem Zeitschritt angepasst, sodass sie die Bewegung in diesem Zeitschritt widerspiegeln. Siehe Abbildung \ref{fig:pbdkollision1}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/PBD_Kollision1}
	\caption{Kollisionen mit PBD: Bewegung}
	\label{fig:pbdkollision1}
\end{figure}

F�r jeden Simulationsschritt in Einer Positionsbasierten Engine werden zuerst die Bewegungen f�r jeden K�rper simuliert, dabei werden Kollisionen oder andere Constraints nicht ber�cksichtigt. Siehe Abbildung \ref{fig:pbdkollision2}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/PBD_Kollision2}
	\caption{Kollision mit PBD: Kollisionserkennung}
	\label{fig:pbdkollision2}
\end{figure}

Wenn eine Kollision auftritt, werden die K�rper auf den k�rzesten Weg $\vec{d}$ auseinander bewegt, sodass sie sich danach nicht mehr �berschneiden. Vergleiche Abbildung \ref{fig:pbdkollision3}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/PBD_Kollision3}
	\caption{Kollision mit PBD: Kollisionsreaktion}
	\label{fig:pbdkollision3}
\end{figure}

Der K�rper hat sich somit in diesem Schritt um $\Delta t\ast\vec{v}$ und um den Weg $\vec{d}$ bewegt, und hat somit die Geschwindigkeit $\vec{v}+\frac{\vec{d}}{\Delta t}$. Um nicht bei jeder Anpassung der Position die Geschwindigkeit anpassen zu m�ssen, wird die Geschwindigkeit anhand des Unterschieds zwischen der alten und neuen Position angepasst. Siehe Abbildung \ref{fig:pbdkollision4}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/PBD_Kollision4}
	\caption{Kollision mit PBD: Geschwindigkeitsanpassung}
	\label{fig:pbdkollision4}
\end{figure}

\subsection{Unterschied zwischen Positionsbasiert und Kraftbasiert anhand eines Beispiels}

Bei dem Beispiel sollte die Bewegung eines Partikels mit einer Constraint so eingeschr�nkt werden, dass sich das Teilchen nur auf einem Kreis um einen Punkt bewegen kann. Vergleiche Abbildung \ref{fig:teilchenaufkreis}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Teilchen_auf_Kreis}
	\caption{Punkt bewegt sich auf einem Kreis}
	\label{fig:teilchenaufkreis}
\end{figure}

Aus der Bedingung, dass der Punkt $p$ immer einen Abstand von $d$ zu $P$ einhalten muss bekommt man diese Gleichung f�r die Abweichung, die immer null sein solle.

\[\left|\vec{Pp}\right|-d=\left|\vec{C}\right|\]
\[\vec{C}=\vec{Pp}\left(1-\frac{d}{\left|\vec{Pp}\right|}\right)\]

\paragraph{Kraftbasierte L�sung}  \enspace \smallskip

Weil die Abweichung $C$ immer null bleiben sollte; folgt daraus, dass die Ableitungen von $C$ auch null sein m�ssen.

\[\dot{\vec{C}}=\frac{d}{dt}\left(\vec{Pp}\left(1-\frac{d}{\left|\vec{Pp}\right|}\right)\right)\]
\[\dot{\vec{C}}=\dot{\vec{p}}\left(1-\frac{d}{\left|\vec{Pp}\right|}\right)+\vec{p-P}\left(\frac{d}{\left|\vec{Pp}\right|^3}\left(\left(p_x-P_x\right)\dot{p_x}+\left(p_y-P_y\right)\dot{p_y}\right)\right)\]

Unter der Annahme, dass $\left|\vec{C}\right|$ gleich null ist kann der Ausdruck vereinfacht werden. Zus�tzlich kann f�r die Ableitung der Position die Geschwindigkeit eingesetzt werden.

\[\dot{\vec{C}}=\frac{\vec{Pp}}{d^2}\left(\left(p_x-P_x\right)v_x+\left(p_y-P_y\right)v_y\right)\]
\[\dot{\vec{C}}=\frac{\vec{p-P}}{d^2}\left(\vec{Pp}\cdot\vec{v}\right)\]
Die Zweite Ableitung muss ebenfalls null sein.
\[\ddot{\vec{C}}=\frac{\vec{v}}{d^2}\left(\left(p_x-P_x\right)v_x+\left(p_y-P_y\right)v_y\right)+\frac{\vec{Pp}}{d^2}\left(v_x^2+\left(p_x-P_x\right)\dot{v_x}+v_y^2+\left(p_y-P_y\right)\dot{v_y}\right)\]
Weil die Geschwindigkeit nur durch die Beschleunigung ver�ndert wird, kann dieser Term kann erneut vereinfacht werden, indem angenommen wird, dass$ \dot{\vec{C}}$ auch null ist.
\[\ddot{\vec{C}}=\frac{\vec{Pp}}{d^2}\left(v_x^2+\left(p_x-P_x\right)a_x+v_y^2+\left(p_y-P_y\right)a_y\right)=\frac{\vec{Pp}}{d^2}\left(\vec{Pp}\cdot\vec{a}+\vec{v}\cdot\vec{v}\right)\]

Wenn man die Bedingung einhalten will, dass $\ddot{\vec{C}}$ null ist, muss man den Ausdruck mit der Constraint-Kraft $\vec{F}$ korrigieren.

\[\ddot{\vec{C}}=\frac{\vec{Pp}}{d^2}\left(\vec{Pp}\cdot\left(\vec{a_1}+\frac{\vec{F}}{m}\right)+\vec{v}\cdot\vec{v}\right)=0\]

\[\vec{Pp}\bullet\vec{F}=-m\left(\vec{Pp}\cdot\vec{a_1}+\vec{v}\cdot\vec{v}\right)\]
$\vec{a_1}$ ... Beschleunigung ohne aus�ben der Constraint-Kraft\smallskip

Die Gleichung kann noch nicht nach $\vec{F}$ gel�st werden, da $\vec{F}$ ein Vektor ist und so auch mehrere Unbekannte enth�lt. Unter der Bedingung, dass die Constraint-Kraft $\vec{F}$ keine Arbeit verrichtet kann das System gel�st werden.
Da keine Arbeit geleistet wird muss auch die Leistung null sein.

\[P=\vec{v}\cdot\vec{F}=0\]

Das Skalarprodukt ist null, wenn beide Vektoren normal zueinander sind und weil $\vec{Pp}\cdot\vec{v}$ auch null ist m�ssen in zwei Dimensionen $\vec{F}$ und $\vec{Pp}$ in die Gleiche oder Entgegengesetzte Richtung zeigen.

\[\vec{F}=\lambda\vec{Pp}\]
\[\vec{Pp}\bullet\lambda\vec{Pp}=-m\left(\vec{Pp}\cdot\vec{a_1}+\vec{v}\cdot\vec{v}\right)\]
\[\vec{F}=-\vec{Pp}\frac{m\left(\vec{Pp}\cdot\vec{a_1}+\vec{v}\cdot\vec{v}\right)}{\vec{Pp}\bullet\vec{Pp}}\]

Zu beachten ist, dass das Teilchen bei diesem Ansatz nur in der richtigen Bahn bleibt, wenn die Ausgangsbedingungen die Constraint-Bedingungen erf�llen. Um auch von allen Ausgangspunkten starten zu k�nnen m�ssten noch weitere Kr�fte hinzugef�gt werden.\smallskip

Die Beschleunigung des Teilchen kann erhalten werden, wenn die Constraint-Kraft wieder eingesetzt wird.

\[\vec{a}=\vec{a_1}+\frac{\vec{F}}{m}=\vec{a_1}-\vec{Pp}\frac{\left(\vec{Pp}\cdot\vec{a_1}+\vec{v}\cdot\vec{v}\right)}{\vec{Pp}\bullet\vec{Pp}}\]

Die Beschleunigungsfunktion kann dann wie im Beispiel mit der Feder von einem Differentialgleichungs-Solver gel�st werden. Vergleiche Codeausschnitt \ref{code:ForceConstraint}.

\begin{longlisting}
	\csharpcode{Paul/src/ForceBasedConstraint.cs}
	\caption{Implementation kraftbasierter Constriaints}
	\label{code:ForceConstraint}
\end{longlisting}


\paragraph{Positionsbasierte L�sung} \enspace \smallskip

Bei der Positionsbasierten L�sung wird das Teilchen auf den k�rzesten Weg wieder auf die Bahn gesetzt. 

\[\vec{C}=\vec{Pp}\left(1-\frac{d}{\left|\vec{Pp}\right|}\right)\]

Die Distanz entspricht der negativen Abweichung $-\vec{C}$.

\[\vec{p_{neu}}=\vec{p}-\vec{C}\]

Nach dem die neue Position berechnet wurde muss die Geschwindigkeit angepasst werden, indem die Positions�nderung des Teilchens w�hrend dem Simulationsschritts berechnet wird.

\begin{longlisting}
	\csharpcode{Paul/src/PBDConstraints.cs}
	\caption{Implementation Positionsbasierte Constraints}
\end{longlisting}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{Paul/IMG/Simulation_PBD-Kraft}
	\caption{}
	\label{fig:simulationpbd-kraft}
\end{figure}


