[TOC]



## TeXstudio installieren und Erstkonfiguration

Es wurde diverse Zusatzsoftware für TeXstudio verwendet. Die in dieser DA beschriebene Reihenfolge ist eine mögliche, aber keine zwingende Installationsreihenfolge, außer wenn es anders vermerkt ist. Wenn man hingegen die Diplomarbeitsfiles kompilieren möchte, muss man alle Zusätze installiert haben, ansonsten bekommt man eine Fehlermeldung, da diverse Sachen dann noch fehlen. Das ist an sich nicht schlimm, kann aber durch Befolgen der hier angegebenen Reihenfolge vermieden werden.

Anmerkungen: Zur Installation benötigt man eine Internetverbindung. Alle angegebenen Files sind Windows-Installationsfiles bzw. Vorgangsweisen für Windows.

### Installation von TeX Live 

Für die Installation von TeXstudio wird empfohlen, auch eine TeX-Distribution zu empfehlen. Es wird als solche Tex Live verwendet, da TeX Live die umfangreichste verfügbare Distribution ist und diese auch von RUH empfohlen worden ist. Man findet das Installationsfile unter `Niob-Engine\Software\texstudio\install-tl-windows.exe`. 

Wenn man das File öffnet, sieht man das in Abbildung 1 gezeigt Installer-Fenster. Da direkt installiert werden soll, muss man auf _Next >_ klicken und dann auf _Install_ (Abbildung 2).

![image-20230307180644756](TeXstudio installieren.assets/image-20230307180644756.png)Abbildung 1

![image-20230307180717590](TeXstudio installieren.assets/image-20230307180717590.png)Abbildung 2

Sobald der erste Teil der Installation abgeschlossen ist, erscheint ein neues Fenster (Abbildung 3). In diesem Fenster kann man den gewünschten Installationspfad angeben, die restlichen Einstellungen können auf dem voreingestellten Wert gelassen werden ( Standard-Papierformat A4, TeXworks als Frontend installiert ). Danach muss man auf _Installation_ klicken.

![image-20230307180726272](TeXstudio installieren.assets/image-20230307180726272.png)Abbildung 3

Sobald dieser Teil der Installation abgeschlossen ist, ist TeX Live fertig installiert.



### Pygments

Pygment ist ein Python-Syntax-Highlighter. Zur Installation benötigt man bereits eine laufende Python-Version 3.0 oder neuer und pip (ein Python-Installationsprogramm). 

Python ist weit verbreitet, weshalb einige vermutlich schon eine Python-Version installiert haben. Für Pygment benötigt man eine Version 3.0 oder höher. Überprüfen, ob man Python installiert hat und ob die aktuelle Version neu genug ist, kann man, indem man in der Eingabeaufforderung den Befehl `py --version` eingibt.

```bash
C:\Users\Matteo>py --version
Der Befehl "py" ist entweder falsch geschrieben oder
konnte nicht gefunden werden.
```

Wenn man diese Rückmeldung bekommt, ist Python nicht installiert.

Wenn Python installiert ist, erhält man eine Rückmeldung wie z.B. die nachfolgende.

```bash
C:\Users\Matteo>py --version
Python 2.7.8
```

In diesem Fall ist Python installiert, wenn die Version aber älter als 3.0 ist ( so wie in diesem Fall ), muss man Python noch nachinstallieren. Falls es in zukünftigen Versionen Probleme geben sollte mit den Python-Befehlen, könnte eine Installation dieser Version auch hilfreich sein. Sollte eine passende Version installiert sein, kann der Schritt `Installation von Python Version 3` übersprungen werden.

Auch bei pip kann man ähnlich wie davor in der Eingabeaufforderung mit dem Befehl `pip --version` überprüft werden, ob das Programm bereits installiert ist.

```bash
C:\Users\Matteo>pip --version
Der Befehl "pip" ist entweder falsch geschrieben oder
konnte nicht gefunden werden.
```

In diesem Fall ist pip nicht installiert und es muss nachinstalliert werden. Sollte eine Version gefunden werden, kann der Schritt `Installation von pip` übersprungen werden.

#### Installation von Python Version 3

Zur Installation von Python 3.11.2, das bei der Entwicklung verwendet wurde, muss man das File `Niob-Engine\Software\texstudio\python-3.11.2-amd64.exe"` ausführen. Zuerst muss man im sich dann öffnenden Installationsprogramm _Add python.exe to PATH_ anwählen und dann auf _Install Now_ klicken (Abbildung Bla).

![image-20230308102427862](TeXstudio installieren.assets/image-20230308102427862.png) Abbildung Bla

Sobald die daraufhin folgende Installation abgeschlossen ist, kann es sein, dass das in Abbildung Blabla zu sehende _Disable path legth limit_ angezeigt wird. Wenn das der Fall ist, muss man das Längenlimit mit Anklicken des Feldes deaktivieren. Danach oder wenn das Limit schon deaktiviert war ist die Installation von Python abgeschlossen.

![image-20230308102803009](TeXstudio installieren.assets/image-20230308102803009.png) Abbildung Blabla



// Braucht man pip überhaupt? Ich weiß es noch nicht

#### Installation von pip

Das Installationsfile `Niob-Engine\Software\texstudio\get-pip.py` vom Python-Installationsprogramm pip kann man über die Eingabeaufforderung mit dem nachfolgenden Befehl installieren.

```bash
C:\Users\Matteo>python "C:\Users\Matteo\Niob-Engine\Software\texstudio\get-pip.py"
```

Sobald dieser Befehl ausgeführt wurde, sieht man als letzte Zeile den folgenden Text.

```bash
Successfully installed pip-23.0.1 wheel-0.38.4
```

Sollte diese Zeile nicht erscheinen, muss man den Vorgang wiederholen. Auftreten kann der Fehler wenn man z.B. keine Internetverbindung hat.

Wenn die Installation erfolgreich war, sollte bei erfolgreichem Installieren mit dem Versionsbefehl der folgende Text zu sehen sein.

```bash
C:\Users\Matteo>pip --version
pip 23.0.1 from C:\Users\Matteo\AppData\Local\Programs\Python\Python311\Lib\site-packages\pip (python 3.11)
```



### Installation von Pygment

Das Installationsfile von Pygment (`Niob-Engine\Software\texstudio\Pygments-2.14.0\setup.py`) muss man mit Python installieren. Dafür muss man in der Eingabeaufforderung in den Ordner `Niob-Engine\Software\texstudio\Pygments-2.14.0` wechseln und dort dann den Befehl `python setup.py install` ausführen, so wie im nachfolgenden Code zu sehen.

```bash
C:\Users\Matteo>cd repos/Niob-Engine/Niob-Engine/Software/texstudio/Pygments-2.14.0

C:\Users\Matteo\repos\Niob-Engine\Niob-Engine\Software\texstudio\Pygments-2.14.0>python setup.py install
```



### Installation von TeXstudio

Der Nächste Schritt ist die Installation von TeXstudio, dem eigentlichen LaTeX-Editor. Das Installationsfile findet man unter `Niob-Engine\Software\texstudio\texstudio-4.5.1-win-qt6.exe`. Wenn man dann den Anweisungen des Installation Wizards folgt (man muss keine Änderungen vornehmen), wird das Programm korrekt installiert.

Ab Abschluss der Installation kann das Programm über die Suche nach _TeXstudio_ gefunden werden. 

### Erstkonfiguration

Die zur Erstellung dieser DA verwendeten Einstellungen unterscheiden sich von den Standardeinstellungen. Daher empfiehlt es sich, für gleiche Ergebnisse unter _Optionen > TeXstudio konfigurieren..._ (Abbildung 4) diverse Einstellungen zu setzen.

![image-20230307183620100](TeXstudio installieren.assets/image-20230307183620100.png)Abbildung 4

Im Abschnitt _Befehle_ ist es wichtig, den Befehl `-shell-escape` im Feld _PdfLaTeX_ hinzuzufügen (Abbildung 5).

![image-20230307185003257](TeXstudio installieren.assets/image-20230307185003257.png) Abbildung 5

Im Abschnitt _Erzeugen_ ist es wichtig, das Standard-Bibliographieprogramm auf `Biber` zu setzen (Abbildung 6).

![image-20230307185309621](TeXstudio installieren.assets/image-20230307185309621.png) Abbildung 6



Sobald diese Schritte alle abgeschlossen sind, hat man den Stand erreicht, mit dem bei der DA gearbeitet wurde.
