#include <avr/io.h>
#include "NiobController.h"

// Funktion zum Auslesen eines ADCs
int Read_ADC(char channel){
	ADMUX&=0xE0;					//Alten Channel löschen
	ADMUX |= channel&0x1F;			//Channel einstellen

	ADCSRA |= (1<<ADSC);			//Einzellmessung starten
	while (ADCSRA & (1<<ADSC)){};	//Auf Ende der Messung warten

	return ADC;						//10 Bit ausgeben
}

int main(void)
{
	//S0-S3 als Input festlegen
	DDRA &= 0xD0;
	PORTA|= 0x0F;

	// ADC initialisieren
	//Einschalten, relative Messung, rechtsbündig
	ADCSRA|= (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); 				
	ADMUX|= (1<<REFS0);

	Init_Controller();
	
	char buttons_Last = 0;
	int adc_Last = 0;
	while (1)
	{
		char buttons = (~PINA)&0x0F;
		// Sicherstellen dass Daten nur gesendet werden wenn sich der Wert verändert hat
		if (buttons_Last^buttons) Send_Input(0,buttons);
		
		int adc_val = Read_ADC(5);
		// Sicherstellen dass Daten nur gesendet werden wenn sich der Wert verändert hat
		if (adc_Last^adc_val) Send_Input(1,adc_val);
		
		// Abspeichern des letzten Zustands des Buttons und des ADC-Wertes
		buttons_Last = buttons;
		adc_Last = adc_val;
	}
}