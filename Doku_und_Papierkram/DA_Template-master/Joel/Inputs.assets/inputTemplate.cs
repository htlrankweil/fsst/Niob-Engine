// template
private static void MEGACardNewInput(string com)
{
    MEGACard_Input megaInput = new MEGACard_Input(com, true);
    megaInput.CompleteDataPackageRcv += OnCompleteDatapackageRcv;
}

private static void OnCompleteDatapackageRcv(object sender, EventArgs e)
{
    MEGACard_Input.DataAllRecievedEventArgs eventArgs = e as MEGACard_Input.DataAllRecievedEventArgs;
    // Write what to do on data recieved here
}