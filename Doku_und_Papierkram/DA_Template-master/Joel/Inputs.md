# Inputs

Anmerkung: Da in dieser DA bzw. dieser Game Engine speziell auf die Bedürfnisse der HTL Rankweil eingegangen wurde, wurde eine Unterstützung der MEGACard, des HTL-Rankweil-hauseigenen Mikrocontroller-Boards implementiert.

Inputs sind Aktionen oder Ereignisse, die von Spielern ausgelöst werden und die im Programm Befehle triggern. Inputs können z. B. Tastatureingaben, Mausbewegungen oder ein Tastendruck an der MEGACard sein.

Die Game Engine empfängt und verarbeitet diese Inputs, um das Verhalten des Spiels zu steuern. Je nach Art des Inputs kann die Engine unterschiedliche Aktionen ausführen, wie z. B. die Bewegung des Spielcharakters, das Auslösen von Animationen, das Abspielen von Soundeffekten oder das Starten von Skripten.

Die komplette Verarbeitung muss sehr schnell stattfinden, da der Spieler sonst im schlimmsten Fall denkt, das Spiel habe seinen Tastendruck nicht erkannt und mehrfach drückt, obwohl das Spiel eigentlich nur am Rechnen war und darum etwas verzögert reagiert.

## MEGACard-Input

Ziel ist es, von der MEGACard aus Daten an die Niob-Engine zu senden. Dabei wird das in Abbildung blabla zu sehende Datenprotokoll verwendet. Es wird mit jedem Mal Senden ein Byte verschickt. In jedem Byte sind die ersten zwei Bits Adress-Bits, an denen die Richtigkeit der Übertragung geprüft wird. Die restlichen sechs Bits sind Channel- oder Daten-Bits

In den restlichen sechs Bits wird die beim jeweils ersten Byte immer ein Channel angegeben. Es gibt 6 Channel-Bits, dementsprechend gibt es 2^6 verschiedene Channels.

Beim zweiten, dritten und vierten Byte werden dann die eigentlichen Daten verschickt. Das erste Daten-Bit im zweiten Byte ist das most significant Bit (MSB), also das höchstwertigt Bit. Bei diesem Protokoll hat es einen Wert von 2^18-1, weil es 18 Daten-Bits gibt. Das LSB ist das achte Bit des vierten Bytes.

![DataSentSerial_Structure](Inputs.assets/DataSentSerial_Structure.png)

### Einführung zur MEGACard

Die MEGACard ist ein von der HTL Rankweil entworfenes und entwickeltes Mikrocontroller-Board mit zahlreichen Peripherien. Es wird an jeden Elektronik-und-technische-Informatik-Schüler der HTL Rankweil verteilt und in diversen Unterrichtsfächern benutzt. 

![image-20230318181021265](Inputs.assets/image-20230318181021265.png)

Auf der Platine ist ein ATMega16 verbaut, ein 16kB großer programmierbarer Flashbaustein von Atmel. (Fußnote Quelle atmega-datasheet)

Mittlerweile gibt es schon fünf Versionen der MEGACard. Auf der neuesten Version (V5) gibt es zahlreiche Peripherien. Unter anderem die Folgenden:

- USB-Mini-Anschluss
- Integriertes Display
- Analog-Digital-Converter
- 4 programmierbare Buttons + 1 hardwaremäßiger Reset-Button
- UART-Schnittstelle
- IIC-Schnittstelle

Der Schaltplan ist in der nachfolgenden Grafik zu sehen.

![image-20230320124931782](Inputs.assets/image-20230320124931782.png)

Der USB-Anschluss ist direkt mit der UART-Schnittstelle verbunden, was es ermöglicht, direkt über ein USB-Kabel mit dem Computer bzw. der Niob-Engine zu kommunizieren. Wichtig zu beachten ist jedoch, dass alle Versionen vor V5 noch keine direkte USB-UART-Kommunikation implementiert haben.

Viele der momentanen Schüler besitzen noch ein älteres Modell der MEGACard, in naher Zukunft werden aber nur noch die neuesten Versionen verteilt werden und sich somit auch längerfristig etablieren. Wegen der einfacheren UART-Möglichkeit und da diese Game Engine das Potenzial hat, noch in vielen zukünftigen Schulprojekten oder Diplomarbeiten weiterentwickelt zu werden, wird die neueste Version der MEGACard als Entwicklungsversion genommen. Dadurch wird auch die Wahrscheinlichkeit verringert, dass Features entwickelt werden, die wegen Entfernen von Peripherien (so wie zum Beispiel der Helligkeitssensor von V4 auf V5 entfernt wurde) später nicht mehr brauchbar sind. 

### UART

Die MEGACard-Computer-Kommunikation basiert auf UART. UART steht für Universal Asynchronous Reciever Transmitter. Jedes UART-fähige Device hat eine Recieving-Input (R, Rx oder RxD) und einen Transmitting-Output (T, Tx oder TxD). Dabei ist R die Leitung, auf der empfangen wird und T die Leitung, auf der Daten gesendet werden. Das bedeutet, dass Recieve des einen Geräts mit Transmit des anderen Geräts verbunden werden muss, so wie in Abbildung blabla zu sehen.

![UART](Inputs.assets/UART.png)

Oftmals wird auch noch eine gemeinsame Ground-Leitung verwendet, damit Fehler durch unterschiedliche relative Spannungspegel vermieden werden.

#### UART-Frame

Ein UART-Frame besteht aus einem Start-Bit, Datenbits, möglicherweise einem Parity-Bit und ein bis zwei Stop-Bits, s.

Da es keinen Clock gibt, wird ein Start-Bit verwendet, um die Übertragung zu beginnen. Der Sender zieht dazu seine Transmit-Leitung aus dem Ruhezustand (einem High-Pegel) auf Low für eine Bitdauer.

Danach werden die Daten gesendet. Es können fünf bis neun Datenbits gesendet werden, die Anzahl bei den einzelnen Übertragungen variiert aber nicht. Es ist eine nichtinvertierte Übertragung, High bedeutet dementsprechend 1 und Low 0.

Wenn die Übertragung der Datenbits senderseitig abgeschlossen ist, wird wahlweise ein Parity-Bit gesendet. Dieses kann Odd, Even oder None sein. Odd bedeutet, dass das Parity-Bit so gesetzt wird, dass die Anzahl Einser insgesamt ungerade ist. Even bedeutet, dass die Anzahl Einser mit dem Parity-Bit gerade ist. None bedeutet kein Parity-Bit (direkt die Stop-Sequenz).

Die Stop-Sequenz kann aus ein oder zwei Stop-Bits bestehen, die jeweils High sind, also auf dem Ruhezustand-Level.

Wichtig zu definieren ist außerdem noch die Zeit, wie lang ein Bit dauert bzw. die Frequenz, mit der Bits pro Sekunde gesendet werden.

Die Niob-Engine verwendet als Einstellung 9600Baud 8N1, das heißt eine Baudrate von 9600 Bit pro Sekunde, 8 Datenbits pro UART-Frame (also 1 Byte), kein Parity-Bit und ein Stop-Bit. 

### Umsetzung bei der MEGACard

#### UART bei älteren MEGACard-Versionen

Wenn mit einer älteren Version als V5 der MEGACard mit einem PC kommuniziert werden soll, ist das auch möglich, wenn auch nicht ganz so simpel (siehe Abbildung blabla). PD0 ist der Recieve-Data-Anschluss der MEGACard, PD1 der Transmit-Data-Anschluss. Diese müssen an einen USB-Converter so angeschlossen werden, wie es angeschrieben steht. Hier muss aufgepasst werden, ob eine RxD-Beschriftung sich auf die Recieve-Leitung des USBs oder den Anschlusspin für ein Recieve-Leitung bezieht (bzw. das gleiche für TxD). Die korrekte Anschlussweise wird standardmäßig im Datenblatt beschrieben.
Es ist auch empfehlenswert, den Ground (GND) des USB-Anschlusses mit dem der Platine zu verbinden.

![](Inputs.assets/UART_V4.png)

#### Software-Implementierung von UART auf der MEGACard

Damit Daten per UART-Protokoll gesendet werden können, kann die in C programmierte Klasse `NiobController` verwendet werden. Diese kümmert sich nur um das einmalige Initialisieren und danach um das Senden der Daten kümmert. Bei der für volle Funktionalität einmal aufzurufenden Initialisierungsmethode `Init_Controller()` werden dabei die in Codeblock blabla zu sehenden Einstellungen gesetzt.

blablaeEinCodeblockCcode

Nach der Initialisierung, die im Main-Code des Benutzers eingebaut werden muss, können per Aufruf der Funktion `Send_Input(char channel, long data)` Daten gesendet werden. Dabei müssen der gewünschte Channel und die zu sendenden Daten übergeben werden.

#### C-Code-Beispiel für MEGACard

Wie ein praktisches Anwendungsbeispiel für C-Code, angewendet auf der MEGACard, ausschauen kann, ist in Codeblock blabla zu sehen.

blablaCodeBlockCCode





### Umsetzung in der Niob-Engine

Der C#-MEGACard-Input ist in der Klasse `MEGACard_Input.cs` programmiert. Das grundsätzliche Prinzip ist, dass der Benutzer in einer Liste der Klasse Funktionen abspeichert, aus der die entsprechende automatisch aufgerufen wird, wenn ein vollständiger Datensatz empfangen wurde. Außerdem kümmert sich die Klasse um das Auslesen des USB-Ports, das Parsen der Daten (das Entfernen von nicht zugehörigen Bits wie den Start-, Parity- und Stop-Bits und das Zusammenfügen zugehöriger UART-Frames) und die Datenüberprüfung (ob einzelne UART-Frames verloren gegangen sind).

#### UART

Zum Einlesen des COM-Ports wird `System.IO.Ports.SerialPort` verwendet. Instanzen dieser Klasse können eine Baudrate, eine Parity, eine Anzahl Data-Bits und eine Anzahl Stop-Bits zugewiesen bekommen (siehe Codeblock blabla). SerialPort kümmert sich um das Senden und Empfangen von Daten, wichtig ist nur, dass es gleich eingestellt wird wie der Sender, in diesem Fall wie die MEGACard (in Codeblock blabla zu sehen). 

blablaEinCodeBlockCsharp-sPort

Diese Einstellungen sind vorkonfiguriert und werden bei Aufruf der Funktion `Connect(string port)` gesetzt. 

Wichtig ist, dass man einen Port angibt, also bei welchem Anschluss am Computer der SerialPort zuhört und schreibt. Die verfügbaren Ports können mit dem Befehl `string[] GetAvailablePorts()` ausgelesen werden.

Statt den Port auszulesen und auf dieser Basis zu entscheiden, welcher genommen werden soll, gibt es auch die Funktion `string AutoConnectToFirstAvailableSerialPort(string[] excludedPorts)`. Diese Funktion fragt alle verfügbaren Ports ab und gleicht sie der Reihe nach (COM1, COM2, COM3, ...) mit den in excludedPorts angegebenen ab. Sobald ein Port nicht in excludedPorts ist, wird mit diesem eine UART-Verbindung versucht aufzubauen. Sollte das nicht klappen, wird der nächste nicht exkludierte Port ausprobiert und so weiter. Die Funktion liefert den Port zurück, mit dem sich das-SerialPort-Objekt verbunden hat.
Eine Liste von exkludierten Ports kann benötigt werden, weil viele Computer  Tastatur und / oder Maus an einem USB-Port angeschlossen haben. Wenn z.B. bei einem Rechner alle dauerhaft belegten Ports bekannt sind, kann so vermieden werden, dass der MEGACard-Input ein Gerät anspricht, das nicht die MEGACard ist.

Es gibt die Funktion `Send(byte toSend)`, mit der auch Daten von der Engine aus gesendet werden können. MEGACard-seitig ist aber nichts implementiert worden, da es viele wichtigere Bereiche gibt.

#### Daten verarbeiten

Wenn ein Datenpaket (in diesem Fall je 8 Bit) vom SerialPort empfangen wurden, löst dieser ein Event aus. Je nachdem, was beim Deklarieren der Klasse angegeben wurde, wird ein MSB-First- oder LSB-First-Parsing-Event ausgelöst. Der Unterschied der beiden Events besteht nur darin, wie die Datensätze (je 6 Bits) aneinandergereiht werden. In den Eventhandlern wird geprüft, ob schon 4 oder mehr Datenpakete angekommen sind und wenn ja, wird das erste angeschaut. Ist die Adresse 00, wird der Datensatz extrahiert und zwischengespeichert. Ansonsten wird es verworfen. Beim zweiten Paket wird wieder die Adresse angeschaut. Sollte sie 01 sein, wird der Datensatz zwischengespeichert. Das gleiche wird beim dritten und vierten Paket geschaut, dementsprechend aber mit 10 bzw. mit 11 als Adressen. Wenn 4 aufeinanderfolgende Pakete die richtigen Adressen haben, werden die Daten dem Eventhandler entsprechend in LSB- oder MSB-Reihenfolge sortiert und es wird ein neues Event ausgelöst (`OnCompleteDatapackageRcv`). Auf dieses kann der Nutzer schlussendlich zugreifen.
Sobald eines der Pakete nicht die momentan erwartete Adresse hat, werden alle zwischengespeicherten Daten und das Paket selber verworfen und es wird von vorne begonnen.

Der Engine-Nutzer kann festlegen, welche Funktionen bei welchem Channel ausgelöst werden sollen. Dazu muss in die Liste channelFunctions an der entsprechenden Stelle ein FunctionPointer eingespeichert werden. Wenn z.B. auf challenFunctions[1] eine Bewegefunktion für ein 3D-Element gespeichert ist, wird bei Datenerhalt von Channel1 das Event ausgelöst und in diesem die Methode ausgeführt. 

Es ist auch möglich, das Event selber abzufragen, um so auch die übertragenen Daten verwerten zu können. Dazu muss die Funktion abgefragt werden, beispielsweise so wie in Codeblock blabla.

codeBlockCSharpVonTemplate

#### MEGACard_Input

Die Klasse *MEGACard_Input* besteht aus dem in Tabelle blabla erklärten Konstruktor, den in den Tabellen blabla und blabla erklärten Properties, den in Tabelle blabla erklärten Variablen und den in Tabelle blabla erklärten Funktionen.

| Konstruktor                                                 | Erklärung                                                    |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| MEGACard_Input(string comOfMEGACard, bool msbFirst = false) | Öffnen des angegebenen COM-Ports (wenn möglich) + Angabe, ob Datenübertragung LSB-First oder MSB-First (empfohlen, auch in Beispielen verwendet) erfolgt. Ruft automatisch die Funktionen *GetAvailablePorts* und *Connect* auf |

| Writeable und Readable Property | Erklärung                                                    |
| ------------------------------- | ------------------------------------------------------------ |
| List<Func<bool>> ChannelFuncs   | Liste zum Abspeichern von Funktionen, hat nach Initialisierung bereits 64 Elemente, wenn Channel n Daten gesendet wird, wird Funktion ChannelFuncs[n] ausgeführt |

| Readable Property | Erklärung                                                    |
| ----------------- | ------------------------------------------------------------ |
| bool MSBFirst     | Wenn true wird von MSB-First-Übertragung ausgegangen, ansonsten von LSB-First-Übertragung |
| string Port       | Gibt den Port an, der offen ist / der gerade geöffnet wird   |
| string[] Ports    | Gibt die Ports vom letzten Mal nachschauen an                |

| Variablen                  | Erklärung                                                    |
| -------------------------- | ------------------------------------------------------------ |
| DateTime[] channelCallTime | Zeit, wann von den Channels das letzte Mal etwas gekommen ist (für jeden Channel einzeln aufrufbar an der entsprechenden Position, also Channel0 auf channelCallTime[0] und so weiter) |

| Funktion                                                     | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| string AutoConnectToFirstAvailableSerialPort(string[] excludedPorts) | Verbindet automatisch mit dem ersten nicht exkludierten Port, automatische Verbindung ist nur erfolgreich, wenn Gerät, mit dem man sich verbinden will, bei Funktionsaufruf schon angeschlossen ist<br />Gibt den Port zurück, bei dem die Verbindung erfolgreich war |
| void Connect(string port)                                    | Verbindet SerialPort-Objekt mit angegebenem Port, verwendet die in Listing blabla code:sPort gezeigten Einstellungen |
| void Disconnect()                                            | Trennt alle SerialPort-Verbindungen                          |
| string[] GetAvailablePorts()                                 | Liefert alle COM-Ports, an denen etwas angeschlossen ist und speichert diese in *Ports* |
| Send(byte toSend)                                            | Funktion zur Übertragung von Daten an MEGACard bei bereits geöffneter Verbindung |



___



## Tastatur- und Mausinputs

Unter Tastaturinput versteht man alle Eingaben, die über eine Tastatur passieren. Dabei ist in diesem Fall aber nicht das physische Drücken einer Taste gemeint, sondern das Event, in dem das Drücken einer Taste schon verarbeitet wurde. Das hat den Vorteil, dass sich keine Gedanken über Tastaturlayout und Tastenbelegungen gemacht werden müssen. Ansonsten wären die Anwendungen, in denen die Game Engine verwendet wird, immer nur für ein bestimmtes Layout und damit nur für eine Sprache bzw. alle Sprachen mit genau diesem Layout geeignet.
Den Tastenverarbeitungsteil bzw. das passende Auslösen eines Events übernimmt das Betriebssystem. Das hat zur Folge, dass ein Tastaturinput-Event im hier verwendeten Sinn nicht immer zwingend von einem physischen Tastendruck getriggert werden muss, sondern auch softwareseitig ausgelöst werden kann, zum Beispiel bei Remote-Steuerung eines Computers.

Wenn im Weiteren von Inputs geredet wird, ist damit das schon verarbeitete Event gemeint.

### Input bei Helix Toolkit

Helix Toolkit unterstützt von sich aus schon Inputs von Tastatur und Maus. Standardmäßig sind Q, W, Z, A, S, D, Backspace, mittlere Maustaste und rechte Maustaste belegt. Diese Bindings wurden allerdings aufgehoben, da in Spielen im Normalfall immer der Entwickler die Belegungen angeben will oder sie den Spieler selbst einstellen lässt. Wichtig zu beachten ist, dass Tasten, die von Helix standardmäßig schon belegt sind, trotzdem noch genau einmal vom Helix-User (und damit auch vom Niob-User) belegt werden können. Damit sich beispielsweise nicht immer die Kamera bewegt, wenn mit der Taste A etwas gesteuert werden soll, muss diese und im gleichen Sinn auch alle anderen Belegungen gelöst werden.

### Keyboard-Inputs

#### KeyboardInput

Die Klasse `KeyboardInput` kümmert sich um alle Inputs, die von einer Taste ausgelöst werden. In C# kann auf alle verfügbaren Tastenarten über `System.Windows.Input.Keys` (eine enum-Klasse) zugegriffen werden.

##### Unbinding der Standardbelegungen

Damit die Standardbelegungen gelöst werden, muss ein Objekt der Klasse `KeyboardInput` instanziiert werden (siehe Tabelle blabla). Im Konstruktor dieses Objekts werden automatisch alle Bindings, die es Helix-seitig standardmäßig gibt, gelöst.

| Konstruktor                                                  | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| KeyboardInput(ref NiobViewPort viewPort, GameLoop gameLoopForKeyRepeat) | Erstellen eines KeyboardInput-Objekts, in dem automatisch Helix-Autobindigs aufgehoben werden und alle GameLoop-Ticks ausgelöst wird |

Außerdem können Funktionen angegeben werden, die beim Drücken von Tasten ausgeführt werden sollen. Diese können zwar direkt über die Variable `Func<bool>[] funcs` gesetzt werden, allerdings wird aus Handling-Gründen empfohlen, die Funktion `SetKey(Key key, Func<bool> functionToApply)` zu verwenden (siehe Tabelle blabla).

| Funktion                                         | Erklärung                                                    |
| ------------------------------------------------ | ------------------------------------------------------------ |
| bool SetKey(Key key, Func<bool> functionToApply) | Wenn die Taste _key_ gedrückt wird, wird functionToApply_ ausgeführt. Kümmert sich um richtiges Abspeichern für korrekte Auswahl durch Tastendruck-Event (an Position des Wertes des angegebenen Key-enums) |

`System.Windows.Input.Keys` enthält die folgenden Tasten, denen die dabeistehenden Integer-Werte zugeordnet sind:

- None = 0
- Cancel = 1
- Back = 2
- Tab = 3
- LineFeed = 4
- Clear = 5
- Enter = 6
- Return = 6
- Pause = 7
- Capital = 8
- CapsLock = 8
- HangulMode = 9
- KanaMode = 9
- JunjaMode = 10
- FinalMode = 11
- HanjaMode = 12
- KanjiMode = 12
- Escape = 13
- ImeConvert = 14
- ImeNonConvert = 15
- ImeAccept = 16
- ImeModeChange = 17
- Space = 18
- PageUp = 19
- Prior = 19
- Next = 20
- PageDown = 20
- End = 21
- Home = 22
- Left = 23
- Up = 24
- Right = 25
- Down = 26
- Select = 27
- Print = 28
- Execute = 29
- PrintScreen = 30
- Snapshot = 30
- Insert = 31
- Delete = 32
- Help = 33
- D0 = 34
- D1 = 35
- D2 = 36
- D3 = 37
- D4 = 38
- D5 = 39
- D6 = 40
- D7 = 41
- D8 = 42
- D9 = 43
- A = 44
- B = 45
- C = 46
- D = 47
- E = 48
- F = 49
- G = 50
- H = 51
- I = 52
- J = 53
- K = 54
- L = 55
- M = 56
- N = 57
- O = 58
- P = 59
- Q = 60
- R = 61
- S = 62
- T = 63
- U = 64
- V = 65
- W = 66
- X = 67
- Y = 68
- Z = 69
- LWin = 70
- RWin = 71
- Apps = 72
- Sleep = 73
- NumPad0 = 74
- NumPad1 = 75
- NumPad2 = 76
- NumPad3 = 77
- NumPad4 = 78
- NumPad5 = 79
- NumPad6 = 80
- NumPad7 = 81
- NumPad8 = 82
- NumPad9 = 83
- Multiply = 84
- Add = 85
- Separator = 86
- Subtract = 87
- Decimal = 88
- Divide = 89
- F1 = 90
- F2 = 91
- F3 = 92
- F4 = 93
- F5 = 94
- F6 = 95
- F7 = 96
- F8 = 97
- F9 = 98
- F10 = 99
- F11 = 100
- F12 = 101
- F13 = 102
- F14 = 103
- F15 = 104
- F16 = 105
- F17 = 106
- F18 = 107
- F19 = 108
- F20 = 109
- F21 = 110
- F22 = 111
- F23 = 112
- F24 = 113
- NumLock = 114
- Scroll = 115
- LeftShift = 116
- RightShift = 117
- LeftCtrl = 118
- RightCtrl = 119
- LeftAlt = 120
- RightAlt = 121
- BrowserBack = 122
- BrowserForward = 123
- BrowserRefresh = 124
- BrowserStop = 125
- BrowserSearch = 126
- BrowserFavorites = 127
- BrowserHome = 128
- VolumeMute = 129
- VolumeDown = 130
- VolumeUp = 131
- MediaNextTrack = 132
- MediaPreviousTrack = 133
- MediaStop = 134
- MediaPlayPause = 135
- LaunchMail = 136
- SelectMedia = 137
- LaunchApplication1 = 138
- LaunchApplication2 = 139
- Oem1 = 140
- OemSemicolon = 140
- OemPlus = 141
- OemComma = 142
- OemMinus = 143
- OemPeriod = 144
- Oem2 = 145
- OemQuestion = 145
- Oem3 = 146
- OemTilde = 146
- AbntC1 = 147
- AbntC2 = 148
- Oem4 = 149
- OemOpenBrackets = 149
- Oem5 = 150
- OemPipe = 150
- Oem6 = 151
- OemCloseBrackets = 151
- Oem7 = 152
- OemQuotes = 152
- Oem8 = 153
- Oem102 = 154
- OemBackslash = 154
- ImeProcessed = 155
- System = 156
- DbeAlphanumeric = 157
- OemAttn = 157
- DbeKatakana = 158
- OemFinish = 158
- DbeHiragana = 159
- OemCopy = 159
- DbeSbcsChar = 160
- OemAuto = 160
- DbeDbcsChar = 161
- OemEnlw = 161
- DbeRoman = 162
- OemBackTab = 162
- Attn = 163
- DbeNoRoman = 163
- CrSel = 164
- DbeEnterWordRegisterMode = 164
- DbeEnterImeConfigureMode = 165
- ExSel = 165
- DbeFlushString = 166
- EraseEof = 166
- DbeCodeInput = 167
- Play = 167
- DbeNoCodeInput = 168
- Zoom = 168
- DbeDetermineString = 169
- NoName = 169
- DbeEnterDialogConversionMode = 170
- Pa1 = 170
- OemClear = 171
- DeadCharProcessed = 172

Mit diesen Werten für Tasten können, wie bereits erwähnt, Funktionen im Array _funcs_ korrekt abgespeichert werden. Außerdem gibt es noch die Arrays _funcActive_ und _funcsCallTime_ (siehe Tabelle blabla).

| Variable                 | Erklärung                                                    |
| ------------------------ | ------------------------------------------------------------ |
| Func<bool>[] funcs       | Array mit allen Tasten zugewiesenen Funktionen, bekommt bei Initialisierung in Konstruktor Größe von 173 zugewiesen, da es 173 verschiedene Tastenwerte bei _System.Windows.Input.Keys_ und dementsprechend viele mögliche Funktionen gibt. Wenn direkt die Variable verändert wird, sind die oben gelisteten Keys-Werte hilfreich, Verwendung der Funktion _SetKey_ handlingtechnisch empfohlen |
| bool[] funcActive        | Alle Funktionen, die während des aktuellen GameLoop-Ticks ausgeführt worden sind, speichern automatisch an der entsprechenden Stelle in dieser Variable true ab, relevant für Vermeidung von mehrfacher Ausführung einer Funktion wenn Funktionen über beispielsweise MEGACard-Input und Tastaturinput steuerbar. |
| DateTime[] funcsCallTime | Alle Funktionen, die in einem GameLoop-Tick ausgeführt werden, speichern automatisch an der entsprechenden Stelle in dieser Variable die aktuelle DateTime, relevant für Vermeidung von mehrfacher Ausführung einer Funktion wenn Funktionen über beispielsweise MEGACard-Input und Tastaturinput steuerbar. |



