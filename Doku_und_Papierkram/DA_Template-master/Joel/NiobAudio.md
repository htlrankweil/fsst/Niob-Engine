## OpenAL

Zum besseren Verständnis der Audio-Engine ist ein grundlegendes Verständnis von OpenAL notwendig. OpenAL ist eine Freeware-Audio-Engine, 

Hilfreich für das generelle Verständnis der nachfolgenden Erklärung ist Grafik blabla, die die Zusammenhänge darstellt.
Sounddaten werden in sogenannten Buffern gespeichert, ein Buffer ist dabei zumeist ein Soundfile bzw. verallgemeinert gesagt eine zusammenhängende Reihe von Sounddaten.
Buffer werden von Sources abgespielt, dabei können mehrere Sources auf denselben Buffer zugreifen, nie aber auf mehrere gleichzeitig. Sources können die Bufferdaten für sich verändern, beispielsweise kann der Output einer Source gepitcht werden, mit einem Effekt oder mit einem Filter belegt werden. Dabei werden allerdings nie Bufferdaten verändert, sondern immer nur die Sound-Outputs der Sources.
Der Listener hört auf alle Outputs der ihm zugehörigen Sources und gibt die Summe aus.
Ein Listener (der auf beliebig viele Sources hört) zusammen mit den Sources, auf die er hört, wird Context genannt. Innerhalb eines Contextes können theoretisch die Positionen aller dazugehörigen Elemente dreidimensional verschoben werden, sodass der Listener eine Source, die sich links vorne von ihm befindet, auch von links vorne hört. Praktisch wurde diese Funktion OpenAL-seitig zum Zeitpunkt der DA noch nicht veröffentlich. Es ist lediglich möglich, eine Source nur auf dem linken Ohr, nur auf dem rechten Ohr oder auf beiden Ohren gleich laut abzuspielen. Deshalb musste das eigentliche (360°-)Stereo noch dazuprogrammiert werden (siehe NiobSoundSource).
Ein Device ist die Summe aus allen Buffern und allen erstellten Contexten und gibt die Summe aller Listener-Outputs wieder. Der Device-Output wird auf das Wiedergabegerät weitergeleitet (ist also das, was der User hört). Auch hier ist es von OpenAL aus zum Zeitpunkt der DA nicht möglich, mehr als einen Context und mehr als einen Listener zu erstellen. Das führt dazu, dass der Output eines Listeners in dieser DA gleichbedeutend mit dem eines Devices ist.

![image-20230317234226533](NiobAudio.assets/image-20230317234226533.png)

 https://www.openal.org/documentation/OpenAL_Programmers_Guide.pdf Seite 8




NiobAudio, die Audio-Engine, besteht aus drei Klassen:
⦁	NiobAudiomanager
⦁	NiobMusic
⦁	NiobSoundSource (beinhaltet die internal Class NSound)
NiobMusic kümmert sich um alles, was sich nicht in einem simulierten Raum befindet, also bei dem die Lautstärke auf den einzelnen Ohren nicht direkt von der Position/der Rotation des Listeners bzw. der Schallquelle beeinflusst werden. Alles was im Raum verschoben werden können soll, also einen vom User steuerbaren 3D-Effekt haben soll, muss als NiobSoundSource definiert werden.
Unterstützt werden allgemein nur Soundfiles im WAVE-Format (.WAV-Files). Sich durch den Namen selbst erklärende Parameter werden hier nicht genauer erläutert.

### NiobMusic

Diese Klasse kümmert sich um alles, was nicht dreidimensional ist, was in der Praxis vor allem Musik ist. Wenn im weiteren Kontext von Musik geredet wird, ist damit alles gemeint, was von einer Instanz bzw. Instanzen dieser Klasse abgespielt wird.

Es sind nicht die internen Properties / Funktionen gelistet, sondern nur die als Engine-Nutzer verfügbaren.

#### Properties

##### Writable und Readable Properties

Es können die folgenden Parameter gesetzt und ausgelesen werden:

| Property                       | Erklärung                                                    |
| ------------------------------ | ------------------------------------------------------------ |
| int CurrentSongPositionInQueue | Welches Element aus dem Queue momentan abgespielt wird       |
| bool Loop                      | Ob der Queue nach Wiedergabe des letzten Elements wieder von vorne anfangen soll |
| bool LoopCurrent               | Ob das aktuell abgespielte Element in Dauerschleife laufen soll, bei Initialisierung ist das erste Element des Queues gemeint |
| float MaxVolume                | Maximale Lautstärke, auf welche Sounddaten, die diesen Wert überschreiten, begrenzt werden. Die Skala ist logarithmisch. Muss >=0  & >= MinVolume & <=1 sein, sonst wird 0 / der MinVolume-Wert / 1 verwendet. Wenn MaxVolume=1 gibt es keine Einschränkung |
| float MinVolume                | Minimale Lautstärke, die Sounddaten haben müssen, um wiedergegeben zu werden. Die Skala ist logarithmisch. Muss >=0  & <= MaxVolume & <=1 sein, sonst wird 0 / der MaxVolume-Wert / 1 verwendet. Wenn MinVolume=0 gibt es keine Einschränkung |
| float Pitch                    | Die Tonhöhenverschiebung an der Quelle definieren. Pitch>=0.5 und Pitch<=2 als Grenzen. 1 entspricht keiner Verschiebung |
| List<string> Queue             | Liste von Soundfiles (deren Pfade, relativ oder absolut), die abgespielt werden |
| float Volume                   | Zahl, die die angewendete Verstärkung (Lautstärkeverstärkung) angibt. Volume >= 0.0f, 1.0 bedeutet ungedämpft/unverändert. Jede Division durch 2 entspricht -6 dB, jede Multiplikation mit 2 +6 dB. 0 ist eine Ausnahme, es bedeutet stummgeschaltet (-∞ dB) |



##### Readable Properies

Bei den folgenden Properties können die Werte als Anwender nur ausgelesen werden. Die Werte für diese Properties werden (sofern nicht anders angegeben) beim direkten Initialisieren einer NiobMusic-Instanz oder indirekt beim Initialisieren über einen NiobAudiomanager angegeben.
⦁	int AmountOfBits: Anzahl der Bits, aus denen ein Sample besteht (wird bei Sounddatenerstellung festgelegt)
⦁	int Buffer: ID des verwendeten Buffers
⦁	ContextHandle Context:(wegen nicht ausprogrammiertem OpenAL irrelevant)
⦁	IntPtr Device: 32-Bit-Pointer, der auf aktuelles Device zeigt (wegen nicht ausprogrammiertem OpenAL irrelevant)
⦁	string FilePlayingRelative: Relativer Pfad der Datei, die momentan abgespielt wird. Wenn kein File, sondern Sounddaten abgespielt werden, ist der String null. Ändert sich mit aktuell abgespielter Datei
⦁	string FilePlayingAbsolute: Absoluter Pfad der Datei, die momentan abgespielt wird. Wenn kein File, sondern Sounddaten abgespielt werden, ist der String null. Ändert sich mit aktuell abgespielter Datei
⦁	int SampleFrequency: Frequenz, mit der die Sounddaten beim Erstellen gesampelt wurden
⦁	int Source: ID der Source
⦁	bool Stereo: Ob der Musikplayer die ihm gegebenen Daten stereo oder mono abspielt. Stereo bezieht sich nicht auf die Positionierung im simulierten Raum, lediglich auf die Verarbeitungsmethode



Grundsätzlich kann alles über die setzbaren Properties festgelegt werden. Die dazugehörigen (gleich-/ähnlichnamigen) Funktionen kann man zwar aufrufen (wenn dies z.B. bei spezifischen Anwendungsfällen gewünscht ist), muss man aber im Normalfall nicht, da die Properties die Funktionen mit den restlichen (bei den Properties nicht zu übergebenden) Übergabewerten automatisch korrekt ausgefüllt aufrufen.

#### Funktionen

Einige Funktionen sind nicht über Properties aufrufbar:
⦁	bool NiobMStart(byte[] soundData, int sampleFrequency, int amountOfBitsPerSample, bool loop, bool stereo, bool overrideQueue): Starten der Soundquelle mit den angegebenen Sounddaten und den dazugehörigen Parametern. Überschreibt den Queue auf Wunsch hin
⦁	bool NiobMPause(): Die Source wird pausiert, true bei erfolgreichem Durchführen
⦁	bool NiobMStop(): Die Source wird gestoppt, true bei erfolgreichem Durchführen
⦁	bool NiobMReset(): Die Source wird gestoppt (wenn nicht eh schon gestoppt) und auf Initialzustand zurückgesetzt, true bei erfolgreichem Durchführen
⦁	bool NiobMStart(bool restartWhenAlreadyPlaying): Setzt die Wiedergabe nach Pausieren/Stoppen fort, fängt bei Stop von vorne an, beginnt von vorne, wenn restartWhenAlreadyPlaying=true, true bei erfolgreichem Durchführen
⦁	NiobMKill(bool closeContext, bool closeDevice, bool deleteBuffer): Die Source wird disposed und je nach Wunsch auch der Context, das Device (kann nur bei geschlossenem Context geschlossen werden) und der verwendete Buffer

Konstruktoren
Es gibt folgende Konstruktoren:
⦁	NiobMusic(NiobAudiomanager manager, List<string> queueOfFilenames, bool startAfterInit, bool loopCurrent, bool loop = true, int sampleFrequency = 0): Wird zum Abspielen von Files genutzt, manager ist für Verwaltungszwecke durch den Audiomanager
⦁	NiobMusic(NiobAudiomanager manager, byte[] soundData, int sampleFrequency, bool stereo, int amountOfBitsPerSample, bool startAfterInit, bool loop, bool overrideQueue): Wird verwendet, wenn selbstgenerierte Sounddaten abgespielt werden sollen (in Byte-Array-Form, nicht als WAVE-Datei). Manager ist für Verwaltungszwecke durch den Audiomanager
Zur Erklärung der anderen Konstruktor-Parameter siehe weiter oben (Properties)

### NiobSoundSource

Diese Klasse kümmert sich um das Handling von zwei NSound-Objekten (zwei Soundquellen). Ein NSound-Objekt ist de facto ein NiobMusic-Objekt, das entweder nur auf dem rechten oder nur auf dem linken Ohr zu hören ist. Die zwei NSound-Quellen werden von NiobSoundSource separat gesteuert, um so einen Sound scheinbar aus einer Richtung kommen lassen zu können.
NiobSoundSource kümmert sie sich damit um alles, was dreidimensional verschoben werden können soll. In der Praxis sind das vermutlich vor allem Sounds, die je nach relativer Position und Drehung des Spielers zur Soundquelle unterschiedlich klingen sollen.

Wenn im weiteren Kontext von Sounds geredet wird, ist damit alles gemeint, was von einer Instanz bzw. Instanzen dieser Klasse abgespielt wird.

 Es sind nicht die internen Properties / Funktionen gelistet, sondern nur die als Engine-Nutzer verfügbaren.

#### Properties

##### Writable und Readable Properties

Grundsätzlich werden beim Setzen von Properties die dazugehörigen Funktionen aufgerufen. Es können die folgenden Parameter festgelegt werden:

| Property                            | Erklärung                                                    |
| ----------------------------------- | ------------------------------------------------------------ |
| IntPtr disableAirAbsorption         | Wenn gesetzt wird die Lautstärkenabschwächung (mit Luft als Ausbreitungsmedium) nicht beachtet |
| IntPtr disableDoppler               | Wenn gesetzt wird der Dopplereffekt nicht beachtet           |
| bool Loop                           | Ob das aktuell abgespielte Element in Dauerschleife laufen soll |
| float MaxVolume                     | Maximale Lautstärke, auf welche Sounddaten, die diesen Wert überschreiten, begrenzt werden. Die Skala ist logarithmisch. Muss >=0  & >= MinVolume & <=1 sein, sonst wird 0 / der MinVolume-Wert / 1 verwendet. Wenn MaxVolume=1 gibt es keine Einschränkung |
| float MinVolume                     | Minimale Lautstärke, die Sounddaten haben müssen, um wiedergegeben zu werden. Die Skala ist logarithmisch. Muss >=0  & <= MaxVolume & <=1 sein, sonst wird 0 / der MaxVolume-Wert / 1 verwendet. Wenn MinVolume=0 gibt es keine Einschränkung |
| string Name                         | Name der Soundquelle; für besseren Überblick und für NiobAudiomanager |
| SharpDX.Vector3 OrientationListener | Drehung des Listeners, beeinflusst die jeweiligen NSound-Lautstärken (also linke/rechte Lautstärke) |
| float Pitch                         | Die Tonhöhenverschiebung an der Quelle definieren. Pitch>=0.5 und Pitch<=2 als Grenzen. 1 entspricht keiner Verschiebung |
| SharpDX.Vector3 PositionListener    | Position des Lsiteners, beeinflusst die Lautstärke (aufgrund der Distanz) |
| SharpDX.Vector3 PositionSource      | Position des Lsiteners, beeinflusst die Lautstärke des Listeners (aufgrund der Distanz) |
| SharpDX.Vector3 VelocityListener    | Geschwindigkeit des Lsiteners, ändert die Position des Listeners und beeinflusst die Tonhöhen (aufgrund des Dopplereffekts, wenn nicht disabled) |
| SharpDX.Vector3 VelocitySource      | Geschwindigkeit der Source, ändert die Position der Source und beeinflusst die Tonhöhen (aufgrund des Dopplereffekts, wenn nicht disabled) |
| float Volume                        | Zahl, die die angewendete Verstärkung (Lautstärkeverstärkung) angibt. Volume >= 0.0f, 1.0 bedeutet ungedämpft/unverändert. Jede Division durch 2 entspricht -6 dB, jede Multiplikation mit 2 +6 dB. 0 ist eine Ausnahme, es bedeutet stummgeschaltet (-∞ dB). Es wird dazu noch die Lautstärkenverminderung durch die 3D-Modellierung beachtet, die eingestellte Lautstärke muss also nicht der Lautstärke entsprechen, mit der am Ende abgespielt wird |

##### Readable Properies

Bei den folgenden Properties können die Werte als Anwender nur ausgelesen werden. Die Werte für diese Properties werden (sofern nicht anders angegeben) beim direkten Initialisieren einer NiobMusic-Instanz oder indirekt beim Initialisieren über einen NiobAudiomanager angegeben.

| Property              | Erklärung                                                    |
| --------------------- | ------------------------------------------------------------ |
| int AmountOfBits      | Anzahl der Bits, die bei der Sounddatenerstellung jeweils während eines Samples abgespeichert wurden |
| int Buffer            | ID des verwendeten Buffers                                   |
| ContextHandle Context | Wegen nicht ausprogrammiertem OpenAL irrelevant              |
| IntPtr Device         | 32-Bit-Pointer, der auf aktuelles Device zeigt (wegen nicht ausprogrammiertem OpenAL irrelevant) |
| string FileToPlay     | Relativer Pfad der Datei, die momentan abgespielt wird. Wenn kein File, sondern Sounddaten abgespielt werden, ist der String null. Ändert sich mit aktuell abgespielter Datei |
| int SampleFrequency   | Frequenz, mit der die Sounddaten beim Erstellen gesampelt wurden |
| int Source            | ID der Source                                                |
| bool Stereo           | Ob der Musikplayer die ihm gegebenen Daten stereo oder mono abspielt. Stereo bezieht sich nicht auf die Positionierung im simulierten Raum (diese ist immer gegeben), lediglich auf die Verarbeitungsmethode |

Grundsätzlich kann alles über die setzbaren Properties festgelegt werden. Die dazugehörigen Funktionen kann man zwar aufrufen (wenn dies z.B. bei spezifischen Anwendungsfällen gewünscht ist), muss man aber im Normalfall nicht, da die Properties die Funktionen mit den entsprechend korrekten Übergabewerten aufrufen.

#### Funktionen

Über die Funktionen werden die Funktionen der zwei NSource-Elemente aufgerufen und mit für links und rechts berechneten Werten befüllt.
Einige Funktionen sind nur direkt, nicht über Properties aufrufbar:

| Funktion                                                     | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| bool SStart(bool restartWhenAlreadyPlaying = false)          | Setzt die Wiedergabe fort (z.B. nach Pausieren/Stoppen), fängt bei Stop von vorne an, beginnt von vorne wenn `restartWhenAlreadyPlaying=true`, gibt `true` bei erfolgreichem Durchführen zurück |
| bool SPause()                                                | SoundSource wird pausiert, gibt `true` bei erfolgreichem Durchführen zurück |
| bool SStop()                                                 | Die SoundSource wird gestoppt, also pausiert und auf Start zurückgesetzt, gibt `true` bei erfolgreichem Durchführen zurück |
| bool SReset()                                                | SoundSource wird gestoppt (wenn nicht eh schon gestoppt) und auf Initialzustand zurückgesetzt, gibt `true` bei erfolgreichem Durchführen zurück |
| void SKill(bool closeContext, bool closeDevice, bool deleteBuffer) | Die Source wird disposed und je nach Wunsch auch der Context, das Device (kann nur bei geschlossenem Context geschlossen werden) und der verwendete Buffer |

#### Konstruktoren

Es gibt folgenden Konstruktoren:

| Konstruktor                                                  | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| NiobSoundSource(NiobAudiomanager manager, NiobEntity3d entity, string filename, bool startAfterInit = true, bool loop = false, int sampleFrequency = 0) | Wird zum Abspielen von Files genutzt, entity ist das Objekt im 3D-Raum, von dem aus die Sounds abgespielt werden, manager ist für Verwaltungszwecke durch den Audiomanager.<br/>Die restlichen Parameter werden unter Properties erklärt bzw. sind selbsterklärend |

#### Berechnungen

Es wird für alle Rechnungen standardmäßig immer von einem Kopf in Kugelform mit 15cm Durchmesser und zwei Ohren als Schallempfänger ausgegangen (siehe Grafik blabla). Als Berechnungsgrundlage für das Übertragungsmedium wird Luft mit 20 °C Temperatur (entspricht 293.15 K), 50 % relativer Luftfeuchtigkeit und einem Atmosphärendruck von 101.325 kPa verwendet. (Fußnote Quelle: NWiii)



##### Lautstärkeberechnungen

Wenn Schallwellen auf ein Hindernis treffen, werden diese nicht vollständig absorbiert, sondern werden gebeugt. Dadurch wird die Lautstärke auf einem Ohr, das von der Schallquelle abgewandt ist, trotzdem nicht 0.
"Die Abweichung vom geometrischen Strahlenverlauf wird bemerkbar, wenn die Dimension der Hindernisse oder der Öffnung in der Größenordnung der Wellenlänge liegt oder kleiner als diese ist." (https://www.spektrum.de/lexikon/physik/beugung/1503). 
Die Wellenlänge lässt sich als Division von Ausbreitungsgeschwindigkeit c durch die Frequenz berechnen, wobei c die Schallgeschwindigkeit in Luft ist.

Da die Hörschwelle bei 20 Hz liegt, erhält man für Lambda dabei den folgende Grenzwert:
$$
\lambda_{20Hz}=\frac{c}{f}=\frac{342.3\frac{m}{s}}{20Hz}=17.115m
$$
Vereinfacht kann man also sagen, dass Schallbeugung dann nicht berücksichtigt werden muss, wenn ein Objekt größer als 17.115 m ist.

Wichtig anzumerken ist, dass hier keine Reflexionen berücksichtigt werden, durch welche man Schall auch hören könnte, wenn man hinter einem  Objekt größer 17.115 m steht. Weil diese enorm ressourcenfressend sind, wurde darauf verzichtet.

##### Luftabschwächung (Distanzabschwächung)

Lautstärke nimmt mit der Distanz ab. Die Abnahme der Schallintensität in Luft lässt sich mit der Formel
$$
a = 8.686 · f^2 · ((1.84 · 10^{-11}) + y) [dB/m]
$$

$$
as = a · d [dB]
$$



berechnen, wobei. Sie wird in die Berechnung mit einbezogen, wenn das entsprechende Property gesetzt ist. (Fußnote Quelle NWiii)

##### Doppler-Berechnungen

Wenn sich eine Schallquelle auf einen Empfänger zubewegt, tritt der sogenannte Dopplereffekt auf. Dieser besagt, dass die Bewegungen zu Frequenzänderungen führen. Bewegen sich Sender und Empfänger aufeinander zu, kann die Frequenzänderung mit der Formel
$$
f=f_0*\frac{c-v_E}{c+v_S}
$$
beschrieben werden. Entfernen sie sich voneinander, wird die Formel
$$
f=f_0*\frac{c+v_E}{c-v_S}
$$
verwendet. (Fußnote Quelle: NWiii)

##### Interferenzen

Interferenzen sind Überlagerungen von Wellen, die zu Verstärkungen und Abschwächungen führen können.(Fußnote Quelle NWiii) Sie wären für ein noch realitätsnahes Sounddesign zwar notwendig, werden nicht berücksichtigt, da dafür auch Beugungen, Reflexionen und alle Entities des näheren Umfeldes beachtet werden müssten und das viel zu viel Rechenleistung benötigen würde.

### NiobAudiomanager

Diese Klasse kümmert sich um die Gesamtverwaltung von Musik (NiobMusic) und Sounds (mehrere NiobSoundSource-Objekte) bzw. das, was mit den Klassen abgespielt wird. NiobAudiomanager ist die Schnittstelle der Audio-Engine zum User bzw. zur Game Engine.
Die verfügbaren Funktionen rufen verschiedene Methoden von NiobMusic und/oder NiobSoundSource auf. Dabei greift jedes Sound- / Musikobjekt, das erstellt wird, auf den Context und das Device des NiobAudiomanagers zu, da es nicht möglich ist, mehrere Instanzen dieser beiden Typen zu erstellen, ohne dass eine Exception geworfen wird.

#### Variablen

| Variable                        | Erklärung                                                    |
| ------------------------------- | ------------------------------------------------------------ |
| List<NiobSoundSource> soundList | Liste, in der alle SoundSource-Objekte (also alle Sounds) abgespeichert werden; wichtig für die Wiedergabe der einzelnen Sounds |
| NiobMusic music                 | NiobMusic-Objekt, von dem aus die Musik abgespielt wird.     |





#### Properties

##### Writable und Readable Properties

| Property                  | Erklärung                                                    |
| ------------------------- | ------------------------------------------------------------ |
| bool DeviceDefined        | Variable für Zustand des Devices (ob definiert oder nicht), um Crashes durch mehrfache Erstellung zu vermeiden |
| bool ContextDefined       | Variable für Zustand des Contextes (ob definiert oder nicht), um Crashes durch mehrfache Erstellung zu vermeiden |
| bool DeviceGettingDefined | Wird während Erstellung eines Devices true gesetzt und danach wieder false, um Crashes zu vermeiden |
| IntPtr DeviceOfManager    | Das aktuelle Device (wird momentan nicht benötigt; wäre sinnvoll, wenn OpenAL mehrere Devices unterstützen würde) |



#### Funktionen

Die meisten Methoden können auch über Properties gesetzt werden. Zum besseren Verständnis werden die Funktionen mit den jeweiligen Parametern hier erklärt.

Anmerkung: Die teilweise mehrfachen Aufrufmöglichkeiten von Musik- / Sound-Funktionen  (einzeln , über eine Masterfunktion, über Properties) wurde implementiert, um so dem Engine-User möglichst wenig Schreibaufwand zu bereiten und ein intuitives Programmieren zu ermöglichen. Es wird generell empfohlen, wenn möglich immer über die im NiobAudiomanager implementierten Funktionen auf die Methoden von NiobMusic und NiobSoundSource-Elementen zuzugreifen, um so den NiobAudiomanager auf dem neuesten Stand zu halten und Crashes zu vermeiden.

| Funktion                                                     | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| void MusicPitch(float pitch), void SoundsPitch(float pitch), void MasterPitch(float pitch) | Funktion zum Hinzufügen von Pitch zu NiobMusic / NiobSoundSources (SoundList-Elementen) / beidem. 1 bedeutet normaler Pitch (kein Pitch), 2 bedeutet Pitch auf 200% (bzw. +100%, alle Frequenzen doppelt), 0.5 bedeutet Pitch auf 50%  (bzw. -50% Pitch, alles Frequenzen halbiert), 0.1 für Pitch auf 10% (oder -90% Pitch) |
| MusicKill(bool closeContext, bool closeDevice, bool deleteBuffer), void SoundKill(bool closeContext, bool closeDevice, bool deleteBuffer), void MasterKill(bool closeContext, bool closeDevice, bool deleteBuffer) | Schließen einer der Musikquelle / der Soundquellen mit dem angegebenem Namen / beidem und den optional angebbaren OpenAL-Elementen; Context muss geschlossen werden, um das Gerät zu schließen, Gerät nicht schließen, wenn noch andere Sounds/Musik abgespielt werden soll, Buffer kann zur Optimierung gelöscht werden wenn nicht mehr benötigt |
| void MusicStop(), void SoundStop(), void MasterStop()        | Stoppt die Musik / alle Sounds / beides. Wenn einzelne Sounds gestoppt werden sollen, muss auf diese über die SoundList direkt zugegriffen werden |
| void MusicPause(), void SoundPause(), void MasterPause()     | Pausiert NiobMusic / NiobSoundSources (SoundList-Elemente) / beides |
| void MusicVolumeSet(float volume), void SoundVolumeSet(float volume), MasterVolumeSet(float volume) | Setzt die Lautstärke von NiobMusic / NiobSoundSources (SoundList-Elemente) / beidem. Bei NiobSoundSources werden dazu immer noch die Lautstärkenverminderung durch 3D-Modellierung beachtet |
| void MusicVolumeMinSet(float minVolume), void SoundVolumeMinSet(float minVolume), void MasterVolumeMinSet(float minVolume) | Setzt die minimale Lautstärke von NiobMusic / NiobSoundSources (SoundList-Elementen) / beidem auf den angegebenen Wert |
| void MusicVolumeMaxSet(float maxVolume), void SoundVolumeMaxSet(float maxVolume), void MasterVolumeMaxSet(float maxVolume) | Setzt die maximale Lautstärke von NiobMusic / NiobSoundSources (SoundList-Elementen) / beidem auf den angegebenen Wert |
| void MusicStart(string filename, bool overrideQueue=false, bool loopCurrent = true) | NiobMusic-Objekt mit angegebenen Parametern laufen lassen, wenn overrideQueue=false wird filename zum Queue hinzugefügt |
| void MusicStart(bool restartWhenAlreadyPlaying = false)      | NiobMusic nach Pausieren/Stoppen fortsetzen                  |
| void MusicMakeNew(List<string> queue, int sampleFrequency = 0, bool startAfterInit = true, bool loopCurrent = false, bool loop = true), void MusicMakeNew(byte[] soundData, int sampleFrequency, int amountOfBitsPerSample, bool loop = true, bool stereo = false, bool overrideQueue = true) | Eine neue NiobMusic-Instanz erstellen (und die alte überschreiben) mit den angegebenen Parametern |
| bool MusicQueueAdd(string song)                              | Funktion, um song zum Queue hinzuzufügen (an Ende des bisherigen Queues) |
| bool MusicQueueAdd(List<string> songs)                       | Funktion, um songs zum Queue hinzuzufügen (an Ende des bisherigen Queues) |
| bool MusicQueueSet(List<string> queue)                       | Funktion, um den Queue durch songs zu ersetzen               |
| bool MusicQueueSet(string song)                              | Funktion, um den Queue durch song zu ersetzen                |
| List<string> MusicQueueGet()                                 | Funktion, um den aktuellen Queue zu erhalten                 |

#### Konstruktoren

| Konstruktor                                                  | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| NiobAudiomanager(List<string> musicFilenameList, int sampleFrequency = 0, bool startAfterInit = true, bool loop = true, bool loopCurrent = false) | Erstellen eines NiobMusic-Objekts basierend auf den Musik-Filename-Liste (mit den angegebenen Parametern), eines Contextes (sofern noch nicht definiert) und eines Devices (sofern noch nicht definiert) |
| NiobAudiomanager(byte[] soundData, int amountOfBitsPerSample, int sampleFrequency, bool stereo = false, bool startAfterInit = true, bool loop = true, bool overrideCurrentQueue = true) | Erstellen eines NiobMusic-Objekts basierend auf einem Sounddaten-Array (mit den angegebenen Parametern), eines Contextes (sofern noch nicht definiert) und eines Devices (sofern noch nicht definiert) |