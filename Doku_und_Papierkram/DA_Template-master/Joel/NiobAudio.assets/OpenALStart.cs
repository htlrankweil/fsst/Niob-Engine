// Einlesen eines Files (so, also ohne weitere Konvertierung, nur für .WAV-Files möglich)
byte[] soundData = System.IO.File.ReadAllBytes(filename);

Alc.MakeContextCurrent(context);
// Laden der Sounddaten in einen existierenden Buffer namens buffer
switch (amountOfBits)   // amountObBits ist die Anzahl der Bits, die bei der Erstellung des WAV-Files pro Sample abgespeichert wurden
{
    case 8:
        AL.BufferData(buffer, ALFormat.Mono8, soundData, soundData.Length, sampleFrequency);
        break;
    case 16:
        AL.BufferData(buffer, ALFormat.Mono16, soundData, soundData.Length, sampleFrequency);
        break;
    default:
        AL.BufferData(buffer, ALFormat.Mono16, soundData, soundData.Length, sampleFrequency);
        break;
}
// Anpassen einer bereits existierenden Source namens source, dass sie den buffer als Quelle verwendet
AL.Source(source, ALSourcei.Buffer, buffer);
// Starten von source (Sounddaten wirklich abspielen)
AL.SourcePlay(source);