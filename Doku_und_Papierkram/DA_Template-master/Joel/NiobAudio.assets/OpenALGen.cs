        public byte[] GetSinusSoundData(int frequencyOfSin, int sampleFrequency, double amp)
        {
            double dt = 2 * System.Math.PI / sampleFrequency;
            int freq = frequencyOfSin;
            var dataCount = sampleFrequency / freq;

            var sinData = new short[dataCount];
            for (int i = 0; i < sinData.Length; ++i)
            {
                sinData[i] = (short)(amp * short.MaxValue * System.Math.Sin(i * dt * freq));
            }
            byte[] sindataBytedArray = new byte[sinData.Length];
            for (int i = 0; i < sinData.Length; i++)
            {
                sindataBytedArray[i] = System.Convert.ToByte(sinData[i]);
            }
            return sindataBytedArray;
        }