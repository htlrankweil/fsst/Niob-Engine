using OpenTK;
using OpenTK.Audio.OpenAL;

// Initialisieren
AL.GetError();                          // Abfragen und Zurücksetzen der Fehler
IntPtr device = Alc.OpenDevice(null);   // Definieren eines Devices:

ContextHandle c = Alc.CreateContext(Device, (int*)null);
context = c;
Alc.MakeContextCurrent(Context);        // Festlegen, welcher Context verwendet werden soll
            
// Generieren von Sound-Buffer(n) / Soundquellen
Alc.MakeContextCurrent(context); //Erneutes Festlegen, falls der Context zwischenzeitlich von einem anderen Source-Objekt geändert wurde
AL.GenBuffers(1, out buffer);
AL.GenSources(1, out source);