### TWID KW 45
#### TWID
6 h
* Besprechung der Vorgehensweise
* Einrichten von Git
* Start mit Programmierung der Basis-Library

#### TWIA
* Größeres Verständnis von und für Shader (Hilfreicher Link: www.online-tutorials.net/directx/shader-konzept/tutorials-t-7-77.html)

#### PIETW
* Shader
* Shaderumwandlung von High-Level Shading Language (hlsl) in ein für Helix verwertbares Format

#### NWILD/NWICD
* Indexer - C# Doku
* Data Model Tree	// Wie kann man Knoten/Kanten abspeichern?
* Shader/Geometry Shader
* Rigging