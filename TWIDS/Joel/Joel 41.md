### TWID KW 41
Joel Kasemann
<hr>

#### TWID
5 h
* Installation eines Ubuntu-Systems in der Virtual Box und LaTex
* Besprechung/Recherche zu Einsatz von Helix Toolkit (SharpDX)
* Überlegungen zur Rigging-Vorgehensweise

#### TWIA
* Funktionsfähiges Ubuntu-System mit LaTex
* Hintergrundinformationen zu SharpDX

#### PIETW
* Bei VM-Installationen muss das Ubunt-File (Download von offizieller Ubuntu-Website) als DVD eingelegt und nach der Installation wieder aus dem Laufwerk ausgeworfen werden. Man sollte den PC/die VM nicht ausschalten, während sie das zweite Mal wieder hochfährt, sonst muss man die Installation erneut machen (-> sehr viel Zeitaufwand)

#### NWILD
* Einlesen von 3D-Dateiformat für SharpDX 