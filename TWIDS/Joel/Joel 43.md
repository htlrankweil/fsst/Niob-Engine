### TWID KW 43
#### TWID
6 h
* Gespräch mit Herr Rusch & Paul Walch über Speichervorgehensweise von Helix ToolKit / ASSIMP (von Helix verwendete Library)
* Überlegungungen zu Embedded Resource Importer mit PW
* Ferien

#### TWIA
* Besprechen der Lösung des ASSIMP-Lade-Problems
* Entspannung

#### PIETW
* Mit ASSIMP einlesen (Wenn man in Helix ToolKit über die gegebene 3D-File-Laden-Funktion (ist eine ASSIMP-Funktion) ein .fbx-File lädt, wird die gebakete Textur nicht richtig eingelesen)

#### NWILD
- Fertigstellen Einlesen von 3D-Format
- Shader